-- items_grid_whole_planet
-- depends: 20240514_01_ddvl3-items-grid

-- St_EstimatedExtent was not previse enough, and since we'll likely have items all around the world, we compute the grid on the whole world

DROP MATERIALIZED VIEW items_grid;
CREATE MATERIALIZED VIEW items_grid AS
SELECT
    row_number() over () as id,
    count(*) as nb_items,
    g.geom
FROM
    ST_SquareGrid(
        0.1,
        ST_MakeEnvelope(-180, -90, 180, 90, 4326)
    ) AS g
    JOIN items AS p ON p.geom && g.geom
GROUP BY g.geom;

CREATE INDEX items_grid_id_idx ON items_grid(id);
CREATE INDEX items_grid_geom_idx ON items_grid USING GIST(geom);

