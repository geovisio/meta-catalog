-- drop_item_dt_index
-- depends: 20240314_01_45nNr-add-index-collection-date

CREATE INDEX items_datetime_idx ON items(datetime);
