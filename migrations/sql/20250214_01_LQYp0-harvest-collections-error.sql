-- harvest_collections_error
-- depends: 20250206_01_eI4Yl-differential-harvest

CREATE TABLE IF NOT EXISTS harvest_errors(
    collection_id UUID, -- not a foreign key to Collection, because the collection might not have been created
    harvest_id UUID NOT NULL REFERENCES harvests(id),
    error VARCHAR NOT NULL
);
