-- items_grid
-- depends: 20240409_02_mZO83-dt-triggers

CREATE MATERIALIZED VIEW items_grid AS
SELECT
    row_number() over () as id,
    count(*) as nb_items,
    g.geom
FROM
    ST_SquareGrid(
        0.1,
        ST_SetSRID(ST_EstimatedExtent('items', 'geom'), 4326)
    ) AS g
    JOIN items AS p ON p.geom && g.geom
GROUP BY g.geom;

CREATE INDEX items_grid_id_idx ON items_grid(id);
CREATE INDEX items_grid_geom_idx ON items_grid USING GIST(geom);
