-- dt_triggers
-- depends: 20240409_01_seVEi-drop-item-dt-index

-- Add min/max items datetimes at the instance level, to be able to quickly give those information without the need of an index
ALTER TABLE instances ADD COLUMN IF NOT EXISTS min_datetime TIMESTAMPTZ;
ALTER TABLE instances ADD COLUMN IF NOT EXISTS max_datetime TIMESTAMPTZ;

-- populate tables
DROP FUNCTION IF EXISTS update_all_instance_datetimes CASCADE;
CREATE FUNCTION update_all_instance_datetimes() RETURNS void AS $$
BEGIN
    WITH min_max AS (
        SELECT 
            min(datetime) AS min_datetime,
            max(datetime) AS max_datetime,
            c.instance_id AS instance_id
        FROM items
        JOIN collections c on c.id = collection_id
        GROUP BY c.instance_id
    )
    UPDATE instances
    SET 
        min_datetime = min_max.min_datetime,
        max_datetime = min_max.max_datetime
    FROM min_max
    WHERE instances.id = min_max.instance_id;
END $$ LANGUAGE plpgsql;

SELECT update_all_instance_datetimes();

DROP FUNCTION IF EXISTS update_instance_datetimes CASCADE;
CREATE FUNCTION update_instance_datetimes() RETURNS trigger AS $$
BEGIN
    WITH min_max AS (
        SELECT 
            min(datetime) AS min_datetime,
            max(datetime) AS max_datetime,
            c.instance_id AS instance_id
        FROM updated_items
        JOIN collections c on c.id = collection_id
        GROUP BY c.instance_id
    )
	UPDATE instances
	SET 
        min_datetime = COALESCE(LEAST(instances.min_datetime, min_max.min_datetime), min_max.min_datetime),
        max_datetime = COALESCE(GREATEST(instances.max_datetime, min_max.max_datetime), min_max.max_datetime)
    FROM min_max
    WHERE instances.id = min_max.instance_id;
	RETURN NULL;
END $$ LANGUAGE plpgsql;


CREATE TRIGGER item_dt_insert_trg
AFTER INSERT ON items
REFERENCING NEW TABLE AS updated_items
FOR EACH STATEMENT EXECUTE FUNCTION update_instance_datetimes();

-- Note: deletion/updates are not handled for the moment, if needed, I think we should just call update_all_instance_datetimes daily
