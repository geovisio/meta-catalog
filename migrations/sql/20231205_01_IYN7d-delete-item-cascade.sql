-- delete_item_cascade
-- depends: 20231024_01_QRtLT-split-geom

ALTER TABLE items 
	DROP CONSTRAINT items_collection_id_fkey , 
	ADD CONSTRAINT items_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collections(id) ON DELETE CASCADE;


ALTER TABLE collection_harvest 
	DROP CONSTRAINT collection_harvest_collection_id_fkey , 
	ADD CONSTRAINT collection_harvest_collection_id_fkey FOREIGN KEY (collection_id) REFERENCES collections(id) ON DELETE CASCADE;
