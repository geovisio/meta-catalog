-- add_index_collection_date
-- depends: 20240125_01_sp4ok-stats

DROP FUNCTION IF EXISTS jsonb_date CASCADE;
CREATE FUNCTION jsonb_date(text) RETURNS timestamp
   IMMUTABLE LANGUAGE sql AS $$
   -- hard code the format of the created date parsed and mark this function as immutable since we want to use it in an index
SELECT to_timestamp($1, 'YYYY-MM-DD"T"HH24:MI:SS.USTZH:TZM')
$$;

CREATE INDEX collections_created_at_idx ON collections(jsonb_date(content->>'created'));
CREATE INDEX collections_updated_at_idx ON collections(jsonb_date(content->>'updated'));
