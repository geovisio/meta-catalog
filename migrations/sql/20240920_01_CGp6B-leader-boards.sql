-- leader_boards
-- depends: 20240704_01_4wcya-pg-trgm

DROP MATERIALIZED VIEW IF EXISTS stats.contribution_by_day;
CREATE MATERIALIZED VIEW stats.contribution_by_day AS
WITH 
col_stats AS (
    SELECT 
        (content->>'created')::date AS created_day,
        (content->'stats:items'->>'count')::int AS nb_items,
        ST_Length(computed_geom::geography) / 1000 AS geometry_length_km,
        provider_id
    FROM collections
)
SELECT 
    SUM(geometry_length_km) AS km_captured,
    SUM(nb_items) AS nb_items,
    provider_id,
    created_day
FROM col_stats
GROUP BY created_day, provider_id;


CREATE UNIQUE INDEX contribution_by_day_provider_idx ON stats.contribution_by_day(provider_id, created_day);

-- add refresh view in function
CREATE OR REPLACE FUNCTION stats.refresh_all_views() RETURNS BOOLEAN
LANGUAGE PLPGSQL AS
$$
BEGIN
 RAISE NOTICE 'Starting to refresh stats views';
 REFRESH MATERIALIZED VIEW CONCURRENTLY stats.items_by_instance_creation_month_contributor;
 REFRESH MATERIALIZED VIEW CONCURRENTLY stats.collections_by_instance_creation_month_contributor;
 REFRESH MATERIALIZED VIEW CONCURRENTLY stats.contribution_by_day;
 RAISE NOTICE 'Stats views refreshed';
 RETURN TRUE;
END;
$$;
