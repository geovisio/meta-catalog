-- accounts
-- depends: 20240606_01_N2zAW-stats-concurrent-refresh

CREATE TABLE IF NOT EXISTS providers (
	id UUID PRIMARY KEY,
    instance_id UUID NOT NULL REFERENCES instances(id),
    name VARCHAR NOT NULL
);

-- Add a link between providers and collections
ALTER TABLE collections ADD COLUMN IF NOT EXISTS provider_id UUID;

ALTER TABLE collections ADD CONSTRAINT provider_fk_id FOREIGN KEY (provider_id) REFERENCES providers (id);

-- trigger to fill the providers table and provider_id
DROP FUNCTION IF EXISTS update_providers CASCADE;
CREATE FUNCTION update_providers() RETURNS trigger AS $$
BEGIN

    WITH provider_data AS (
        SELECT jsonb_path_query(content, '$.providers[*]') as raw_provider,
            instance_id,
            id AS collection_id
        FROM new_cols
    ),
    col_providers AS (
        SELECT 
            (raw_provider->>'id')::UUID AS id,
            raw_provider->>'name' AS name,
            instance_id,
            collection_id
        FROM provider_data
        WHERE raw_provider ? 'id'
    ),
    update_col AS (
        UPDATE collections
        SET provider_id = col_providers.id
        FROM col_providers
        WHERE collections.id = col_providers.collection_id
    )
    INSERT INTO providers (id, instance_id, name)
    SELECT id, instance_id, name
    FROM col_providers
    ON CONFLICT (id) DO UPDATE SET name = EXCLUDED.name WHERE providers.name IS DISTINCT FROM EXCLUDED.name;
	RETURN NULL;
END $$ LANGUAGE plpgsql;


CREATE TRIGGER col_insert_trg
AFTER INSERT ON collections
REFERENCING NEW TABLE AS new_cols
FOR EACH STATEMENT EXECUTE FUNCTION update_providers();
