-- fix_stat_projection
-- depends: 20241115_01_8Ggu8-more-instances-info



DROP MATERIALIZED VIEW IF EXISTS stats.items_by_instance_creation_month_contributor;
CREATE MATERIALIZED VIEW stats.items_by_instance_creation_month_contributor AS
    WITH items_stats AS (
        SELECT 
            items.id, 
            items.geom, 
            collections.instance_id, 
            (items.content->'properties'->>'original_file:size')::int AS original_file_size,
            items.content#>>'{providers,0,name}' AS contributor,
            to_char((items.content->'properties'->>'created')::timestamptz, 'YYYY-MM') AS created_month
        FROM items
        JOIN collections on items.collection_id = collections.id
    )
    SELECT 
        COUNT(id) AS nb_items,
        -- we do not compute the approximated coverage anymore as the computation is broken and we need to change the way we compute this statistic.
        0 AS approximated_coverage_km, 
        SUM(original_file_size) AS original_file_size,
        instance_id,
        contributor,
        created_month
    FROM items_stats
    GROUP BY instance_id, created_month, contributor;

CREATE UNIQUE INDEX items_by_instance_creation_month_contributor_idx
  ON stats.items_by_instance_creation_month_contributor (instance_id, created_month, contributor);
