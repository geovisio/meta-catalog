-- accounts
-- depends: 20240606_01_N2zAW-stats-concurrent-refresh

ALTER TABLE collections DROP COLUMN IF EXISTS provider_id;

DROP FUNCTION update_providers CASCADE;

DROP TABLE IF EXISTS providers;

