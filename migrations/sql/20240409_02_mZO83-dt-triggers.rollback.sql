-- dt_triggers
-- depends: 20240409_01_seVEi-drop-item-dt-index

ALTER TABLE instances DROP COLUMN min_datetime;
ALTER TABLE instances DROP COLUMN max_datetime;

DROP Function update_instance_datetimes CASCADE;