-- stats
-- depends: 20231205_01_IYN7d-delete-item-cascade

CREATE SCHEMA IF NOT EXISTS stats;

-- Add views to ease stats computation
-- This one aggregates stats on instance, month of when the pictures was uploaded, and the contributor
DROP MATERIALIZED VIEW IF EXISTS stats.items_by_instance_creation_month_contributor;
CREATE MATERIALIZED VIEW stats.items_by_instance_creation_month_contributor AS
    WITH items_stats AS (
        SELECT 
            items.id, 
            items.geom, 
            collections.instance_id, 
            (items.content->'properties'->>'original_file:size')::int AS original_file_size,
            items.content#>>'{providers,0,name}' AS contributor,
            to_char((items.content->'properties'->>'created')::timestamptz, 'YYYY-MM') AS created_month
        FROM items
        JOIN collections on items.collection_id = collections.id
    )
    SELECT 
        COUNT(id) AS nb_items,
        -- we approximate and aggregate all points to ~20m, and consider that each picture covers 20m of roads
        COUNT(DISTINCT(ST_SnapToGrid(ST_Transform(geom, 2154), 20))) * 20/1000 AS approximated_coverage_km, 
        SUM(original_file_size) AS original_file_size,
        instance_id,
        contributor,
        created_month
    FROM items_stats
    GROUP BY instance_id, created_month, contributor;

DROP MATERIALIZED VIEW IF EXISTS stats.collections_by_instance_creation_month_contributor;
CREATE MATERIALIZED VIEW stats.collections_by_instance_creation_month_contributor AS
    WITH collections_stats AS (
        SELECT 
            id, 
            instance_id, 
            content#>>'{providers,0,name}' AS contributor,
            to_char((content->>'created')::timestamptz, 'YYYY-MM') AS created_month,
            ST_Length(computed_geom::geography) AS geometry_length
        FROM collections
    )
    SELECT 
        COUNT(id) AS nb_col,
        SUM(geometry_length) / 1000 AS collections_length_km,
        instance_id,
        contributor,
        created_month
    FROM collections_stats
    GROUP BY instance_id, created_month, contributor;

-- Function to refresh all views, if new views are added, this function should be updated
CREATE OR REPLACE FUNCTION stats.refresh_all_views() RETURNS BOOLEAN 
LANGUAGE PLPGSQL AS
$$
BEGIN
 RAISE NOTICE 'Starting to refresh stats views';
 REFRESH MATERIALIZED VIEW stats.items_by_instance_creation_month_contributor;
 REFRESH MATERIALIZED VIEW stats.collections_by_instance_creation_month_contributor;
 RAISE NOTICE 'Stats views refreshed';
 RETURN TRUE;
END;
$$;

