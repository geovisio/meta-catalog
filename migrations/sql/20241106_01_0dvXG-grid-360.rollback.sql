-- grid_360
-- depends: 20241025_01_yClAU-fov-index

DROP MATERIALIZED VIEW IF EXISTS items_grid;

CREATE MATERIALIZED VIEW items_grid AS
SELECT
    row_number() over () as id,
    count(*) as nb_items,
    g.geom
FROM
    ST_SquareGrid(
        0.1,
        ST_MakeEnvelope(-180, -90, 180, 90, 4326)
    ) AS g
JOIN items AS p ON p.geom && g.geom
GROUP BY g.geom;

CREATE UNIQUE INDEX items_grid_id_idx ON items_grid(id);
CREATE INDEX items_grid_geom_idx ON items_grid USING GIST(geom);
