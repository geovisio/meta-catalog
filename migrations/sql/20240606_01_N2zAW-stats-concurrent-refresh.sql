-- stats_concurrent_refresh
-- depends: 20240516_01_fKN7K-items-grid-whole-planet

-- refresh all view concurrently, so that we do not block.
-- This needs a unique index on all views

CREATE UNIQUE INDEX items_by_instance_creation_month_contributor_idx
  ON stats.items_by_instance_creation_month_contributor (instance_id, created_month, contributor);

CREATE UNIQUE INDEX collections_by_instance_creation_month_contributor_idx
  ON stats.collections_by_instance_creation_month_contributor (instance_id, created_month, contributor);

-- Function to refresh all views, if new views are added, this function should be updated
CREATE OR REPLACE FUNCTION stats.refresh_all_views() RETURNS BOOLEAN
LANGUAGE PLPGSQL AS
$$
BEGIN
 RAISE NOTICE 'Starting to refresh stats views';
 REFRESH MATERIALIZED VIEW CONCURRENTLY stats.items_by_instance_creation_month_contributor;
 REFRESH MATERIALIZED VIEW CONCURRENTLY stats.collections_by_instance_creation_month_contributor;
 RAISE NOTICE 'Stats views refreshed';
 RETURN TRUE;
END;
$$;


