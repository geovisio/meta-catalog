-- incremental_harvest
-- depends: 20250204_01_7k4fk-fix-stat-projection

ALTER TABLE harvests DROP COLUMN IF EXISTS is_incremental;
ALTER TABLE harvests DROP COLUMN IF EXISTS finished_at;
ALTER TABLE instances DROP COLUMN IF EXISTS idx;


