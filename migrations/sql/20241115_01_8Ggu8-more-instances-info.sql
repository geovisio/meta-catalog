-- more_instances_info
-- depends: 20241106_01_0dvXG-grid-360

ALTER TABLE instances ADD COLUMN IF NOT EXISTS configuration JSONB;
ALTER TABLE instances ADD COLUMN IF NOT EXISTS configuration_updated_at TIMESTAMPTZ;
