-- fox_index
-- depends: 20240920_01_CGp6B-leader-boards

CREATE INDEX items_field_of_view ON items USING BTREE (COALESCE((content->'properties'->'pers:interior_orientation'->>'field_of_view')::int, 0));
