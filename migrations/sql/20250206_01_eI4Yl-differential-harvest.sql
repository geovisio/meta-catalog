-- incremental_harvest
-- depends: 20250204_01_7k4fk-fix-stat-projection

ALTER TABLE harvests ADD COLUMN IF NOT EXISTS is_incremental BOOL;
ALTER TABLE harvests ADD COLUMN IF NOT EXISTS finished_at TIMESTAMPTZ;

-- add an integer indentifier to the instance, it will be used for the lock, and maybe one day we'll migrate to this identifier as primary key, uuid are useless in this case.
ALTER TABLE instances ADD COLUMN IF NOT EXISTS idx SERIAL NOT NULL;


