# 🤝 Panoramax Federation

We welcome every Panoramax instance to join the Panoramax Federation.
If you agree to this facultative registration:

- All the **pictures hosted on your instance would be available on https://panoramax.xyz** and through the API https://panoramax.xyz/api. This is just a catalog with a copy of your pictures metadata: all your pictures will remain hosted on your own instance.
- Your instance would be **officialy recognised by Panoramax**: it would be listed on the Panoramax project website https://panoramax.fr and included in every communication: printed (flyer), oral presentations and more

## ⚙️ Technical requirements

We require that you:

- provide the following information in a dedicated issue at [on GitLab :material-gitlab:](https://gitlab.com/panoramax/server/meta-catalog/-/issues):
  - instance URL, name and logo
  - decide if your instance is open to external contributions or no. If so, we will setup the Panoramax mobile app to allow all users to send pictures to your instance
  - choose the geographical coverage where the users can send pictures to your instance. By default, it is open worldwide
- provide a 24/7 running server for your instance. This is a goal, we don't expect any guarantee of service of any kind
- keep your instance software enough up-to-date to allow the meta-catalog to harvest it on a regular basis.

## ⚖️ Legal requirements

Your instance must only host street-level pictures respecting local laws (anonymized, etc.) of public places or private ones where clear authorization has been obtained.

### Pictures and metadata licenses

We aim to protect and nurture the Panoramax digital common:

- avoid the creation of closed data, derivated database, artwork or picture
- allow to contribute to OpenStreetMap using the pictures
- allow any use in favor of public interest (including public services)

Thus only 2 licenses for the pictures and metadata are accepted for the moment:

- LO V2.0 (Licence Ouverte/Open License version 2: French government open data license similar to CC-BY)
- CC-BY-SA 4.0 for original or derivated pictures sharing + usable for creating derivated data (including AI models) under LO 2.0, CC-BY 4.0 or ODbL 1.0

!!! Note

    This last license allows to create derivated open data useful to public services and OpenStreetMap. This also prevents to create closed derivated data, including AI models or training datasets.
