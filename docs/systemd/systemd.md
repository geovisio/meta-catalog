# Using system.d services

## API

Copy the file [panoramax.service](https://gitlab.com/panoramax/server/meta-catalog/-/blob/develop/docs/systemd/panoramax.service?ref_type=heads) in `/usr/lib/systemd/system/`.

Reload the daemon:

```bash
 systemctl daemon-reload
```

Start the service:

```bash
service panoramax start
```

To get the status of the service:

```bash
service panoramax status
```

To get the service logs:

```bash
journalctl -u panoramax
```

## Harvester

2 files:

- the harvest service
- the timer around it

Copy the files [harvest-instance.service](https://gitlab.com/panoramax/server/meta-catalog/-/blob/develop/docs/systemd/harvest-instance.service?ref_type=heads) and [harvest-instance.timer](https://gitlab.com/panoramax/server/meta-catalog/-/blob/develop/docs/systemd/harvest-instance.timer?ref_type=heads) in `/usr/lib/systemd/system/`.

Change the values you need to change:

- name of the instance, if you have several instance (and change also the name of the files)
- path to configuration file for the harvested instance
- frequency of the harvest

Reload the daemon:

```bash
systemctl daemon-reload
```

Start the timer:

```bash
systemctl start harvest-instance.timer
```

## Stats

The stats are handled as materialzed views that needs to be refreshed.

Like the [Harvester](#harvester), there are 2 files:

- the stat refresh service
- the timer around it

Copy the files [stats-refresh.service](https://gitlab.com/panoramax/server/meta-catalog/-/blob/develop/docs/systemd/stats-refresh.service?ref_type=heads) and [stats-refresh.timer](https://gitlab.com/panoramax/server/meta-catalog/-/blob/develop/docs/systemd/stats-refresh.timer?ref_type=heads) in `/usr/lib/systemd/system/`.

Reload the daemon:

```bash
systemctl daemon-reload
```

Start the timer:

```bash
systemctl start stats-refresh.timer
```
