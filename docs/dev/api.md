# API

!!! Note

    For the API development, you need to install the [Rust](https://www.rust-lang.org/fr) compiler.

    Check the dedicated [documentation section](../install.md#install-standalone-api) for more information.

You can compile without running with:

```bash
cargo build
```

Then you can run `./target/debug/panoramax-api --db-url=postgres://username:password@localhost:5432/panoramax`.

If you also want to quickly know if the compilation is ok you can do

```bash
cargo check
```

For development purpose, you might want hot reload to run build on file changes. To do this, install [cargo-watch](https://github.com/watchexec/cargo-watch):

```bash
cd api
cargo install cargo-watch
```

then

```bash
RUST_LOG=info PORT=9001 DB_URL=postgres://username:password@localhost:5432/panoramax cargo watch -x run
```

## Matomo

Usage statistics can be sent to a configured matomo instance by providing it's url/site-id when running the API.

The environment variables for this are:

- `MATOMO_URL`
- `MATOMO_SITE_ID`

# Tests

To run the tests:

```
cd api
cargo test
```

# Format

All files should be formated with [rustfmt](https://github.com/rust-lang/rustfmt):

```bash
cargo fmt
```