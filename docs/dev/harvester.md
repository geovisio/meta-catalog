# Harvester

## Tests

To run the tests, install the `[dev]` requirements in your virtualenv (cf [the installation section](../install.md#loading-data)), then run pytest:

```bash
pip install -e .[dev]
pytest
```

!!! Note

    By default, the tests use a [Docker Compose](https://docs.docker.com/compose/) environment (using `./docker-compose.yml` and `./harvester/tests/docker-compose-geovisio.yml`) to set-up a temporary stack. You can also spawn the docker compose on you own, then run the tests using the external docker compose used. This can speed up greatly the tests:

    ```bash
    docker compose -f docker-compose.yml -f harvester/tests/docker-compose-geovisio.yml up -d --wait

    # run pytest telling it where to find all services
    pytest --external-geovisio_instance_1-url=http://localhost:5101/api --external-geovisio_instance_2-url=http://localhost:5102/api  --external-api-url http://localhost:9000 --external-db-url postgresql://username:password@localhost:5439/panoramax --external-secrets-url http://localhost:5100 -vv -s
    ```

## Format

All files should be formated using [ruff](https://docs.astral.sh/ruff/formatter/):

```bash
ruff format
```
