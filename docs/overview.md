# Federated catalog overview

The Panoramax federated catalog (also called **meta catalog**) crawls all referenced Panoramax instances and gather their metadata in a :simple-postgresql: database.

!!! Note

    Only the metadata are crawled, not the pictures, the pictures always stay in each instance.

All those metadata are then exposed using an [SpatioTemporal Asset Catalog](https://stacspec.org/) API following the same format as the Panoramax instances.

## :material-sickle: Instances harvest

The instance harvesters are :simple-python: processes that regularly ask the referenced instances what have changed since the last harvest. This way they get a small amount of data and can be kept up to date easily.

## API

A :simple-rust: API exposes all harvested metadata with:

- A rest API compatible with [SpatioTemporal Asset Catalog](https://stacspec.org/) (abbreviated as STAC) and [OGC WFS 3](https://github.com/opengeospatial/WFS_FES) specifications
- Vector tiles to easily display the data on a 🗺️ map.

The API has a subset of the Panoramax instance API (defined in [Geovisio](https://gitlab.com/panoramax/server/api)), primarily making possible to read metadata, access pictures and search things.

One key difference is that contributions are not possible directly in the federated catalog, so no contribution APIs are available.

The :simple-openapiinitiative: OpenAPI documentation is available [in :material-code-json: json format](https://api.panoramax.xyz/openapi.json) or can be viewed with [:simple-swagger: Swagger UI](https://api.panoramax.xyz/api/docs/swagger).
