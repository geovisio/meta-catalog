import aiohttp.http_exceptions
from .conftest import (
    harvest,
    import_collection_to_geovisio,
    ExternalServicesUrl,
    FIXTURE_DIR,
    cleanup_geovisio_instance,
    add_picture_to_collection,
    wait_until_geovisio_collection_ready,
)
from harvester import Instance, harvest_instances, Config
import pytest
from psycopg.rows import dict_row
import psycopg
import requests
from datetime import datetime
import zoneinfo
import json
from uuid import UUID
import aiohttp
import asyncio
from aioresponses import aioresponses

UTC = zoneinfo.ZoneInfo(key="Etc/UTC")


@pytest.fixture(scope="function", autouse=True)
def cleanup_geovisio_instance_1(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers):
    """Delete all collections from geovisio instance 1"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)


@pytest.fixture(scope="function", autouse=True)
def cleanup_geovisio_instance_2(external_services: ExternalServicesUrl, geovision_instance_2_auth_headers):
    """Delete all collections from geovisio instance 2"""
    cleanup_geovisio_instance(external_services.geovisio_instance_2_url, geovision_instance_2_auth_headers)


@pytest.fixture(scope="function", autouse=True)
def cleanup_db_before_test(external_services):
    with psycopg.connect(external_services.db_url) as db:
        db.execute(
            """
        TRUNCATE table collections CASCADE;
        TRUNCATE table harvests CASCADE;
        TRUNCATE table instances CASCADE;
        """
        )


def _get_col_id(col_location: str) -> UUID:
    return UUID(col_location.split("/")[-1])


def test_full_harvest(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers):
    col1 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[
            FIXTURE_DIR / "col1" / "e1.jpg",
            FIXTURE_DIR / "col1" / "e2.jpg",
            FIXTURE_DIR / "col1" / "e3.jpg",
        ],
    )
    col1_id = _get_col_id(col1)

    col2 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 2",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col2_id = _get_col_id(col2)

    harvest(
        instance=Instance(
            name="geovisio_instance_1",
            root_url=external_services.geovisio_instance_1_url,
        ),
        external_services=external_services,
        incremental_harvest=False,
    )

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT count(*) AS nb_instances FROM instances;").fetchone()
        assert r and r["nb_instances"] == 1
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 2
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 5

        harvests = db.execute("SELECT id, instance_id, start FROM harvests;").fetchall()
        assert len(harvests) == 1
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert r and len(col_harvests) == 2

    # Harvesting again the instance should not change the harvested items (but this should change the `harvests` and `collection_harvest` that log the harvests)
    harvest(
        instance=Instance(
            name="geovisio_instance_1",
            root_url=external_services.geovisio_instance_1_url,
        ),
        external_services=external_services,
        incremental_harvest=False,
    )

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT count(*) AS nb_instances FROM instances;").fetchone()
        assert r and r["nb_instances"] == 1
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 2
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 5

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 2

        # again there is only 5 collection_harvest, since the old rows should have been deleted
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            # all collections are marked as 'updated' even if nothing has changed because
            # it's a full harvest and we do not try to detect if there are real changes for the moment
            (col1_id, harvests[-1]["id"]): {"status": "updated"},
            (col2_id, harvests[-1]["id"]): {"status": "updated"},
        }

    # delete 1 collection and add another one to geovisio
    col3 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 3",
        pic_names=[
            FIXTURE_DIR / "col3" / "1.jpg",
            FIXTURE_DIR / "col3" / "2.jpg",
            FIXTURE_DIR / "col3" / "3.jpg",
        ],
    )
    col3_id = _get_col_id(col3)

    delete_col = requests.delete(col2, headers=geovision_instance_1_auth_headers)
    delete_col.raise_for_status()

    # harvest again, we should only have 2 collections now, [col1 and col3]
    harvest(
        instance=Instance(
            name="geovisio_instance_1",
            root_url=external_services.geovisio_instance_1_url,
        ),
        external_services=external_services,
        incremental_harvest=False,
    )

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT content->>'title' AS title FROM collections;").fetchall()
        assert r and set(t["title"] for t in r) == {"collection 1", "collection 3"}
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 6  # 2*3 items now

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 3

        # again there is only 5 collection_harvest, since the old rows should have been deleted
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, harvests[-1]["id"]): {"status": "updated"},
            (col3_id, harvests[-1]["id"]): {"status": "created"},
        }


def test_full_harvest_two_instances(
    external_services: ExternalServicesUrl,
    geovision_instance_1_auth_headers,
    geovision_instance_2_auth_headers,
):
    col1 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[
            FIXTURE_DIR / "col1" / "e1.jpg",
            FIXTURE_DIR / "col1" / "e2.jpg",
            FIXTURE_DIR / "col1" / "e3.jpg",
        ],
    )
    col1_id = _get_col_id(col1)

    col2 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 2",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col2_id = _get_col_id(col2)

    col3_instance2 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 3 in instance 2",
        pic_names=[
            FIXTURE_DIR / "col3" / "1.jpg",
            FIXTURE_DIR / "col3" / "2.jpg",
            FIXTURE_DIR / "col3" / "2.jpg",
        ],
    )
    col3_id = _get_col_id(col3_instance2)

    instances = [
        Instance(
            name="geovisio_instance_1",
            root_url=external_services.geovisio_instance_1_url,
        ),
        Instance(
            name="geovisio_instance_2",
            root_url=external_services.geovisio_instance_2_url,
        ),
    ]
    harvest_instances(
        db=external_services.db_url,
        instances=instances,
        config=Config(host_name=f"{external_services.api_url}/api", incremental_harvest=False),
    )

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT count(*) AS nb_instances FROM instances;").fetchone()
        assert r and r["nb_instances"] == 2
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 3
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 8

        harvests = db.execute(
            "SELECT h.id AS id, i.name AS instance_name, start, finished_at FROM harvests h JOIN instances i on h.instance_id = i.id;"
        ).fetchall()
        harvest_on_instance_1 = [h["id"] for h in harvests if h["instance_name"] == "geovisio_instance_1"]
        harvest_on_instance_2 = [h["id"] for h in harvests if h["instance_name"] == "geovisio_instance_2"]
        assert len(harvest_on_instance_1) == 1 and len(harvest_on_instance_2) == 1
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, harvest_on_instance_1[0]): {"status": "created"},
            (col2_id, harvest_on_instance_1[0]): {"status": "created"},
            (col3_id, harvest_on_instance_2[0]): {"status": "created"},
        }

        # min/max datetimes on each instances should have been updated
        r = db.execute("SELECT name, min_datetime, max_datetime FROM instances ORDER BY name;").fetchall()
        assert r == [
            {
                "max_datetime": datetime(2022, 10, 19, 7, 56, 38, tzinfo=UTC),
                "min_datetime": datetime(2015, 4, 25, 13, 36, 17, tzinfo=UTC),
                "name": "geovisio_instance_1",
            },
            {
                "max_datetime": datetime(2021, 7, 29, 9, 16, 56, tzinfo=UTC),
                "min_datetime": datetime(2021, 7, 29, 9, 16, 54, tzinfo=UTC),
                "name": "geovisio_instance_2",
            },
        ]
        # we should also have filled the providers table and each collection should be associated to a provider
        col_providers = db.execute(
            "SELECT p.name, i.name AS instance_name, c.id AS collection_id FROM collections c JOIN providers p on p.id = c.provider_id JOIN instances i ON i.id = p.instance_id;"
        ).fetchall()
        col_providers = {cp.pop("collection_id"): cp for cp in col_providers}
        assert col_providers == {
            col1_id: {
                "name": "Default account",
                "instance_name": "geovisio_instance_1",
            },
            col2_id: {
                "name": "Default account",
                "instance_name": "geovisio_instance_1",
            },
            col3_id: {
                "name": "Default account",
                "instance_name": "geovisio_instance_2",
            },
        }

    # delete col1 and one item from col3 and re-harvest all instances
    requests.delete(col1, headers=geovision_instance_1_auth_headers).raise_for_status()

    pic_to_delete_resp = requests.get(f"{col3_instance2}/items")
    pic_to_delete_resp.raise_for_status()
    pic_to_delete = pic_to_delete_resp.json()["features"][1]["id"]
    requests.delete(
        f"{col3_instance2}/items/{pic_to_delete}",
        headers=geovision_instance_2_auth_headers,
    ).raise_for_status()

    harvest_instances(
        db=external_services.db_url,
        instances=instances,
        config=Config(host_name=f"{external_services.api_url}/api", incremental_harvest=False),
    )

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT count(*) AS nb_instances FROM instances;").fetchone()
        assert r and r["nb_instances"] == 2
        r = db.execute("SELECT content->>'title' AS title FROM collections;").fetchall()
        assert r and set(t["title"] for t in r) == {
            "collection 2",
            "collection 3 in instance 2",
        }

        r = db.execute("SELECT collection_id, count(*) AS nb_items FROM items GROUP BY collection_id;").fetchall()
        assert r and {t["collection_id"]: t["nb_items"] for t in r} == {
            col3_id: 2,
            col2_id: 2,
        }

        harvests = db.execute(
            "SELECT h.id AS id, i.name AS instance_name, start FROM harvests h JOIN instances i on h.instance_id = i.id ORDER BY start ASC;"
        ).fetchall()
        harvest_on_instance_1 = [h["id"] for h in harvests if h["instance_name"] == "geovisio_instance_1"]
        harvest_on_instance_2 = [h["id"] for h in harvests if h["instance_name"] == "geovisio_instance_2"]
        assert len(harvest_on_instance_1) == 2 and len(harvest_on_instance_2) == 2
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col2_id, harvest_on_instance_1[-1]): {"status": "updated"},
            (col3_id, harvest_on_instance_2[-1]): {"status": "updated"},
        }


def test_incremental_harvest(
    external_services: ExternalServicesUrl,
    geovision_instance_1_auth_headers,
):
    """
    Harvest several times an instance supporting incremental_harvest harvest.
    After the first harvest, each harvest will only collect collections that have been created/updated/deleted since last harvest
    """
    col1 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[
            FIXTURE_DIR / "col1" / "e1.jpg",
            FIXTURE_DIR / "col1" / "e2.jpg",
            FIXTURE_DIR / "col1" / "e3.jpg",
        ],
    )
    col1_id = _get_col_id(col1)

    col2 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 2",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col2_id = _get_col_id(col2)

    instance = Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url)
    harvest(instance=instance, external_services=external_services)

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT name FROM instances;").fetchall()
        assert r and r == [{"name": "geovisio_instance_1"}]
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 2
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 5

        harvests = db.execute("SELECT id, instance_id, start FROM harvests;").fetchall()
        assert len(harvests) == 1
        first_harvest = harvests[0]
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert r and len(col_harvests) == 2
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, first_harvest["id"]): {"status": "created"},
            (col2_id, first_harvest["id"]): {"status": "created"},
        }

    # Harvesting again the instance should not change the harvested items (but this should change the `harvests` and `collection_harvest` that log the harvests)
    harvest(instance=instance, external_services=external_services)

    # since nothing has changed in the crawled instances, nothing should have changed in the database,
    # apart from a new harvest being created (but without `collection_harvest` attached)
    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT name FROM instances;").fetchall()
        assert r and r == [{"name": "geovisio_instance_1"}]
        r = db.execute("SELECT count(*) AS nb_cols FROM collections;").fetchone()
        assert r and r["nb_cols"] == 2
        r = db.execute("SELECT count(*) AS nb_items FROM items;").fetchone()
        assert r and r["nb_items"] == 5

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 2
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, first_harvest["id"]): {"status": "created"},
            (col2_id, first_harvest["id"]): {"status": "created"},
        }

    # delete 1 collection and add another one to geovisio
    col3 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 3",
        pic_names=[FIXTURE_DIR / "col3" / "1.jpg", FIXTURE_DIR / "col3" / "2.jpg"],
    )
    col3_id = _get_col_id(col3)

    requests.delete(col2, headers=geovision_instance_1_auth_headers).raise_for_status()

    # harvest again, we should only have 2 collections now, [col1 and col3]
    harvest(instance=instance, external_services=external_services)

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT content->>'title' AS title FROM collections;").fetchall()
        assert r and set(t["title"] for t in r) == {"collection 1", "collection 3"}
        r = db.execute("SELECT collection_id, count(*) AS nb_items FROM items GROUP BY collection_id;").fetchall()
        assert r and {t["collection_id"]: t["nb_items"] for t in r} == {
            col1_id: 3,
            col3_id: 2,
        }

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 3

        # for incremental_harvest harvest, we keep the history of the collection_harvest
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, harvests[0]["id"]): {"status": "created"},
            (col3_id, harvests[-1]["id"]): {"status": "created"},
        }

    # update one collection by adding a picture to it
    add_picture_to_collection(col3, pic=FIXTURE_DIR / "col3" / "3.jpg", position=3)

    wait_until_geovisio_collection_ready(
        col3,
        wanted_state={
            "sequence_status": "ready",
            "items": {i: "ready" for i in range(1, 4)},
        },
    )

    harvest(instance=instance, external_services=external_services)

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT content->>'title' AS title FROM collections;").fetchall()
        assert r and set(t["title"] for t in r) == {"collection 1", "collection 3"}
        r = db.execute("SELECT collection_id, count(*) AS nb_items FROM items GROUP BY collection_id;").fetchall()
        assert r and {t["collection_id"]: t["nb_items"] for t in r} == {
            col1_id: 3,
            col3_id: 3,
        }

        harvests = db.execute("SELECT id, instance_id, start FROM harvests ORDER BY start;").fetchall()
        assert len(harvests) == 4

        # for incremental_harvest harvest, we keep the history of the collection_harvest
        col_harvests = db.execute("SELECT collection_id, harvest_id, status FROM collection_harvest;").fetchall()
        assert {(ch.pop("collection_id"), ch.pop("harvest_id")): ch for ch in col_harvests} == {
            (col1_id, harvests[0]["id"]): {"status": "created"},
            (col3_id, harvests[-2]["id"]): {"status": "created"},
            (col3_id, harvests[-1]["id"]): {"status": "updated"},
        }


def test_add_collection_without_provider_id(external_services: ExternalServicesUrl):
    """Some geovisio version might not have a provider ID in the collection, we should still be able to harvest them"""
    collection_content = {
        "id": "b99b5b1c-9e00-4e57-87f9-381d3837cbe5",
        "type": "Collection",
        "links": [
            {
                "rel": "items",
                "href": "http://localhost:9000/api/collections/b99b5b1c-9e00-4e57-87f9-381d3837cbe5/items",
                "type": "application/geo+json",
                "title": "Pictures in this sequence",
            },
            {
                "rel": "parent",
                "href": "http://localhost:9000/api/",
                "type": "application/json",
                "title": "Instance catalog",
            },
            {
                "rel": "root",
                "href": "http://localhost:9000/api/",
                "type": "application/json",
                "title": "Instance catal²og",
            },
            {
                "rel": "self",
                "href": "http://localhost:9000/api/collections/b99b5b1c-9e00-4e57-87f9-381d3837cbe5",
                "type": "application/json",
                "title": "Metadata of this sequence",
            },
        ],
        "title": "collection 3 in instance 2",
        "extent": {
            "spatial": {
                "bbox": [
                    [
                        1.9191854417991367,
                        49.00688961988304,
                        1.9191896230005276,
                        49.0068986458004,
                    ]
                ]
            },
            "temporal": {"interval": [["2021-07-29T09:16:54+00:00", "2021-07-29T09:16:56+00:00"]]},
        },
        "created": "2024-07-04T09:12:41.647200+00:00",
        "license": "proprietary",
        "updated": "2024-07-04T09:12:41.867767+00:00",
        "keywords": ["pictures", "collection 3 in instance 2"],
        "providers": [{"name": "Default account", "roles": ["producer"]}],
        "description": "A sequence of geolocated pictures",
        "stats:items": {"count": 3},
        "stac_version": "1.0.0",
        "stac_extensions": ["https://stac-extensions.github.io/stats/v0.2.0/schema.json"],
    }

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        instance_id = db.execute(
            "INSERT INTO instances (name, url) VALUES (%(name)s, %(url)s)RETURNING ID;",
            {"name": "instance", "url": "http://some_panoramax.com"},
        ).fetchone()["id"]

        col_id = db.execute(
            """INSERT INTO collections (content, instance_id)
        VALUES (%(col)s, %(instance)s)
        RETURNING id;
        """,
            {"col": json.dumps(collection_content), "instance": instance_id},
        ).fetchone()["id"]

        # we should also have filled the collection, but without a provider_id
        col_providers = db.execute(
            """SELECT p.name AS provider_name, i.name AS instance_name, c.id AS collection_id, c.provider_id FROM collections c
            LEFT JOIN providers p on p.id = c.provider_id
            LEFT JOIN instances i ON i.id = c.instance_id;"""
        ).fetchall()
        col_providers = {cp.pop("collection_id"): cp for cp in col_providers}
        assert col_providers == {
            col_id: {
                "provider_name": None,
                "provider_id": None,
                "instance_name": "instance",
            },
        }


def test_full_harvest_with_max_items_by_collection_limit(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers):
    """We can use paginations for collections with lots of items"""
    col1 = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[
            FIXTURE_DIR / "col1" / "e1.jpg",
            FIXTURE_DIR / "col1" / "e2.jpg",
            FIXTURE_DIR / "col1" / "e3.jpg",
        ],
    )
    col1_id = _get_col_id(col1)

    harvest_instances(
        db=external_services.db_url,
        instances=[
            Instance(
                name="geovisio_instance_1",
                root_url=external_services.geovisio_instance_1_url,
            )
        ],
        config=Config(
            host_name=f"{external_services.api_url}/api",
            incremental_harvest=False,
            max_items_per_collection=1,
        ),  # we fetch the items 1 by 1
    )

    with psycopg.connect(external_services.db_url, row_factory=dict_row) as db:
        r = db.execute("SELECT count(*) AS nb_instances FROM instances;").fetchone()
        assert r and r["nb_instances"] == 1
        r = db.execute("SELECT content->>'title' AS title FROM collections;").fetchall()
        assert r and set(t["title"] for t in r) == {
            "collection 1",
        }

        r = db.execute("SELECT collection_id, count(*) AS nb_items FROM items GROUP BY collection_id;").fetchall()
        assert r and {t["collection_id"]: t["nb_items"] for t in r} == {col1_id: 3}  # all the items should have been fetched


def test_retry_on_items():
    with aioresponses() as mock:
        url = "https://panoramax_instance.org/collections/00000000-0000-0000-0000-000000000000/items?limit=2000"
        mock.get(url, status=500)
        mock.get(url, exception=aiohttp.ClientConnectionError)
        mock.get(
            url,
            status=200,
            body='{"features": [{"id": "00000000-0000-0000-0000-000000000000"}], "links": []}',
        )

        import harvester

        async def get_all_items():
            items_gen = harvester.harvest.get_items(
                "https://panoramax_instance.org/collections/00000000-0000-0000-0000-000000000000",
                Config(host_name="https://panoramax_instance.org/api"),
            )
            items = []
            async for item in items_gen:
                items.append(item)
            return items

        items = asyncio.run(get_all_items())

        assert list(items) == [{"id": "00000000-0000-0000-0000-000000000000"}]

        assert len(mock.requests) == 1

        assert len(list(mock.requests.values())[0]) == 3  # should have been called 3 times, until it get a sucessful response assert


def test_retry_on_items_failure():
    """If the call fail more than twice, the get_items function should raise the underlying exception"""
    with aioresponses() as mock:
        url = "https://panoramax_instance.org/collections/00000000-0000-0000-0000-000000000000/items?limit=2000"
        mock.get(url, status=500)
        mock.get(url, exception=aiohttp.ClientConnectionError)
        mock.get(url, exception=aiohttp.ClientConnectionError)
        # the last one should never be called
        mock.get(
            url,
            status=200,
            body='{"features": [{"id": "00000000-0000-0000-0000-000000000000"}], "links": []}',
        )

        import harvester

        async def get_all_items():
            items_gen = harvester.harvest.get_items(
                "https://panoramax_instance.org/collections/00000000-0000-0000-0000-000000000000",
                Config(host_name="https://panoramax_instance.org/api"),
            )
            items = []
            async for item in items_gen:
                items.append(item)
            return items

        with pytest.raises(aiohttp.ClientConnectionError):
            asyncio.run(get_all_items())

        assert len(mock.requests) == 1

        assert len(list(mock.requests.values())[0]) == 3  # should have been called 3 times, until it get a sucessful response assert
