import asyncio
import pytest
from python_on_whales import DockerClient, utils
from pathlib import Path
from typing import List, Callable
import psycopg
import harvester
from typing import Generator
from dataclasses import dataclass
import time
import requests


BASE_DIR = Path(__file__).parent
FIXTURE_DIR = Path(__file__).parent / "fixtures"
BASE_COMPOSE_FILES: List[utils.ValidPath] = [
    BASE_DIR.parent.parent / "docker-compose.yml",
    BASE_DIR / "docker-compose-geovisio.yml",
]


def pytest_addoption(parser):
    parser.addoption("--external-geovisio_instance_1-url", action="store")
    parser.addoption("--external-geovisio_instance_2-url", action="store")
    parser.addoption("--external-api-url", action="store")
    parser.addoption("--external-db-url", action="store")
    parser.addoption("--external-geovisio_instance_1-token-url", action="store")
    parser.addoption("--external-geovisio_instance_2-token-url", action="store")


@dataclass
class ExternalServicesUrl:
    geovisio_instance_1_url: str
    geovisio_instance_2_url: str
    api_url: str
    db_url: str
    geovisio_instance_1_token_url: str
    geovisio_instance_2_token_url: str


@pytest.fixture(scope="session", autouse=True)
def external_services(pytestconfig) -> Generator[ExternalServicesUrl, None, None]:
    geovisio_instance_1_url = pytestconfig.getoption("--external-geovisio_instance_1-url")
    geovisio_instance_2_url = pytestconfig.getoption("--external-geovisio_instance_2-url")
    api_url = pytestconfig.getoption("--external-api-url")
    db_url = pytestconfig.getoption("--external-db-url")
    geovisio_instance_1_token_url = pytestconfig.getoption("--external-geovisio_instance_1-token-url")
    geovisio_instance_2_token_url = pytestconfig.getoption("--external-geovisio_instance_2-token-url")

    params = [
        geovisio_instance_1_url,
        geovisio_instance_2_url,
        api_url,
        db_url,
        geovisio_instance_1_token_url,
        geovisio_instance_2_token_url,
    ]
    if any(params) and not all(params):
        raise Exception(f"Either no external parameter should be provided (to let the tests run all the needed dockers, or all ({params}))")

    if any(params):
        yield ExternalServicesUrl(
            geovisio_instance_1_url=geovisio_instance_1_url,
            geovisio_instance_2_url=geovisio_instance_2_url,
            api_url=api_url,
            db_url=db_url,
            geovisio_instance_1_token_url=geovisio_instance_1_token_url,
            geovisio_instance_2_token_url=geovisio_instance_2_token_url,
        )
        return

    docker = DockerClient(compose_files=BASE_COMPOSE_FILES)
    try:
        docker.compose.up(detach=True, wait=True, build=True)

        geovisio_1_host, geovisio_1_port = docker.compose.port("geovisio_instance_1", 5000)
        geovisio_2_host, geovisio_2_port = docker.compose.port("geovisio_instance_2", 5000)
        db_host, db_port = docker.compose.port("database", 5432)
        api_host, api_port = docker.compose.port("api", 9000)
        geovisio_instance_1_token_host, geovisio_instance_1_token_port = docker.compose.port("geovisio_instance_1_token", 5100)
        geovisio_instance_2_token_host, geovisio_instance_2_token_port = docker.compose.port("geovisio_instance_2_token", 5100)

        yield ExternalServicesUrl(
            geovisio_instance_1_url=f"http://{geovisio_1_host}:{geovisio_1_port}/api",
            geovisio_instance_2_url=f"http://{geovisio_2_host}:{geovisio_2_port}/api",
            api_url=f"http://{api_host}:{api_port}",
            db_url=f"postgresql://username:password@{db_host}:{db_port}/panoramax",
            geovisio_instance_1_token_url=f"http://{geovisio_instance_1_token_host}:{geovisio_instance_1_token_port}",
            geovisio_instance_2_token_url=f"http://{geovisio_instance_2_token_host}:{geovisio_instance_2_token_port}",
        )
    finally:
        docker.compose.down()


def geovisio_instance_auth_headers(query):
    """Get geovisio instance admin token to be able to delete collections"""
    r = requests.get(query)
    r.raise_for_status()
    t = r.text.strip()
    return {"Authorization": f"Bearer {t}"}


@pytest.fixture(scope="session")
def geovision_instance_1_auth_headers(external_services):
    return geovisio_instance_auth_headers(f"{external_services.geovisio_instance_1_token_url}/geovisio_admin_token")


@pytest.fixture(scope="session")
def geovision_instance_2_auth_headers(external_services):
    """Get geovisio instance 1 admin token to be able to delete collections"""
    return geovisio_instance_auth_headers(f"{external_services.geovisio_instance_2_token_url}/geovisio_admin_token")


def cleanup_db(external_services):
    with psycopg.connect(external_services.db_url) as db:
        db.execute(
            """
        TRUNCATE table collections CASCADE;
        TRUNCATE table harvests CASCADE;
        TRUNCATE table instances CASCADE;
        TRUNCATE table providers CASCADE;
        """
        )


@pytest.fixture(scope="module", autouse=True)
def cleanup_db_fixture(external_services):
    cleanup_db(external_services)


def wait_until(func: Callable[[], bool], timeout: float = 10, period: float = 0.25, message=""):
    mustend = time.time() + timeout
    while time.time() < mustend:
        if func():
            print(f"{message} ready !")
            return True
        print(f"waiting for {message} ...")
        time.sleep(period)
    assert False, f"waited too long for {message}"


def get_geovisio_collection_status(col_location: str):
    status = requests.get(f"{col_location}/geovisio_status")
    status.raise_for_status()
    return {
        "sequence_status": status.json()["status"],
        "items": {p["rank"]: p["status"] for p in status.json()["items"]},
    }


def wait_until_geovisio_collection_ready(col_location: str, wanted_state):
    def check():
        current_state = get_geovisio_collection_status(col_location)
        print(f"current state = {current_state}")
        return current_state == wanted_state

    return wait_until(check, message=f"collection {col_location}")


def cleanup_geovisio_instance(instance_url, auth_headers):
    """Delete all collections from a geovisio instance"""
    s = requests.get(f"{instance_url}/collections")
    s.raise_for_status()
    for c in s.json()["collections"]:
        self_link = next(li["href"] for li in c["links"] if li["rel"] == "self")
        delete_col = requests.delete(self_link, headers=auth_headers)
        delete_col.raise_for_status()


def add_picture_to_collection(collection_location: str, pic: Path, position: int):
    """
    Upload a picture to a collection in a given position
    """
    with open(pic, "rb") as pic_file:
        picture_response = requests.post(
            f"{collection_location}/items",
            files={"position": (None, position), "picture": (pic.name, pic_file)},
        )  # type: ignore
        picture_response.raise_for_status()


def import_collection_to_geovisio(instance_url: str, title: str, pic_names: List[Path]) -> str:
    """
    Create a collection in a geovisio instance and import some pics in it.
    Returns the collection location
    """
    col = requests.post(f"{instance_url}/collections", data={"title": title})
    col.raise_for_status()
    col_location = col.headers["Location"]

    for i, p in enumerate(pic_names, start=1):
        add_picture_to_collection(col_location, p, i)

    wait_until_geovisio_collection_ready(
        col_location,
        wanted_state={
            "sequence_status": "ready",
            "items": {i: "ready" for i, _ in enumerate(pic_names, start=1)},
        },
    )

    return col_location


def harvest(instance, external_services, incremental_harvest=True):
    print(f"Harvesting {instance.name}")
    harvester.harvest_instances(
        db=external_services.db_url,
        instances=[instance],
        config=harvester.Config(
            host_name=f"{external_services.api_url}/api",
            incremental_harvest=incremental_harvest,
        ),
    )
    print(f"Finished harvesting {instance.name}")
    # after harvest, we vacuum analyze to get nice stats
    with psycopg.connect(external_services.db_url, autocommit=True) as db:
        db.execute("VACUUM ANALYZE collections;")
        db.execute("VACUUM ANALYZE items;")
