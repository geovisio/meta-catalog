import stac_api_validator.validations
import stac_api_validator
import openapi_spec_validator
import requests
from ..conftest import harvest, import_collection_to_geovisio, ExternalServicesUrl, FIXTURE_DIR, cleanup_geovisio_instance
import pytest
from harvester import Instance


@pytest.fixture(scope="module", autouse=True)
def cleanup_geovisio_instance_1(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers):
    """Delete all collections from geovisio instance 1"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)


@pytest.fixture(scope="module", autouse=True)
def import_pic_in_geovisio_instance_1(external_services: ExternalServicesUrl, cleanup_geovisio_instance_1) -> str:
    """import a collection into geovisio"""
    return import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col1" / "e1.jpg", FIXTURE_DIR / "col1" / "e2.jpg", FIXTURE_DIR / "col1" / "e3.jpg"],
    )


# all tests in this module can know that the geovisio_instance_1 has been harvested
# Note: no test in this module should load data, If a test needs to change the data loaded, it should be in another module
# This way, the tests in this module can be idempotent and be run in a random order or in parallele
@pytest.fixture(scope="module", autouse=True)
def harvest_instance_1(external_services, import_pic_in_geovisio_instance_1):
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )


def test_open_api(external_services):
    r = requests.get(f"{external_services.api_url}/openapi.json")
    assert r.status_code == 200
    schema = r.json()

    openapi_spec_validator.validate(schema)


def test_stac_conformance(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections")
    assert r.status_code == 200
    cols = r.json()["collections"]
    assert len(cols) > 0
    first_col = cols[0]["id"]
    geom = '{"type": "Polygon", "coordinates": [[[-4.04,51.30],[-4.04,42.05],[9.19,42.05],[9.19,51.30],[-4.04,51.30]]]}'

    _warnings, errors = stac_api_validator.validations.validate_api(
        root_url=f"{external_services.api_url}/api",
        ccs_to_validate=["core", "collections", "browseable", "filter"],
        collection=first_col,
        geometry=geom,
        auth_bearer_token=None,
        auth_query_parameter=None,
        fields_nested_property=None,
        validate_pagination=True,
        query_config=stac_api_validator.validations.QueryConfig(
            query_comparison_field=None,
            query_eq_value=None,
            query_neq_value=None,
            query_lt_value=None,
            query_lte_value=None,
            query_gt_value=None,
            query_gte_value=None,
            query_substring_field=None,
            query_starts_with_value=None,
            query_ends_with_value=None,
            query_contains_value=None,
            query_in_field=None,
            query_in_values=None,
        ),
        transaction_collection=None,
        headers={},
    )

    assert not errors
