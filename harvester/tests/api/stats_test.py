import psycopg
import requests
from ..conftest import (
    harvest,
    import_collection_to_geovisio,
    ExternalServicesUrl,
    FIXTURE_DIR,
    cleanup_geovisio_instance,
    cleanup_db,
)
from .conftest import Dataset
import pytest
from harvester import Instance


# Tests in this module needs to import a dataset as a fixture to get the needed data loaded.
# Check all dataset to know which data is loaded
# Note: no test in this module should load data, if different data are needed, create a new dataset


@pytest.fixture(scope="module")
def cleanup_all_databases(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers, geovision_instance_2_auth_headers):
    """Delete all collections"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)
    cleanup_geovisio_instance(external_services.geovisio_instance_2_url, geovision_instance_2_auth_headers)
    cleanup_db(external_services)


@pytest.fixture(scope="module")
def dataset_1(external_services, cleanup_all_databases):
    """
    This dataset loads 1 collection with 3 pictures in instance 1
    and 2 collections with 2 and 3 pictures in instance 2
    """
    col1_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col1" / "e1.jpg", FIXTURE_DIR / "col1" / "e2.jpg", FIXTURE_DIR / "col1" / "e3.jpg"],
    )
    col1_id = col1_location.split("/")[-1]
    col2_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 2",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col2_id = col2_location.split("/")[-1]
    col3_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 3",
        pic_names=[FIXTURE_DIR / "col3" / "1.jpg", FIXTURE_DIR / "col3" / "2.jpg", FIXTURE_DIR / "col3" / "3.jpg"],
    )
    col3_id = col3_location.split("/")[-1]
    # harvest both instances
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )
    harvest(
        instance=Instance(name="geovisio_instance_2", root_url=external_services.geovisio_instance_2_url),
        external_services=external_services,
    )
    with psycopg.connect(external_services.db_url) as db:
        # to get something consistent over time, we change the uploading time to a fixed date
        db.execute(
            "UPDATE items SET content = jsonb_set(content, '{properties,created}', '\"2023-02-09T10:12:37.356010+00:00\"', false) WHERE collection_id = %s;",
            [col1_id],
        )
        db.execute(
            "UPDATE collections SET content = jsonb_set(content, '{created}', '\"2023-02-09T10:12:37.356010+00:00\"', false) WHERE id = %s;",
            [col1_id],
        )
        db.execute(
            "UPDATE items SET content = jsonb_set(content, '{properties,created}', '\"2024-01-09T10:12:37.356010+00:00\"', false) WHERE collection_id = %s;",
            [col2_id],
        )
        db.execute(
            "UPDATE collections SET content = jsonb_set(content, '{created}', '\"2024-01-09T10:12:37.356010+00:00\"', false) WHERE id = %s;",
            [col2_id],
        )
        db.execute(
            "UPDATE items SET content = jsonb_set(content, '{properties,created}', '\"2023-02-19T12:12:37.356010+00:00\"', false) WHERE collection_id = %s;",
            [col3_id],
        )
        db.execute(
            "UPDATE collections SET content = jsonb_set(content, '{created}', '\"2023-02-19T12:12:37.356010+00:00\"', false) WHERE id = %s;",
            [col3_id],
        )

        # refresh stats materialized views
        db.execute("SELECT stats.refresh_all_views();")

        provider_ids = db.execute("SELECT p.id, p.name, i.name FROM providers p JOIN instances i on i.id = p.instance_id;").fetchall()

        return Dataset(
            provider_ids={f"{name}@{instance}": str(id) for id, name, instance in provider_ids},
            collection_ids={"col1": col1_id, "col2": col2_id, "col3": col3_id},
        )


def test_stat_root_endpoint(external_services, dataset_1):
    r = requests.get(f"{external_services.api_url}/api/stats")
    assert r.status_code == 200, r.text
    j = r.json()

    # the broken down stats sum should be equals to the main stats
    for k in ["nb_pictures", "pictures_original_size", "nb_contributors", "approximated_coverage_km", "collections_length_km"]:
        assert sum(v[k] for v in j["stats_by_instance"].values()) == j["generic_stats"][k]

    # The last stats_by_upload_month should be the instance stats
    max_month = max(month for month in j["stats_by_upload_month"].keys())
    for instance, last_month_instance_stats in j["stats_by_upload_month"][max_month].items():
        assert j["stats_by_instance"][instance]["nb_pictures"] == last_month_instance_stats["nb_pictures"]
        assert j["stats_by_instance"][instance]["pictures_original_size"] == last_month_instance_stats["pictures_original_size"]
        assert j["stats_by_instance"][instance]["approximated_coverage_km"] == last_month_instance_stats["approximated_coverage_km"]
        assert j["stats_by_instance"][instance]["collections_length_km"] == last_month_instance_stats["collections_length_km"]
        # assert j["stats_by_instance"][instance]["nb_contributors"] == last_month_instance_stats["nb_contributors"] # TODO

    assert j == {
        "generic_stats": {
            "nb_pictures": 8,
            "pictures_original_size": 1835133,
            "nb_contributors": 2,
            "approximated_coverage_km": 0,
            "collections_length_km": 0,
        },
        "stats_by_instance": {
            "geovisio_instance_1": {
                "nb_pictures": 3,
                "pictures_original_size": 1536130,
                "nb_contributors": 1,
                "approximated_coverage_km": 0,
                "collections_length_km": 0,
            },
            "geovisio_instance_2": {
                "nb_pictures": 5,
                "pictures_original_size": 299003,
                "nb_contributors": 1,
                "approximated_coverage_km": 0,
                "collections_length_km": 0,
            },
        },
        "stats_by_upload_month": {
            "2023-02": {
                "geovisio_instance_2": {
                    "nb_pictures": 3,
                    "pictures_original_size": 238201,
                    "nb_contributors": 0,
                    "approximated_coverage_km": 0,
                    "collections_length_km": 0,
                    "nb_active_contributors": 1,
                },
                "geovisio_instance_1": {
                    "nb_pictures": 3,
                    "pictures_original_size": 1536130,
                    "nb_contributors": 0,
                    "approximated_coverage_km": 0,
                    "collections_length_km": 0,
                    "nb_active_contributors": 1,
                },
            },
            "2024-01": {
                "geovisio_instance_2": {
                    "nb_pictures": 5,
                    "pictures_original_size": 299003,
                    "nb_contributors": 0,
                    "approximated_coverage_km": 0,
                    "collections_length_km": 0,
                    "nb_active_contributors": 1,
                }
            },
        },
    }


def test_stat_by_contributors(external_services, dataset_1):
    r = requests.get(f"{external_services.api_url}/api/stats/by_contributor")
    assert r.status_code == 200, r.text
    j = r.json()
    assert j == {
        "stats_by_contributor": [
            {
                "instance_name": "geovisio_instance_2",
                "km_captured": 0,
                "name": "Default account",
                "id": dataset_1.provider_ids["Default account@geovisio_instance_2"],
                "nb_items": 5,
                "history": {"last_7_days": {"nb_items": 0, "km_captured": 0}, "last_30_days": {"nb_items": 0, "km_captured": 0}},
            },
            {
                "instance_name": "geovisio_instance_1",
                "km_captured": 0,
                "name": "Default account",
                "id": dataset_1.provider_ids["Default account@geovisio_instance_1"],
                "nb_items": 3,
                "history": {"last_7_days": {"nb_items": 0, "km_captured": 0}, "last_30_days": {"nb_items": 0, "km_captured": 0}},
            },
        ],
    }
