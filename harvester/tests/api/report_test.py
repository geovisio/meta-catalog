import psycopg
import requests

from .conftest import Dataset
from ..conftest import harvest, import_collection_to_geovisio, ExternalServicesUrl, FIXTURE_DIR, cleanup_geovisio_instance, cleanup_db
import pytest
from harvester import Instance


@pytest.fixture(scope="module")
def cleanup_all_databases(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers, geovision_instance_2_auth_headers):
    """Delete all collections"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)
    cleanup_geovisio_instance(external_services.geovisio_instance_2_url, geovision_instance_2_auth_headers)
    cleanup_db(external_services)


@pytest.fixture(scope="module")
def dataset_1(external_services, cleanup_all_databases):
    """
    This dataset loads 1 collection with 3 pictures in instance 1
    and 2 collections with 2 and 3 pictures in instance 2
    """
    loc = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col_1_id = loc.split("/")[-1]
    loc = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 3",
        pic_names=[
            FIXTURE_DIR / "col3" / "1.jpg",
            FIXTURE_DIR / "col3" / "2.jpg",
            FIXTURE_DIR / "col3" / "3.jpg",
            FIXTURE_DIR / "col3" / "4.jpg",
            FIXTURE_DIR / "col3" / "5.jpg",
        ],
    )
    col_2_id = loc.split("/")[-1]
    # harvest both instances
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )
    harvest(
        instance=Instance(name="geovisio_instance_2", root_url=external_services.geovisio_instance_2_url),
        external_services=external_services,
    )
    # update the instance url for the api to be able to query them, they need their docker url
    with psycopg.connect(external_services.db_url) as db:
        db.execute(
            "UPDATE instances SET url = 'http://geovisio_instance_1:5000/api' WHERE url = %s;", [external_services.geovisio_instance_1_url]
        )
        db.execute(
            "UPDATE instances SET url = 'http://geovisio_instance_2:5000/api' WHERE url = %s;", [external_services.geovisio_instance_2_url]
        )

    return Dataset(
        provider_ids={},
        collection_ids={"col1": col_1_id, "col2": col_2_id},
    )


def get_first_pic_id(db_url: str, collection_id: str):
    with psycopg.connect(db_url) as db:
        with db.cursor() as cursor:
            cursor.execute(
                "SELECT id FROM items WHERE collection_id = %s LIMIT 1;",
                [collection_id],
            )
            return str(cursor.fetchone()[0])


def test_report_picture(external_services, dataset_1):
    pic_id = get_first_pic_id(external_services.db_url, dataset_1.collection_ids["col1"])
    r = requests.post(f"{external_services.api_url}/api/reports", json={"picture_id": pic_id, "issue": "privacy"})

    assert r.status_code == 200
    print(r.json())
    assert "id" in r.json()

    assert r.json()["issue"] == "privacy"
    assert r.json()["picture_id"] == pic_id


def test_report_sequence(external_services, dataset_1):
    r = requests.post(
        f"{external_services.api_url}/api/reports",
        json={"sequence_id": dataset_1.collection_ids["col1"], "issue": "blur_missing", "reporter_email": "toto@toto.com"},
    )
    assert r.status_code == 200
    assert "id" in r.json()
    assert r.json()["issue"] == "blur_missing"
    assert r.json()["sequence_id"] == dataset_1.collection_ids["col1"]


def test_report_both_pic_and_seq(external_services, dataset_1):
    pic_id = get_first_pic_id(external_services.db_url, dataset_1.collection_ids["col1"])
    r = requests.post(
        f"{external_services.api_url}/api/reports",
        json={"picture_id": pic_id, "sequence_id": dataset_1.collection_ids["col1"], "issue": "copyright"},
    )
    assert r.status_code == 200
    assert r.json()["issue"] == "copyright"
    assert r.json()["picture_id"] == pic_id
    assert r.json()["sequence_id"] == dataset_1.collection_ids["col1"]


def test_report_no_issue(external_services, dataset_1):
    """An issue is mandatory, not providing one is an error"""
    pic_id = get_first_pic_id(external_services.db_url, dataset_1.collection_ids["col1"])
    r = requests.post(
        f"{external_services.api_url}/api/reports",
        json={
            "picture_id": pic_id,
        },
    )
    assert r.status_code == 400

    assert r.text == "Json deserialize error: missing field `issue` at line 1 column 54"


def test_report_more_fields(external_services, dataset_1):
    pic_id = get_first_pic_id(external_services.db_url, dataset_1.collection_ids["col1"])
    r = requests.post(
        f"{external_services.api_url}/api/reports",
        json={"picture_id": pic_id, "issue": "copyright", "pouet": "bla"},
    )
    assert r.status_code == 200
    # the instances does not complain about additional fields
    assert r.json()["issue"] == "copyright"
    assert r.json()["picture_id"] == pic_id
    assert r.json()["picture_id"] == pic_id


def test_report_no_pic_and_seq(external_services, dataset_1):
    r = requests.post(
        f"{external_services.api_url}/api/reports",
        json={
            "issue": "blur_missing",
        },
    )
    assert r.status_code == 400
    assert r.json() == {
        "message": "Invalid parameter: Impossible to send report, either a picture_id or a sequence_id should be set",
        "status": 400,
    }


def test_report_unknown_pic(external_services, dataset_1):
    r = requests.post(
        f"{external_services.api_url}/api/reports",
        json={"picture_id": "00000000-0000-0000-0000-000000000000", "issue": "copyright"},
    )
    assert r.status_code == 404
    assert r.json() == {
        "message": "Feature not found",
        "status": 404,
    }


def test_report_unknown_sequence(external_services, dataset_1):
    r = requests.post(
        f"{external_services.api_url}/api/reports",
        json={"sequence_id": "00000000-0000-0000-0000-000000000000", "issue": "copyright"},
    )
    assert r.status_code == 404, r.text
    assert r.json() == {
        "message": "Collection not found",
        "status": 404,
    }
