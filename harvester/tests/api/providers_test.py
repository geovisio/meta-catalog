import psycopg
import mapbox_vector_tile
import pystac
import requests
from ..conftest import harvest, import_collection_to_geovisio, ExternalServicesUrl, FIXTURE_DIR, cleanup_geovisio_instance, cleanup_db
from .conftest import Dataset
import pytest
from harvester import Instance

# Tests in this module needs to import a dataset as a fixture to get the needed data loaded.
# Check all dataset to know which data is loaded
# Note: no test in this module should load data, if different data are needed, create a new dataset


@pytest.fixture(scope="module")
def cleanup_all_databases(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers, geovision_instance_2_auth_headers):
    """Delete all collections"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)
    cleanup_geovisio_instance(external_services.geovisio_instance_2_url, geovision_instance_2_auth_headers)
    cleanup_db(external_services)


@pytest.fixture(scope="module")
def dataset(external_services, cleanup_all_databases):
    """
    This dataset loads:
    * 1 collection in instance 1, associated to the default user
    * 2 collections in instance 2, associated to the 'bob'
    * 1 collections in instance 2, associated to the 'bobette'

    Note: to avoid too much burden, the users are changed by modifying the harvested database
    """
    col1_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col1" / "e1.jpg", FIXTURE_DIR / "col1" / "e2.jpg", FIXTURE_DIR / "col1" / "e3.jpg"],
    )
    col1_id = col1_location.split("/")[-1]
    col2_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 2",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col2_id = col2_location.split("/")[-1]
    col3_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 3",
        pic_names=[FIXTURE_DIR / "col3" / "1.jpg", FIXTURE_DIR / "col3" / "2.jpg", FIXTURE_DIR / "col3" / "3.jpg"],
    )
    col3_id = col3_location.split("/")[-1]
    col4_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 3",
        pic_names=[FIXTURE_DIR / "col4" / "e5.jpg"],
    )
    col4_id = col4_location.split("/")[-1]
    # harvest both instances
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )
    harvest(
        instance=Instance(name="geovisio_instance_2", root_url=external_services.geovisio_instance_2_url),
        external_services=external_services,
    )
    with psycopg.connect(external_services.db_url) as db:
        instance_ids = db.execute("SELECT id, name FROM instances;").fetchall()
        instance_ids = {name: id for id, name in instance_ids}
        db.execute(
            "UPDATE providers SET name = 'bob' WHERE instance_id = %s",
            [instance_ids["geovisio_instance_2"]],
        )
        bobette_id = "12345678-1234-5678-0000-567812345678"
        db.execute(
            "INSERT INTO providers (id, name, instance_id) VALUES (%s, 'bobette', %s);",
            [bobette_id, instance_ids["geovisio_instance_2"]],
        )
        provider_ids = db.execute("SELECT id, name FROM providers;").fetchall()

        db.execute(
            "UPDATE collections SET provider_id = %(bobette)s WHERE id = %(col4)s;",
            {"bobette": bobette_id, "col4": col4_id},
        )

        return Dataset(
            provider_ids={name: str(id) for id, name in provider_ids},
            collection_ids={"col1": col1_id, "col2": col2_id, "col3": col3_id, "col4": col4_id},
        )


def test_user_search_no_param(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/users/search")
    assert r.status_code == 400
    assert r.json() == {"message": "Query deserialize error: missing field `q`", "status": 400}


def test_user_search(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/users/search?q=bob")
    assert r.status_code == 200
    assert r.json() == {
        "features": [
            {
                "id": dataset.provider_ids["bob"],
                "instance_name": "geovisio_instance_2",
                "label": "bob@geovisio_instance_2",
                "name": "bob",
                "links": [
                    {
                        "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}",
                        "rel": "user-info",
                        "type": "application/json",
                    },
                    {
                        "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}/catalog",
                        "rel": "child",
                        "type": "application/json",
                    },
                ],
            },
            {
                "id": dataset.provider_ids["bobette"],
                "instance_name": "geovisio_instance_2",
                "label": "bobette@geovisio_instance_2",
                "name": "bobette",
                "links": [
                    {
                        "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bobette']}",
                        "rel": "user-info",
                        "type": "application/json",
                    },
                    {
                        "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bobette']}/catalog",
                        "rel": "child",
                        "type": "application/json",
                    },
                ],
            },
        ]
    }


def test_user_search_limit(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/users/search?q=bob&limit=1")
    assert r.status_code == 200
    assert r.json() == {
        "features": [
            {
                "id": dataset.provider_ids["bob"],
                "instance_name": "geovisio_instance_2",
                "label": "bob@geovisio_instance_2",
                "name": "bob",
                "links": [
                    {
                        "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}",
                        "rel": "user-info",
                        "type": "application/json",
                    },
                    {
                        "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}/catalog",
                        "rel": "child",
                        "type": "application/json",
                    },
                ],
            },
        ]
    }


def test_get_user(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}")
    assert r.status_code == 200
    assert r.json() == {
        "id": dataset.provider_ids["bob"],
        "instance_name": "geovisio_instance_2",
        "label": "bob@geovisio_instance_2",
        "name": "bob",
        "links": [
            {
                "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}",
                "rel": "user-info",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}/catalog",
                "rel": "child",
                "type": "application/json",
            },
        ],
    }

    # get 'data' link, we should be able to find bob's 2 collections
    d = next(link for link in r.json()["links"] if link["rel"] == "child")
    r = requests.get(d["href"])
    assert r.status_code == 200
    col_ids = {col["href"].split("/")[-1] for col in r.json()["links"] if col["rel"] == "child"}
    assert col_ids == {dataset.collection_ids["col2"], dataset.collection_ids["col3"]}


def test_get_user_unknown(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/users/00000000-0000-0000-0000-000000000000")
    assert r.status_code == 404
    assert r.json() == {"status": 404, "message": "Provider not found"}


def test_user_json_styles(external_services, dataset):
    user = dataset.provider_ids["bob"]

    r = requests.get(f"{external_services.api_url}/api/users/{user}/map/style.json")

    assert r.status_code == 200
    layers_id = [layer.get("id") for layer in r.json()["layers"] if layer.get("id") is not None]
    assert layers_id == [f"geovisio_{user}_sequences", f"geovisio_{user}_pictures"]
    assert f"geovisio_{user}" in r.json()["sources"]


def _get_tiles_data(response: requests.Response):
    assert response.status_code == 200
    data = mapbox_vector_tile.decode(response.content)
    return data, {
        "nb_pic": len(data.get("pictures", {}).get("features", [])),
        "nb_seq": len(data.get("sequences", {}).get("features", [])),
        "nb_grid": len(data.get("grid", {}).get("features", [])),
    }


def test_global_json_styles(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/map/style.json")

    assert r.status_code == 200
    layers_id = [layer.get("id") for layer in r.json()["layers"] if layer.get("id") is not None]
    assert layers_id == ["geovisio_sequences", "geovisio_pictures", "geovisio_grid"]
    assert "geovisio" in r.json()["sources"]


def test_user_tile(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}/map/15/16558/11252.mvt", allow_redirects=False)

    tile, stats = _get_tiles_data(r)
    assert stats == {"nb_pic": 3, "nb_seq": 1, "nb_grid": 0}
    # but bobette does not have any data here
    r = requests.get(
        f"{external_services.api_url}/api/users/{dataset.provider_ids['bobette']}/map/15/16558/11252.mvt", allow_redirects=False
    )
    assert r.status_code == 204


def test_global_tile_lvl_15(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/map/15/16558/11252.mvt", allow_redirects=False)

    tile, stats = _get_tiles_data(r)
    assert stats == {"nb_pic": 3, "nb_seq": 1, "nb_grid": 0}
    assert tile.get("pictures", {}).get("features", [])[0].get("properties", {}).get("account_id") == dataset.provider_ids["bob"]
    assert tile.get("sequences", {}).get("features", [])[0].get("properties", {}).get("account_id") == dataset.provider_ids["bob"]


def test_global_tile_lvl_14(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/map/14/8279/5626.mvt", allow_redirects=False)

    tile, stats = _get_tiles_data(r)
    assert stats == {"nb_pic": 0, "nb_seq": 1, "nb_grid": 0}  # no pic at level 14
    assert tile.get("sequences", {}).get("features", [])[0]["properties"].get("account_id") == dataset.provider_ids["bob"]


def test_users_endpoint(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/users")
    assert r.status_code == 200
    data = r.json()
    # should be a valid Catalog
    catalog = pystac.Catalog.from_dict(data)
    catalog.validate()

    # we should find all users here
    assert sorted(data["links"], key=lambda li: li["href"]) == sorted(
        [
            {
                "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bobette']}/catalog",
                "rel": "child",
                "title": "Collection for user bobette on geovisio_instance_2",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}/catalog",
                "rel": "child",
                "title": "Collection for user bob on geovisio_instance_2",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/api/users/{dataset.provider_ids['Default account']}/catalog",
                "rel": "child",
                "title": "Collection for user Default account on geovisio_instance_1",
                "type": "application/json",
            },
            {"href": f"{external_services.api_url}/api", "rel": "root", "type": "application/json"},
            {"href": f"{external_services.api_url}/api/users", "rel": "self", "type": "application/json"},
        ],
        key=lambda li: li["href"],
    )


def test_user_collection(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/users/{dataset.provider_ids['bob']}/catalog")
    assert r.status_code == 200
    data = r.json()
    # we should find all collections here
    assert data["description"] == "user's catalog"
    assert data["id"] == f"user:{dataset.provider_ids['bob']}"
    assert data["stac_version"] == "1.1.0"
    assert data["type"] == "Catalog"
    assert [link["rel"] for link in data["links"]] == [
        "first",
        "prev",
        "next",
        "last",
        "root",
        "self",
        "child",
        "child",
    ]

    assert sorted([link for link in data["links"] if link["rel"] == "child"], key=lambda li: li["href"]) == sorted(
        [
            {
                "href": f"{external_services.api_url}/api/collections/{dataset.collection_ids['col2']}",
                "rel": "child",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/api/collections/{dataset.collection_ids['col3']}",
                "rel": "child",
                "type": "application/json",
            },
        ],
        key=lambda li: li["href"],
    )
    # should be a valid Catalog
    catalog = pystac.Catalog.from_dict(data)
    catalog.validate()
