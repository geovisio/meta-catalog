import json
import requests
from ..conftest import (
    harvest,
    import_collection_to_geovisio,
    ExternalServicesUrl,
    FIXTURE_DIR,
    cleanup_geovisio_instance,
)
import pytest
import pystac
import re
from harvester import Instance


@pytest.fixture(scope="module", autouse=True)
def cleanup_geovisio_instance_1(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers):
    """Delete all collections from geovisio instance 1"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)


@pytest.fixture(scope="module", autouse=True)
def import_pic_in_geovisio_instance_1(external_services: ExternalServicesUrl, cleanup_geovisio_instance_1) -> str:
    """import a collection into geovisio"""
    return import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[
            FIXTURE_DIR / "col1" / "e1.jpg",
            FIXTURE_DIR / "col1" / "e2.jpg",
            FIXTURE_DIR / "col1" / "e3.jpg",
        ],
    )


# all tests in this module can know that the geovisio_instance_1 has been harvested
# Note: no test in this module should load data, If a test needs to change the data loaded, it should be in another module
# This way, the tests in this module can be idempotent and be run in a random order or in parallele
@pytest.fixture(scope="module", autouse=True)
def harvest_instance_1(external_services, import_pic_in_geovisio_instance_1):
    harvest(
        instance=Instance(
            name="geovisio_instance_1",
            root_url=external_services.geovisio_instance_1_url,
        ),
        external_services=external_services,
    )


def test_root_endpoint(external_services):
    r = requests.get(f"{external_services.api_url}/api")
    assert r.status_code == 200
    data = r.json()

    # we remove the `extent` field for later, not to bother about the coordinate precision
    extent = data.pop("extent")

    assert extent["temporal"] == {"interval": [["2022-10-19T07:56:34Z", "2022-10-19T07:56:38Z"]]}

    assert [round(float(f), 5) for f in extent["spatial"]["bbox"][0]] == [
        -1.68455,
        48.15506,
        -1.68447,
        48.15508,
    ]

    assert data == {
        "conformsTo": [
            "https://api.stacspec.org/v1.0.0/core",
            "http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/core",
            "http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/geojson",
            "https://api.stacspec.org/v1.0.0/browseable",
            "https://api.stacspec.org/v1.0.0/collections",
            "https://api.stacspec.org/v1.0.0/ogcapi-features",
            "https://api.stacspec.org/v1.0.0/item-search",
        ],
        "description": "This catalog list all geolocated pictures available in this Panoramax instance",
        "id": "panoramax",
        "links": [
            {
                "href": f"{external_services.api_url}/api",
                "rel": "root",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/api",
                "rel": "self",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/openapi.json",
                "rel": "service-desc",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/redoc",
                "rel": "service-doc-redoc",
                "type": "text/html",
            },
            {
                "href": f"{external_services.api_url}/api/docs/swagger",
                "rel": "service-doc",
                "type": "text/html",
            },
            {
                "href": f"{external_services.api_url}/api/collections",
                "rel": "data",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/api/users",
                "rel": "child",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/api/users/search",
                "rel": "user-search",
                "type": "application/json",
            },
            {
                "href": f"{external_services.api_url}/api/search",
                "rel": "search",
                "type": "application/geo+json",
            },
            {
                "href": external_services.api_url + "/api/map/{z}/{x}/{y}.mvt",
                "rel": "xyz",
                "title": "Pictures and sequences vector tiles",
                "type": "application/vnd.mapbox-vector-tile",
            },
            {
                "href": f"{external_services.api_url}/api/map/style.json",
                "rel": "xyz-style",
                "title": "MapLibre Style JSON",
                "type": "application/json",
            },
            {
                "href": external_services.api_url + "/api/users/{userId}/map/{z}/{x}/{y}.mvt",
                "rel": "user-xyz",
                "title": "Pictures and sequences vector tiles for a given user",
                "type": "application/vnd.mapbox-vector-tile",
            },
            {
                "href": f"{external_services.api_url}" + "/api/users/{userId}/map/style.json",
                "rel": "user-xyz-style",
                "title": "MapLibre Style JSON for a given user",
                "type": "application/json",
            },
            {
                "href": external_services.api_url + "/api/collections/{id}/thumb.jpg",
                "rel": "collection-preview",
                "title": "Thumbnail URL for a given sequence",
                "type": "image/jpeg",
            },
            {
                "href": external_services.api_url + "/api/pictures/{id}/thumb.jpg",
                "rel": "item-preview",
                "title": "Thumbnail URL for a given picture",
                "type": "image/jpeg",
            },
            {
                "href": external_services.api_url + "/api/reports",
                "rel": "report",
                "title": "Post feedback/report about picture or sequence",
                "type": "application/json",
            },
            {
                "href": external_services.api_url + "/api/instances",
                "rel": "instances",
                "title": "Get the list of instances",
                "type": "application/json",
            },
        ],
        "stac_version": "1.1.0",
        "type": "Catalog",
    }
    # validate the catalog with pystac
    catalog = pystac.Catalog.from_dict(data)
    catalog.validate()


def test_collections_endpoint(external_services):
    from stac_api_validator import validations

    r = requests.get(f"{external_services.api_url}/api/collections")
    assert r.status_code == 200
    data = r.json()
    assert len(data["collections"]) == 1

    col = data["collections"][0]["id"]
    # validate the collection with pystac

    warnings, errors = validations.validate_api(
        root_url=f"{external_services.api_url}/api",
        ccs_to_validate=["core", "collections", "browseable", "filter"],
        collection=col,
        geometry=None,
        auth_bearer_token=None,
        auth_query_parameter=None,
        fields_nested_property=None,
        validate_pagination=True,
        query_config=validations.QueryConfig(
            query_comparison_field=None,
            query_eq_value=None,
            query_neq_value=None,
            query_lt_value=None,
            query_lte_value=None,
            query_gt_value=None,
            query_gte_value=None,
            query_substring_field=None,
            query_starts_with_value=None,
            query_ends_with_value=None,
            query_contains_value=None,
            query_in_field=None,
            query_in_values=None,
        ),
        transaction_collection=None,
        headers={},
    )

    assert not errors


def test_collections_items_with_different_host(external_services):
    # use urllib so requests do not override stuff based on the host header
    from urllib import request

    r = request.urlopen(
        request.Request(
            f"{external_services.api_url}/api/collections",
            headers={"Host": "my_custom_host.com"},
        )
    )
    assert r.status == 200
    res = json.loads(r.read().decode())
    assert res["links"] == [
        {
            "href": "http://my_custom_host.com/api",
            "rel": "root",
            "type": "application/json",
        },
        {
            "href": "http://my_custom_host.com/api/collections",
            "rel": "self",
            "type": "application/json",
        },
    ]
    assert len(res["collections"]) == 1
    col = res["collections"][0]
    assert col["links"] == [
        {
            "href": "http://my_custom_host.com/api",
            "rel": "root",
            "type": "application/json",
        },
        {
            "href": "http://my_custom_host.com/api/collections",
            "rel": "parent",
            "type": "application/json",
        },
        {
            "href": f"http://my_custom_host.com/api/collections/{col['id']}",
            "rel": "self",
            "type": "application/json",
        },
        {
            "href": f"http://my_custom_host.com/api/collections/{col['id']}/items",
            "rel": "items",
            "type": "application/json",
        },
    ]

    r = request.urlopen(
        request.Request(
            f"{external_services.api_url}/api/collections/{col['id']}",
            headers={"Host": "my_custom_host.com"},
        )
    )
    assert r.status == 200
    res = json.loads(r.read().decode())
    assert res["links"] == [
        {
            "href": "http://my_custom_host.com/api",
            "rel": "root",
            "type": "application/json",
        },
        {
            "href": "http://my_custom_host.com/api/collections",
            "rel": "parent",
            "type": "application/json",
        },
        {
            "href": f"http://my_custom_host.com/api/collections/{col['id']}",
            "rel": "self",
            "type": "application/json",
        },
        {
            "href": f"http://my_custom_host.com/api/collections/{col['id']}/items",
            "rel": "items",
            "type": "application/json",
        },
    ]

    # items links are also valid by the host
    r = request.urlopen(
        request.Request(
            f"{external_services.api_url}/api/collections/{col['id']}/items",
            headers={"Host": "my_custom_host.com"},
        )
    )
    assert r.status == 200
    res = json.loads(r.read().decode())
    assert res["links"] == [
        {
            "href": "http://my_custom_host.com/api",
            "rel": "root",
            "type": "application/json",
        },
        {
            "href": "http://my_custom_host.com/api/collections",
            "rel": "parent",
            "type": "application/json",
        },
        {
            "href": f"http://my_custom_host.com/api/collections/{col['id']}",
            "rel": "self",
            "type": "application/json",
        },
    ]

    for f in res["features"]:
        for link in f["links"]:
            assert link["href"].startswith("http://my_custom_host.com/api"), link["href"]

    # same for a specific item
    item_id = res["features"][0]["id"]
    r = request.urlopen(
        request.Request(
            f"{external_services.api_url}/api/collections/{col['id']}/items/{item_id}",
            headers={"Host": "my_custom_host.com"},
        )
    )
    assert r.status == 200
    res = json.loads(r.read().decode())
    for link in res["links"]:
        assert link["href"].startswith("http://my_custom_host.com/api")

    # same for the /search endpoint
    r = request.urlopen(
        request.Request(
            f"{external_services.api_url}/api/search",
            headers={"Host": "my_custom_host.com"},
        )
    )
    assert r.status == 200
    res = json.loads(r.read().decode())
    assert res["links"] == []
    for f in res["features"]:
        for link in f["links"]:
            assert link["href"].startswith("http://my_custom_host.com/api")


def get_collections(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections")
    assert r.status_code == 200
    res = []
    for col in r.json()["collections"]:
        c = pystac.Collection.from_dict(col)
        item_link = next(li.href for li in c.links if li.rel == "items")
        items = requests.get(item_link)
        items.raise_for_status()
        for i in items.json()["features"]:
            c.add_item(pystac.Item.from_dict(i))
        res.append(c)
    return res


def test_get_collection_thumbnail(external_services):
    """We should be able to a specific collection thumbnail"""
    r = requests.get(f"{external_services.api_url}/api/collections")
    col_id = r.json()["collections"][0]["id"]

    r = requests.get(
        f"{external_services.api_url}/api/collections/{col_id}/thumb.jpg",
        allow_redirects=False,
    )

    assert r.status_code == 308, r.text
    location = r.headers["Location"]

    assert re.match(
        rf"^{external_services.geovisio_instance_1_url}/pictures/.+/thumb.jpg$",
        location,
    ), location


def test_get_invalid_collection_thumbnail(external_services):
    """We should get an error when asking for a unknown collection thumbnail"""
    r = requests.get(
        f"{external_services.api_url}/api/collections/00000000-0000-0000-0000-000000000000/thumb.jpg",
        allow_redirects=False,
    )

    assert r.status_code == 404, r.text
    assert r.json() == {
        "message": "Impossible to find items for collection 00000000-0000-0000-0000-000000000000",
        "status": 404,
    }


def test_search_by_id(external_services):
    cols = get_collections(external_services)

    assert len(cols) == 1
    first_col_items = list(cols[0].get_items())
    assert len(first_col_items) == 3

    # post and get queries should yield the same results
    responses = [
        requests.get(f"{external_services.api_url}/api/search?ids={first_col_items[0].id},{first_col_items[1].id}"),
        requests.post(
            f"{external_services.api_url}/api/search",
            json={"ids": [first_col_items[0].id, first_col_items[1].id]},
        ),
    ]
    for r in responses:
        assert r.status_code == 200, r.text
        data = r.json()
        assert len(data["features"]) == 2

        assert set([i["id"] for i in data["features"]]) == {
            first_col_items[0].id,
            first_col_items[1].id,
        }


def test_search_by_id_not_found(external_services):
    r = requests.get(f"{external_services.api_url}/api/search?ids=00000000-0000-0000-0000-000000000000")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 0


def test_search_by_bbox_post(external_services):
    cols = get_collections(external_services)

    assert len(cols) == 1
    first_col_items = list(cols[0].get_items())
    assert len(first_col_items) == 3

    # large bbox should get every items
    r = requests.post(
        f"{external_services.api_url}/api/search",
        json={"bbox": [-1.684546389, 48.155066389, -1.684468056, 48.155073056]},
    )
    assert r.status_code == 200, r.text
    data = r.json()
    assert len(data["features"]) == 3

    # bbox and limit should work
    r = requests.post(
        f"{external_services.api_url}/api/search",
        json={
            "bbox": [-1.684546389, 48.155066389, -1.684468056, 48.155073056],
            "limit": 1,
        },
    )
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 1

    # reduced bbox should filter only 2 items
    r = requests.post(
        f"{external_services.api_url}/api/search",
        json={"bbox": [-1.684506389, 48.155066389, -1.684468056, 48.155071389]},
    )
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 2

    # out of context bbox should return no items
    r = requests.post(f"{external_services.api_url}/api/search", json={"bbox": [10, 40, 11, 41]})
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 0


def test_search_by_bbox_get(external_services):
    cols = get_collections(external_services)

    assert len(cols) == 1
    first_col_items = list(cols[0].get_items())
    assert len(first_col_items) == 3

    # large bbox should get every items
    r = requests.get(f"{external_services.api_url}/api/search?bbox=-1.684546389,48.155066389,-1.684468056,48.155073056")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 3

    # bbox and limit should work
    r = requests.get(f"{external_services.api_url}/api/search?bbox=-1.684546389,48.155066389,-1.684468056,48.155073056&limit=1")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 1

    r = requests.get(f"{external_services.api_url}/api/search?bbox=-1.684506389,48.155066389,-1.684468056,48.155071389")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 2


def test_invalid_search_by_bbox(external_services):
    r = requests.get(f"{external_services.api_url}/api/search?bbox=plop,pouet")
    assert r.status_code == 400
    assert r.json() == {
        "status": 400,
        "message": "Query deserialize error: Invalid parameter: plop is not a valid f64",
    }

    r = requests.post(f"{external_services.api_url}/api/search", json={"bbox": ["plop", "pouet"]})
    assert r.status_code == 400
    assert r.text == 'Json deserialize error: invalid type: string "plop", expected f64 at line 1 column 16'

    # bbox should have exactly 4 elements
    r = requests.get(f"{external_services.api_url}/api/search?bbox=-1.684546389,48.155066389,-1.684468056")
    assert r.status_code == 400
    assert r.json() == {
        "status": 400,
        "message": "Invalid parameter: Invalid bbox, the boundbing box should be an array with 4 values",
    }


def test_unknown_pic(external_services):
    r = requests.get(f"{external_services.api_url}/api/pictures/00000000-0000-0000-0000-000000000000/hd.jpg")
    assert r.status_code == 404
    assert r.json() == {"message": "Feature not found", "status": 404}


def test_invalid_uuid_pic(external_services):
    r = requests.get(f"{external_services.api_url}/api/pictures/pouet/hd.jpg")
    assert r.status_code == 404
    assert "UUID parsing failed" in r.text  # at one point, we'll need better error messages


def _get_one_pic_id(external_services):
    one_pic_res = requests.post(f"{external_services.api_url}/api/search", json={"limit": 1})
    assert one_pic_res.status_code == 200
    return one_pic_res.json()["features"][0]["id"]


def test_get_picture_hd_asset(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    r = requests.get(
        f"{external_services.api_url}/api/pictures/{one_pic_id}/hd.jpg",
        allow_redirects=False,
    )
    assert r.status_code == 308, r.text
    # this should redirect to the geovisio instance
    instance_url = external_services.geovisio_instance_1_url.removesuffix("/api")
    # Note: the instance_1 serve the HD through a relative path `/main-pictures`
    pic_path = f"/{str(one_pic_id)[0:2]}/{str(one_pic_id)[2:4]}/{str(one_pic_id)[4:6]}/{str(one_pic_id)[6:8]}/{str(one_pic_id)[9:]}.jpg"
    assert r.headers["Location"] == f"{instance_url}/main-pictures{pic_path}"


def test_get_picture_sd_asset(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    r = requests.get(
        f"{external_services.api_url}/api/pictures/{one_pic_id}/sd.jpg",
        allow_redirects=False,
    )
    assert r.status_code == 308, r.text
    # this should redirect to the geovisio instance
    assert r.headers["Location"] == f"{external_services.geovisio_instance_1_url}/pictures/{one_pic_id}/sd.jpg"


def test_get_picture_thumb_asset(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    r = requests.get(
        f"{external_services.api_url}/api/pictures/{one_pic_id}/thumb.jpg",
        allow_redirects=False,
    )
    assert r.status_code == 308, r.text
    # this should redirect to the geovisio instance
    assert r.headers["Location"] == f"{external_services.geovisio_instance_1_url}/pictures/{one_pic_id}/thumb.jpg"


def test_get_picture_unknown_asset(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    r = requests.get(
        f"{external_services.api_url}/api/pictures/{one_pic_id}/plop.jpg",
        allow_redirects=False,
    )
    assert r.status_code == 400
    assert r.json() == {
        "message": "Invalid parameter: plop is not a valid asset, should be either hd, sd or thumb",
        "status": 400,
    }


def test_get_picture_hd_asset_bad_format(external_services):
    one_pic_id = _get_one_pic_id(external_services)
    # for the moment only jpeg pic are handled
    r = requests.get(
        f"{external_services.api_url}/api/pictures/{one_pic_id}/hd.pouet",
        allow_redirects=False,
    )
    assert r.status_code == 404, r.text


def test_out_of_bound_zoom_tile(external_services):
    r = requests.get(f"{external_services.api_url}/api/map/20/1/1.mvt", allow_redirects=False)
    assert r.status_code == 400 and r.json() == {
        "status": 400,
        "message": "Invalid parameter: Z parameter is out of bounds (should be 0-15)",
    }
    # Note: for invalid parameter (lon should be a unsigned integer), the error is not so good since actix cannot be a matching route.
    # we'll keep it this way for the moment and improve if needed
    assert requests.get(f"{external_services.api_url}/api/map/-2/1/1.mvt", allow_redirects=False).status_code == 404


def test_out_of_bound_lon_tile(external_services):
    r = requests.get(f"{external_services.api_url}/api/map/2/10/1.mvt", allow_redirects=False)
    assert r.status_code == 400 and r.json() == {
        "status": 400,
        "message": "Invalid parameter: X or Y parameter is out of bound",
    }

    # Note: for invalid parameter (lon should be a unsigned integer), the error is not so good since actix cannot be a matching route.
    # we'll keep it this way for the moment and improve if needed
    r = requests.get(f"{external_services.api_url}/api/map/2/-10/1.mvt", allow_redirects=False)
    assert r.status_code == 404


def test_out_of_bound_lat_tile(external_services):
    r = requests.get(f"{external_services.api_url}/api/map/4/1/100.mvt", allow_redirects=False)
    assert r.status_code == 400 and r.json() == {
        "status": 400,
        "message": "Invalid parameter: X or Y parameter is out of bound",
    }
    # Note: for invalid parameter (lon should be a unsigned integer), the error is not so good since actix cannot be a matching route.
    # we'll keep it this way for the moment and improve if needed
    r = requests.get(f"{external_services.api_url}/api/map/4/1/-1.mvt", allow_redirects=False)
    assert r.status_code == 404
