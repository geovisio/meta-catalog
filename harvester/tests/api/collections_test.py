import psycopg
import requests
from ..conftest import harvest, import_collection_to_geovisio, ExternalServicesUrl, FIXTURE_DIR, cleanup_geovisio_instance, cleanup_db
import pytest
from harvester import Instance
from psycopg.sql import SQL


# Tests in this module needs to import a dataset as a fixture to get the needed data loaded.
# Check all dataset to know which data is loaded
# Note: no test in this module should load data, if different data are needed, create a new dataset


@pytest.fixture(scope="module")
def cleanup_all_databases(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers, geovision_instance_2_auth_headers):
    """Delete all collections"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)
    cleanup_geovisio_instance(external_services.geovisio_instance_2_url, geovision_instance_2_auth_headers)
    cleanup_db(external_services)


@pytest.fixture(scope="module", autouse=True)
def dataset_1(external_services, cleanup_all_databases):
    """
    In those tests, we want a lot of collections.

    one collection ("a collection")
    To speed up loading, only one collection is loaded in each instance, and they are duplicated in the database

    Note: there is lots of duplicated pictures, if the in futur checks are added to filter them, this test will need to be updated
    """
    import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection without geom",  # collection has no geometry since the points are too far apart
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )

    # this collection is copied 10 times, translating it
    col_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="another collections",
        pic_names=[
            FIXTURE_DIR / "col3" / "1.jpg",
            FIXTURE_DIR / "col3" / "2.jpg",
            FIXTURE_DIR / "col3" / "3.jpg",
            FIXTURE_DIR / "col3" / "4.jpg",
            FIXTURE_DIR / "col3" / "5.jpg",
        ],
    )
    col_id = col_location.split("/")[-1]
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )
    harvest(
        instance=Instance(name="geovisio_instance_2", root_url=external_services.geovisio_instance_2_url),
        external_services=external_services,
    )
    # NOTE: for the moment we don't need pictures, if needed, they need to be duplicated here too
    with psycopg.connect(external_services.db_url, autocommit=True) as db:
        for i in range(1, 10):
            new_creation_date = f'"2024-01-{i:02d}T13:58:03.515598+00:00"'
            new_update_date = f'"2024-02-{i:02d}T13:58:03.515598+00:00"'
            db.execute(
                """
WITH
cols AS (
        SELECT
                uuid_generate_v4() as new_id,
                content,
                instance_id,
                computed_geom,
                computed_picture_type,
                computed_model,
                computed_capture_date
        FROM collections
        WHERE id = %(id)s
)
, patched_cols AS (
    SELECT
            jsonb_set(
                jsonb_set(
                    jsonb_set(content, '{id}', (SELECT '"' || new_id || '"')::jsonb),
                    '{created}', %(new_creation_date)s
                ),
                '{updated}', %(new_update_date)s
            ),
            instance_id,
            ST_Translate(computed_geom, %(translate)s, %(translate)s) as computed_geom,
            computed_picture_type,
            computed_model,
            computed_capture_date
        FROM cols
)
INSERT INTO collections (content, instance_id, computed_geom, computed_picture_type, computed_model, computed_capture_date) SELECT * from patched_cols ;
""",
                {"id": col_id, "translate": (i / 10), "new_creation_date": new_creation_date, "new_update_date": new_update_date},
            )

        # we update the 2 original collections to have consistent created/updated date
        q = SQL(
            """UPDATE collections SET content = 
                    jsonb_set(
                        jsonb_set(
                            content, '{created}', %(new_creation_date)s
                        ),
                        '{updated}', %(new_update_date)s
                    )
                    """
        )
        db.execute(
            q + SQL(" WHERE content->>'title' = 'collection without geom'"),
            {"new_creation_date": '"2024-03-01T00:58:03.515598+00:00"', "new_update_date": '"2024-03-02T00:58:03.515598+00:00"'},
        )
        db.execute(
            q + SQL(" WHERE id = %(id)s"),
            {
                "new_creation_date": '"2024-01-10T13:58:03.515598+00:00"',
                "new_update_date": '"2024-02-10T13:58:03.515598+00:00"',
                "id": col_id,
            },
        )


def get_cols(r):
    assert r.status_code == 200
    return r.json()["collections"]


def test_limit_collections(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections")

    assert len(get_cols(r)) == 11

    r = requests.get(f"{external_services.api_url}/api/collections?limit=5")
    assert len(get_cols(r)) == 5


def test_invalid_limit_collections(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections?limit=1001")
    assert r.status_code == 400
    assert r.json() == {"status": 400, "message": "Invalid parameter: The limit cannot exceed 1000"}


def test_get_collections_by_large_bbox(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections")
    assert len(get_cols(r)) == 11

    # a very large bbox will still return all collections with a geometry, since one of the collection does not have one, we only got 10 results
    r = requests.get(f"{external_services.api_url}/api/collections?bbox=-10,40,11,53")
    assert len(get_cols(r)) == 10

    # and if we add a limit, the 2 are combined
    r = requests.get(f"{external_services.api_url}/api/collections?bbox=-10,40,11,53&limit=5")
    assert len(get_cols(r)) == 5


def test_get_collections_invalid_bbox(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections?bbox=-1.684546389,48.155066389")
    assert r.status_code == 400
    assert r.json() == {
        "message": "Invalid parameter: Invalid bbox, the boundbing box should be an array with 4 values",
        "status": 400,
    }
    r = requests.get(f"{external_services.api_url}/api/collections?bbox=pouet")
    assert r.status_code == 400
    assert r.json() == {"status": 400, "message": "Query deserialize error: Invalid parameter: pouet is not a valid f64"}


def test_get_collections_by_bbox(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections")
    assert len(get_cols(r)) == 11

    # if we filters around the some collections we only got them
    r = requests.get(f"{external_services.api_url}/api/collections?bbox=2.0,49.1,2.6,49.7")
    assert len(get_cols(r)) == 6

    # and we can limit even more
    r = requests.get(f"{external_services.api_url}/api/collections?bbox=2.0,49.1,2.6,49.7&limit=2")
    assert len(get_cols(r)) == 2


def test_get_collections_sortby_created(external_services):
    def created(r):
        return [c["created"] for c in get_cols(r)]

    initial_dates = [
        "2024-01-01T13:58:03.515598+00:00",
        "2024-01-02T13:58:03.515598+00:00",
        "2024-01-03T13:58:03.515598+00:00",
        "2024-01-04T13:58:03.515598+00:00",
        "2024-01-05T13:58:03.515598+00:00",
        "2024-01-06T13:58:03.515598+00:00",
        "2024-01-07T13:58:03.515598+00:00",
        "2024-01-08T13:58:03.515598+00:00",
        "2024-01-09T13:58:03.515598+00:00",
        "2024-01-10T13:58:03.515598+00:00",
        "2024-03-01T00:58:03.515598+00:00",
    ]

    r = requests.get(f"{external_services.api_url}/api/collections?sortby=%2Bcreated")
    assert created(r) == initial_dates

    # '+' is the default
    r = requests.get(f"{external_services.api_url}/api/collections?sortby=created")
    assert created(r) == initial_dates

    r = requests.get(f"{external_services.api_url}/api/collections?sortby=-created")
    assert created(r) == list(reversed(initial_dates))

    # we can also combine with limit
    r = requests.get(f"{external_services.api_url}/api/collections?sortby=-created&limit=3")
    assert created(r) == list(reversed(initial_dates))[0:3]


def test_get_collections_sortby_updated(external_services):
    def updated(r):
        return [c["updated"] for c in get_cols(r)]

    initial_dates = [
        "2024-02-01T13:58:03.515598+00:00",
        "2024-02-02T13:58:03.515598+00:00",
        "2024-02-03T13:58:03.515598+00:00",
        "2024-02-04T13:58:03.515598+00:00",
        "2024-02-05T13:58:03.515598+00:00",
        "2024-02-06T13:58:03.515598+00:00",
        "2024-02-07T13:58:03.515598+00:00",
        "2024-02-08T13:58:03.515598+00:00",
        "2024-02-09T13:58:03.515598+00:00",
        "2024-02-10T13:58:03.515598+00:00",
        "2024-03-02T00:58:03.515598+00:00",
    ]

    r = requests.get(f"{external_services.api_url}/api/collections?sortby=%2Bupdated")
    assert updated(r) == initial_dates

    # '+' is the default
    r = requests.get(f"{external_services.api_url}/api/collections?sortby=updated")
    assert updated(r) == initial_dates

    r = requests.get(f"{external_services.api_url}/api/collections?sortby=-updated")
    assert updated(r) == list(reversed(initial_dates))

    # we can also combine with limit
    r = requests.get(f"{external_services.api_url}/api/collections?sortby=-updated&limit=3")
    assert updated(r) == list(reversed(initial_dates))[0:3]


def test_get_collections_invalid_sortby(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections?sortby=pouet")
    assert r.status_code == 400 and r.json() == {
        "message": "Query deserialize error: Invalid sortby parameter: invalid field to sort on\n\nCaused by:\n    invalid collection field: `pouet`, only `created`, `updated` and `provider_id` are supported for the moment",
        "status": 400,
    }
    r = requests.get(f"{external_services.api_url}/api/collections?sortby=%updated")
    assert r.status_code == 400 and r.json() == {
        "message": "Query deserialize error: Invalid sortby parameter: invalid field to sort on\n\nCaused by:\n    invalid collection field: `%updated`, only `created`, `updated` and `provider_id` are supported for the moment",
        "status": 400,
    }
    r = requests.get(f"{external_services.api_url}/api/collections?sortby=-")
    assert r.status_code == 400 and r.json() == {
        "message": "Query deserialize error: Invalid sortby parameter: invalid field to sort on\n\nCaused by:\n    invalid collection field: `-`, only `created`, `updated` and `provider_id` are supported for the moment",
        "status": 400,
    }


def test_get_collections_page_filter(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections")

    def created(r):
        return [c["created"] for c in get_cols(r)]

    assert len(created(r)) == 11
    # ask for the collection created 1us before the fourth one
    r = requests.get(f"{external_services.api_url}/api/collections?sortby=created&page=created+%3E+'2024-01-04T13:58:03.515597%2B00:00'")
    assert created(r) == [
        "2024-01-04T13:58:03.515598+00:00",
        "2024-01-05T13:58:03.515598+00:00",
        "2024-01-06T13:58:03.515598+00:00",
        "2024-01-07T13:58:03.515598+00:00",
        "2024-01-08T13:58:03.515598+00:00",
        "2024-01-09T13:58:03.515598+00:00",
        "2024-01-10T13:58:03.515598+00:00",
        "2024-03-01T00:58:03.515598+00:00",
    ]

    # if we move us after, we dont get it
    r = requests.get(f"{external_services.api_url}/api/collections?sortby=created&page=created+%3E+'2024-01-04T13:58:03.515598%2B00:00'")
    assert created(r) == [
        "2024-01-05T13:58:03.515598+00:00",
        "2024-01-06T13:58:03.515598+00:00",
        "2024-01-07T13:58:03.515598+00:00",
        "2024-01-08T13:58:03.515598+00:00",
        "2024-01-09T13:58:03.515598+00:00",
        "2024-01-10T13:58:03.515598+00:00",
        "2024-03-01T00:58:03.515598+00:00",
    ]

    # if also works with the limits
    r = requests.get(
        f"{external_services.api_url}/api/collections?limit=2&sortby=created&page=created+%3E+'2024-01-04T13:58:03.515598%2B00:00'"
    )
    assert created(r) == [
        "2024-01-05T13:58:03.515598+00:00",
        "2024-01-06T13:58:03.515598+00:00",
    ]


def test_get_collections_invalid_page_filter(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections?page=updated+%3E+'2023-07-12T10:12:36Z'")
    assert r.status_code == 400 and r.json() == {
        "message": "Invalid parameter: When using `page` parameter, the results must be sorted by the same field as the page filter (`sortby=updated`)",
        "status": 400,
    }


def test_get_collections_all_results_no_pagination(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections")
    assert len(get_cols(r)) == 11

    links = r.json()["links"]
    assert links == [
        {"href": f"{external_services.api_url}/api", "rel": "root", "type": "application/json"},
        {"href": f"{external_services.api_url}/api/collections", "rel": "self", "type": "application/json"},
    ]  # no pagination links since all results have been returned


def get_link(links, rel):
    return next((li["href"] for li in links if li["rel"] == rel), None)


def test_get_collections_forward_pagination(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections?limit=2")
    assert len(get_cols(r)) == 2

    def created(r):
        return [c["created"] for c in get_cols(r)]

    links = r.json()["links"]
    # it's the first page, there is only a next/last link, no prev/first
    assert links == [
        {
            "href": f"{external_services.api_url}/api/collections?sortby=created&page=created%3E%272024-01-02T13%3A58%3A03.515598%2B00%3A00%27&limit=2",
            "rel": "next",
            "type": "application/json",
        },
        {
            "href": f"{external_services.api_url}/api/collections?sortby=created&page=created%3C%3D%272024-03-01T00%3A58%3A03.515598%2B00%3A00%27&limit=2",
            "rel": "last",
            "type": "application/json",
        },
        {"href": f"{external_services.api_url}/api", "rel": "root", "type": "application/json"},
        {"href": f"{external_services.api_url}/api/collections", "rel": "self", "type": "application/json"},
    ]
    all_cols = [created(r)]

    next_link = get_link(links, "next")
    assert next_link
    while next_link:
        r = requests.get(next_link)
        assert r.status_code == 200
        all_cols.append(created(r))
        links = r.json()["links"]
        next_link = get_link(links, "next")
        if not next_link:
            # last link should not have a 'last' page since it's already the last one
            assert not get_link(links, "last")

    # we should have got the collections by batch of 2 collections,
    assert all_cols == [
        ["2024-01-01T13:58:03.515598+00:00", "2024-01-02T13:58:03.515598+00:00"],
        ["2024-01-03T13:58:03.515598+00:00", "2024-01-04T13:58:03.515598+00:00"],
        ["2024-01-05T13:58:03.515598+00:00", "2024-01-06T13:58:03.515598+00:00"],
        ["2024-01-07T13:58:03.515598+00:00", "2024-01-08T13:58:03.515598+00:00"],
        ["2024-01-09T13:58:03.515598+00:00", "2024-01-10T13:58:03.515598+00:00"],
        ["2024-03-01T00:58:03.515598+00:00"],
    ]


def test_get_collections_backward_pagination_on_updated_reverse_sort(external_services):
    r = requests.get(f"{external_services.api_url}/api/collections?limit=2&sortby=-updated")
    assert len(get_cols(r)) == 2

    # we only compare the date, not the time to ease test reading
    def updated(r):
        return [c["updated"][0:10] for c in get_cols(r)]

    links = r.json()["links"]
    # it's the first page, there is only a next/last link, no prev/first
    assert links == [
        {
            "href": f"{external_services.api_url}/api/collections?sortby=-updated&page=updated%3C%272024-02-10T13%3A58%3A03.515598%2B00%3A00%27&limit=2",
            "rel": "next",
            "type": "application/json",
        },
        {
            "href": f"{external_services.api_url}/api/collections?sortby=-updated&page=updated%3E%3D%272024-02-01T13%3A58%3A03.515598%2B00%3A00%27&limit=2",
            "rel": "last",
            "type": "application/json",
        },
        {"href": f"{external_services.api_url}/api", "rel": "root", "type": "application/json"},
        {"href": f"{external_services.api_url}/api/collections", "rel": "self", "type": "application/json"},
    ]
    all_cols = [updated(r)]

    next_link = get_link(links, "next")
    assert next_link
    while next_link:
        r = requests.get(next_link)
        assert r.status_code == 200
        all_cols.append(updated(r))

        links = r.json()["links"]
        next_link = get_link(links, "next")
        if not next_link:
            # last link should not have a 'last' page since it's already the last one
            assert not get_link(links, "last")

    # we should have got the collections by batch of 2 collections, in reverse order
    assert all_cols == [
        ["2024-03-02", "2024-02-10"],
        ["2024-02-09", "2024-02-08"],
        ["2024-02-07", "2024-02-06"],
        ["2024-02-05", "2024-02-04"],
        ["2024-02-03", "2024-02-02"],
        ["2024-02-01"],
    ]


def test_get_collections_backward_pagination(external_services):
    """We should be able to get all collections by iterating from the last page"""
    r = requests.get(f"{external_services.api_url}/api/collections?limit=2")
    assert len(get_cols(r)) == 2

    # we only compare the date, not the time to ease test reading
    def created(r):
        return [c["created"][0:10] for c in get_cols(r)]

    links = r.json()["links"]
    # it's the first page, there is only a next/last link, no prev/first
    assert links == [
        {
            "href": f"{external_services.api_url}/api/collections?sortby=created&page=created%3E%272024-01-02T13%3A58%3A03.515598%2B00%3A00%27&limit=2",
            "rel": "next",
            "type": "application/json",
        },
        {
            "href": f"{external_services.api_url}/api/collections?sortby=created&page=created%3C%3D%272024-03-01T00%3A58%3A03.515598%2B00%3A00%27&limit=2",
            "rel": "last",
            "type": "application/json",
        },
        {"href": f"{external_services.api_url}/api", "rel": "root", "type": "application/json"},
        {"href": f"{external_services.api_url}/api/collections", "rel": "self", "type": "application/json"},
    ]
    all_cols = []

    last_link = get_link(links, "last")
    assert last_link
    r = requests.get(last_link)
    assert r.status_code == 200
    # we get the last 2 results, and ordered by ascending creation date
    assert created(r) == ["2024-01-10", "2024-03-01"]

    prev_link = last_link
    while prev_link:
        r = requests.get(prev_link)
        assert r.status_code == 200
        all_cols.append(created(r))
        links = r.json()["links"]
        prev_link = get_link(links, "prev")
        if not prev_link:
            # last backward link should not have a 'first' page since it's already the first one
            assert not get_link(links, "first")

    # we should have got the collections by batch of 2 collections,
    assert all_cols == [
        ["2024-01-10", "2024-03-01"],
        ["2024-01-08", "2024-01-09"],
        ["2024-01-06", "2024-01-07"],
        ["2024-01-04", "2024-01-05"],
        ["2024-01-02", "2024-01-03"],
        ["2024-01-01"],
    ]
