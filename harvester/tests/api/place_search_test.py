import psycopg
import requests
from ..conftest import harvest, import_collection_to_geovisio, ExternalServicesUrl, FIXTURE_DIR, cleanup_geovisio_instance, cleanup_db
import pytest
from dataclasses import dataclass
from typing import Dict, Any, List
from harvester import Instance


# Tests in this module needs to import a dataset as a fixture to get the needed data loaded.
# Check all dataset to know which data is loaded
# Note: no test in this module should load data, if different data are needed, create a new dataset


@pytest.fixture(scope="module")
def cleanup_all_databases(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers, geovision_instance_2_auth_headers):
    """Delete all collections"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)
    cleanup_geovisio_instance(external_services.geovisio_instance_2_url, geovision_instance_2_auth_headers)
    cleanup_db(external_services)


@pytest.fixture(scope="module")
def dataset_1(external_services, cleanup_all_databases):
    """
    This dataset loads 1 collection with 3 pictures in instance 1
    and 2 collections with 2 and 3 pictures in instance 2
    """
    import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 3",
        pic_names=[
            FIXTURE_DIR / "col3" / "1.jpg",
            FIXTURE_DIR / "col3" / "2.jpg",
            FIXTURE_DIR / "col3" / "3.jpg",
            FIXTURE_DIR / "col3" / "4.jpg",
            FIXTURE_DIR / "col3" / "5.jpg",
        ],
    )
    # harvest both instances
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )
    harvest(
        instance=Instance(name="geovisio_instance_2", root_url=external_services.geovisio_instance_2_url),
        external_services=external_services,
    )


@dataclass
class SearchResponse:
    json: Dict[Any, Any]
    filenames: List[str]
    distances: List[float]


def search(external_services, position, additional_query=""):
    r = requests.get(f"{external_services.api_url}/api/search?place_position={position}{additional_query}")
    assert r.status_code == 200
    data = r.json()
    filenames = [f["properties"]["original_file:name"] for f in data["features"]]
    ids = [f["id"] for f in data["features"]]

    lon, lat = position.split(",")
    with psycopg.connect(external_services.db_url) as db:
        # to get something consistent over time, we change the uploading time to a fixed date
        distances = db.execute(
            "SELECT ST_Distance(geom, ST_Point(%s, %s)::geography) FROM items WHERE id = ANY(%s);",
            [lon, lat, ids],
        ).fetchall()
        distances = [d[0] for d in distances]
        return SearchResponse(json=data, filenames=filenames, distances=distances)


def test_search_a_given_place(external_services, dataset_1):
    r = search(external_services, position="1.9191859,49.0068908", additional_query="&place_distance=0-10&limit=2")
    assert r.filenames == ["1.jpg", "2.jpg"]
    assert r.distances == [0.13543812, 0.91407851]


def test_search_a_given_place_limit(external_services, dataset_1):
    # results are sorted, we always get the nearest when limiting results
    r = search(external_services, position="1.9191859,49.0068908", additional_query="&place_distance=0-10&limit=1")
    assert r.filenames == ["1.jpg"]


def test_search_a_given_place_get_higher_range(external_services, dataset_1):
    r = search(external_services, position="1.9191859,49.0068908", additional_query="&place_distance=5-20")
    assert r.filenames == ["4.jpg", "5.jpg"]
    assert r.distances == [7.19502434, 9.20712293]

    # No impact of fov tolerance on results (as all pics are 360°)
    r = search(external_services, position="1.9191859,49.0068908", additional_query="&place_distance=5-20&place_fov_tolerance=2")
    assert r.filenames == ["4.jpg", "5.jpg"]
    assert r.distances == [7.19502434, 9.20712293]


def test_search_a_given_place_post(external_services, dataset_1):
    r = requests.post(
        f"{external_services.api_url}/api/search", json={"limit": 2, "place_position": [1.9191859, 49.0068908], "place_distance": "0-10"}
    )
    filenames = [f["properties"]["original_file:name"] for f in r.json()["features"]]
    assert filenames == ["1.jpg", "2.jpg"]


def test_search_a_given_place_get_far(external_services, dataset_1):
    r = search(external_services, position="1.9191859,49.0068908", additional_query="&place_distance=0-10000&limit=2")
    assert r.filenames == ["1.jpg", "2.jpg"]
    assert r.distances == [0.13543812, 0.91407851]


def test_search_a_given_place_flat_pictures(external_services, dataset_1):
    r = search(external_services, position="-1.9499096,48.1397572")
    assert r.filenames == ["b1.jpg"]


def test_search_a_given_place_flat_pictures_not_looking_at_poi(external_services, dataset_1):
    """Should not return picture (near first one, but out of sight)"""
    r = search(external_services, position="-1.9499029,48.1398476")
    assert r.filenames == []


def test_search_a_given_place_flat_pictures_looking_at_poi(external_services, dataset_1):
    """Increasing the fov_tolerance should make it possible to see the picture"""
    r = search(external_services, position="-1.9499029,48.1398476", additional_query="&place_fov_tolerance=180")
    assert r.filenames == ["b1.jpg"]


def test_search_by_collection_id(external_services, dataset_1):
    r = requests.get(f"{external_services.api_url}/api/collections")
    assert r.status_code == 200
    col_id = r.json()["collections"][0]["id"]
    assert r.json()["collections"][0]["stats:items"]["count"] == 2  # collection has 2 elements, we should find them all in the /search

    # post and get queries should yield the same results
    responses = [
        requests.get(f"{external_services.api_url}/api/search?collections={col_id}"),
        requests.post(f"{external_services.api_url}/api/search", json={"collections": [col_id]}),
    ]
    for r in responses:
        assert r.status_code == 200, r.text
        data = r.json()
        assert len(data["features"]) == 2
        assert set([i["collection"] for i in data["features"]]) == {col_id}


def test_search_by_collection_id_not_found(external_services, dataset_1):
    r = requests.get(
        f"{external_services.api_url}/api/search?collections=00000000-0000-0000-0000-000000000000,11111111-1111-1111-1111-111111111111"
    )
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 0


def test_search_by_filter_on_field_of_view_360(external_services, dataset_1):
    r = requests.get(f"{external_services.api_url}/api/search?filter=field_of_view=360")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 5


def test_search_by_filter_on_field_of_view_non_360(external_services, dataset_1):
    r = requests.get(f"{external_services.api_url}/api/search?filter=field_of_view<360")
    assert r.status_code == 200
    data = r.json()
    assert len(data["features"]) == 2  # 2 items does not have a defined field of view, so they should be returned


def test_search_by_filter_invalid_field(external_services, dataset_1):
    r = requests.get(f"{external_services.api_url}/api/search?filter=pouet=360")
    assert r.status_code == 400
    assert r.json() == {
        "message": """Query deserialize error: Impossible to validate cql2 expression

Caused by:
    field 'pouet' is not allowed in the item search query""",
        "status": 400,
    }


def test_search_by_filter_invalid_cql2(external_services, dataset_1):
    r = requests.get(f"{external_services.api_url}/api/search?filter=pouet")
    assert r.status_code == 400
    assert r.json() == {
        "message": """Query deserialize error: Impossible to validate cql2 expression

Caused by:
    Expression is not a valid cql2 filter: jsonschema validation failed""",
        "status": 400,
    }
