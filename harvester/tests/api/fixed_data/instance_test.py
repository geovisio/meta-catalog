import psycopg
import requests


def test_instances(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/instances")

    with psycopg.connect(external_services.db_url) as db:
        instances_ids = {i[1]: str(i[0]) for i in db.execute("SELECT id, name FROM instances;").fetchall()}

    assert r.status_code == 200, r.text
    assert r.json() == {
        "instances": [
            {
                "id": instances_ids["geovisio_instance_1"],
                "name": "geovisio_instance_1",
                "url": f"{external_services.geovisio_instance_1_url}",
                "extent": {"temporal": {"interval": [["2022-10-19T07:56:34Z", "2022-10-19T07:56:38Z"]]}},
                "links": [
                    {
                        "href": f"{external_services.api_url}/api/instances/geovisio_instance_1",
                        "rel": "self",
                        "type": "application/json",
                    }
                ],
                "configuration": {
                    "auth": {
                        "enabled": False,
                    },
                    "color": "#bf360c",
                    "description": {
                        "label": "The open source photo mapping solution",
                        "langs": {
                            "en": "The open source photo mapping solution",
                        },
                    },
                    "email": "panoramax@panoramax.fr",
                    "geo_coverage": {
                        "label": "Worldwide\nThe picture can be sent from anywhere in the world.",
                        "langs": {
                            "en": "Worldwide\nThe picture can be sent from anywhere in the world.",
                        },
                    },
                    "license": {
                        "id": "proprietary",
                    },
                    "logo": "https://gitlab.com/panoramax/gitlab-profile/-/raw/main/images/logo.svg",
                    "name": {
                        "label": "GeoVisio",
                        "langs": {
                            "en": "GeoVisio",
                        },
                    },
                    "pages": [],
                    "version": "2.8.0",
                },
            },
            {
                "id": instances_ids["geovisio_instance_2"],
                "name": "geovisio_instance_2",
                "url": f"{external_services.geovisio_instance_2_url}",
                "extent": {"temporal": {"interval": [["2015-04-25T13:36:17Z", "2022-10-19T07:56:42Z"]]}},
                "links": [
                    {
                        "href": f"{external_services.api_url}/api/instances/geovisio_instance_2",
                        "rel": "self",
                        "type": "application/json",
                    }
                ],
                "configuration": {
                    "auth": {
                        "enabled": False,
                    },
                    "color": "#bf360c",
                    "description": {
                        "label": "The open source photo mapping solution",
                        "langs": {
                            "en": "The open source photo mapping solution",
                        },
                    },
                    "email": "panoramax@panoramax.fr",
                    "geo_coverage": {
                        "label": "Worldwide\nThe picture can be sent from anywhere in the world.",
                        "langs": {
                            "en": "Worldwide\nThe picture can be sent from anywhere in the world.",
                        },
                    },
                    "license": {
                        "id": "proprietary",
                    },
                    "logo": "https://gitlab.com/panoramax/gitlab-profile/-/raw/main/images/logo.svg",
                    "name": {
                        "label": "GeoVisio",
                        "langs": {
                            "en": "GeoVisio",
                        },
                    },
                    "pages": [],
                    "version": "2.8.0",
                },
            },
        ]
    }


def test_one_instances(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/instances/geovisio_instance_1")
    with psycopg.connect(external_services.db_url) as db:
        instances_ids = {i[1]: str(i[0]) for i in db.execute("SELECT id, name FROM instances;").fetchall()}

    assert r.status_code == 200, r.text
    assert r.json() == {
        "id": instances_ids["geovisio_instance_1"],
        "name": "geovisio_instance_1",
        "url": f"{external_services.geovisio_instance_1_url}",
        "extent": {"temporal": {"interval": [["2022-10-19T07:56:34Z", "2022-10-19T07:56:38Z"]]}},
        "links": [
            {
                "href": f"{external_services.api_url}/api/instances/geovisio_instance_1",
                "rel": "self",
                "type": "application/json",
            }
        ],
        "configuration": {
            "auth": {
                "enabled": False,
            },
            "color": "#bf360c",
            "description": {
                "label": "The open source photo mapping solution",
                "langs": {
                    "en": "The open source photo mapping solution",
                },
            },
            "email": "panoramax@panoramax.fr",
            "geo_coverage": {
                "label": "Worldwide\nThe picture can be sent from anywhere in the world.",
                "langs": {
                    "en": "Worldwide\nThe picture can be sent from anywhere in the world.",
                },
            },
            "license": {
                "id": "proprietary",
            },
            "logo": "https://gitlab.com/panoramax/gitlab-profile/-/raw/main/images/logo.svg",
            "name": {
                "label": "GeoVisio",
                "langs": {
                    "en": "GeoVisio",
                },
            },
            "pages": [],
            "version": "2.8.0",
        },
    }


def test_one_instances_unknown(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/instances/pouet")
    assert r.status_code == 404
    assert r.json() == {"message": "Instance not found", "status": 404}
