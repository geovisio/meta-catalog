import requests
import mapbox_vector_tile


def test_grid(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/map/0/0/0.mvt", allow_redirects=False)
    assert r.status_code == 200
    data = mapbox_vector_tile.decode(r.content)
    is_valid_tile(data)
    assert get_layers_count(data) == {"grid": 3}
    nb_items = sorted(
        [{k: v for k, v in f["properties"].items()} for f in data["grid"]["features"]],
        key=lambda k: k["id"],
    )
    assert nb_items == [
        {
            "id": 1,
            "coef": 0.5,
            "coef_360_pictures": 0.0,  # <- Note: the coef if 0 here because the median of 360 picture is 0, so all coef are 0
            "coef_flat_pictures": 0.0,
            "nb_360_pictures": 3,
            "nb_flat_pictures": 0,
            "nb_pictures": 3,
        },
        {
            "id": 2,
            "coef": 1.0,
            "coef_360_pictures": 0.0,
            "coef_flat_pictures": 1.0,
            "nb_360_pictures": 0,
            "nb_flat_pictures": 4,
            "nb_pictures": 4,
        },
        {
            "id": 3,
            "coef": 0.3,
            "coef_360_pictures": 0.0,
            "coef_flat_pictures": 0.5,
            "nb_360_pictures": 0,
            "nb_flat_pictures": 2,
            "nb_pictures": 2,
        },
    ]


def test_grid_empty(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/map/4/1/1.mvt", allow_redirects=False)
    assert r.status_code == 204


def test_high_zoom_tile_with_pictures(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/map/15/16230/11369.mvt", allow_redirects=False)
    assert r.status_code == 200
    data = mapbox_vector_tile.decode(r.content)
    is_valid_tile(data)
    assert get_layers_count(data) == {"pictures": 4, "sequences": 1}
    for f in data["pictures"]["features"]:
        f["properties"].pop("id")  # pop id not to compare them

    assert len(data["pictures"]["features"]) == 4
    # Note: the order of the features is not deterministic, so we cannot compare them
    assert {
        "geometry": {
            "coordinates": [
                2710,
                2038,
            ],
            "type": "Point",
        },
        "id": 0,
        "properties": {
            "account_id": dataset.provider_ids["bobette@geovisio_instance_2"],
            "gps_accuracy": 3.4,
            "h_pixel_density": 3,
            "model": "SONY FDR-X1000V",
            "sequences": f'["{dataset.collection_ids["col4"]}"]',
            "ts": "2022-10-19 07:56:42+00",
            "type": "flat",
        },
        "type": "Feature",
    } in data["pictures"]["features"]

    assert {
        "geometry": {
            "coordinates": [
                2769,
                2033,
            ],
            "type": "Point",
        },
        "id": 0,
        "properties": {
            "account_id": dataset.provider_ids["Default account@geovisio_instance_1"],
            "gps_accuracy": 3.4,
            "h_pixel_density": 20,
            "heading": "277",
            "model": "SONY FDR-X1000V",
            "sequences": f'["{dataset.collection_ids["col1"]}"]',
            "ts": "2022-10-19 07:56:34+00",
            "type": "flat",
        },
        "type": "Feature",
    } in data["pictures"]["features"]

    assert {
        "geometry": {
            "coordinates": [
                2754,
                2036,
            ],
            "type": "Point",
        },
        "id": 0,
        "properties": {
            "account_id": dataset.provider_ids["Default account@geovisio_instance_1"],
            "gps_accuracy": 3.4,
            "h_pixel_density": 20,
            "heading": "272",
            "model": "SONY FDR-X1000V",
            "sequences": f'["{dataset.collection_ids["col1"]}"]',
            "ts": "2022-10-19 07:56:36+00",
            "type": "flat",
        },
        "type": "Feature",
    } in data["pictures"]["features"]
    assert {
        "geometry": {
            "coordinates": [
                2740,
                2037,
            ],
            "type": "Point",
        },
        "id": 0,
        "properties": {
            "account_id": dataset.provider_ids["Default account@geovisio_instance_1"],
            "gps_accuracy": 3.4,
            "h_pixel_density": 20,
            "heading": "272",
            "model": "SONY FDR-X1000V",
            "sequences": f'["{dataset.collection_ids["col1"]}"]',
            "ts": "2022-10-19 07:56:38+00",
            "type": "flat",
        },
        "type": "Feature",
    } in data["pictures"]["features"]


def test_high_zoom_tile_with_360(external_services, dataset):
    """this tile should return the collection 3, containing 360 pictures"""
    r = requests.get(f"{external_services.api_url}/api/map/14/8279/5626.mvt", allow_redirects=False)
    assert r.status_code == 200
    data = mapbox_vector_tile.decode(r.content)
    is_valid_tile(data)
    f = data["sequences"]["features"][0]
    assert f == {
        "geometry": {
            "coordinates": [
                [
                    1410,
                    3482,
                ],
                [
                    1411,
                    3484,
                ],
                [
                    1412,
                    3492,
                ],
            ],
            "type": "LineString",
        },
        "id": 0,
        "properties": {
            "account_id": dataset.provider_ids["bob@geovisio_instance_2"],
            "date": "2021-07-29",
            "id": dataset.collection_ids["col3"],
            "model": "GoPro Max",
            "type": "equirectangular",
            "gps_accuracy": 4.0,
            "h_pixel_density": 1,
        },
        "type": "Feature",
    }


def test_mid_zoom_tile(external_services, dataset):
    """This tile should return the collection 1"""
    r = requests.get(f"{external_services.api_url}/api/map/12/2028/1421.mvt", allow_redirects=False)
    assert r.status_code == 200
    data = mapbox_vector_tile.decode(r.content)
    is_valid_tile(data)
    assert get_layers_count(data) == {"sequences": 1}
    f = data["sequences"]["features"][0]
    assert f == {
        "geometry": {
            "type": "LineString",
            "coordinates": [
                [
                    3418,
                    3326,
                ],
                [
                    3416,
                    3327,
                ],
                [
                    3414,
                    3327,
                ],
            ],
        },
        "properties": {
            "account_id": dataset.provider_ids["Default account@geovisio_instance_1"],
            "model": "SONY FDR-X1000V",
            "id": dataset.collection_ids["col1"],
            "type": "flat",
            "date": "2022-10-19",
            "h_pixel_density": 20,
            "gps_accuracy": 3.4,
        },
        "id": 0,
        "type": "Feature",
    }


def get_layers_count(tile):
    return {layer: len(f["features"]) for layer, f in tile.items()}


def is_valid_tile(tile):
    for layer in ["sequences", "pictures", "grid"]:
        if layer not in tile:
            continue
        for f in tile[layer]["features"]:
            prop = f["properties"]
            if layer == "grid":
                assert "id" in prop
                assert "nb_pictures" in prop
                assert "nb_360_pictures" in prop
                assert "nb_flat_pictures" in prop
                assert "coef" in prop
                assert "coef_360_pictures" in prop
                assert "coef_flat_pictures" in prop
            else:
                assert "account_id" in prop
                assert "model" in prop
                assert "type" in prop
                assert "h_pixel_density" in prop
                assert "gps_accuracy" in prop


def test_get_style(external_services, dataset):
    r = requests.get(f"{external_services.api_url}/api/map/style.json", allow_redirects=False)
    assert r.status_code == 200
    assert r.json() == {
        "version": 8,
        "name": "GeoVisio Vector Tiles",
        "metadata": {
            "panoramax:fields": {
                "sequences": [
                    "id",
                    "account_id",
                    "model",
                    "type",
                    "date",
                    "gps_accuracy",
                    "h_pixel_density",
                ],
                "pictures": [
                    "id",
                    "account_id",
                    "ts",
                    "heading",
                    "sequences",
                    "type",
                    "model",
                    "gps_accuracy",
                    "h_pixel_density",
                ],
                "grid": [
                    "id",
                    "nb_pictures",
                    "nb_360_pictures",
                    "nb_flat_pictures",
                    "coef",
                    "coef_360_pictures",
                    "coef_flat_pictures",
                ],
            }
        },
        "sources": {
            "geovisio": {
                "type": "vector",
                "tiles": [f"{external_services.api_url}/api/map/" + "{z}/{x}/{y}.mvt"],
                "minzoom": 0,
                "maxzoom": 15,
            }
        },
        "layers": [
            {
                "id": "geovisio_sequences",
                "type": "line",
                "source": "geovisio",
                "source-layer": "sequences",
                "paint": {
                    "line-color": "#FF6F00",
                    "line-width": [
                        "interpolate",
                        ["linear"],
                        ["zoom"],
                        0,
                        0.5,
                        10,
                        2,
                        14,
                        4,
                        16,
                        5,
                        22,
                        3,
                    ],
                    "line-opacity": [
                        "interpolate",
                        ["linear"],
                        ["zoom"],
                        6.25,
                        0,
                        7,
                        1,
                    ],
                },
                "layout": {
                    "line-cap": "square",
                },
            },
            {
                "id": "geovisio_pictures",
                "type": "circle",
                "source": "geovisio",
                "source-layer": "pictures",
                "paint": {
                    "circle-color": "#FF6F00",
                    "circle-radius": [
                        "interpolate",
                        ["linear"],
                        ["zoom"],
                        15,
                        4.5,
                        17,
                        8,
                        22,
                        12,
                    ],
                    "circle-opacity": [
                        "interpolate",
                        ["linear"],
                        ["zoom"],
                        15,
                        0,
                        16,
                        1,
                    ],
                    "circle-stroke-color": "#ffffff",
                    "circle-stroke-width": [
                        "interpolate",
                        ["linear"],
                        ["zoom"],
                        17,
                        0,
                        20,
                        2,
                    ],
                },
            },
            {
                "id": "geovisio_grid",
                "type": "circle",
                "source": "geovisio",
                "source-layer": "grid",
                "layout": {
                    "circle-sort-key": ["get", "coef"],
                },
                "paint": {
                    "circle-radius": [
                        "interpolate",
                        ["linear"],
                        ["zoom"],
                        1,
                        ["match", ["get", "coef"], 0, 0, 1],
                        6 - 2,
                        ["match", ["get", "coef"], 0, 0, 6],
                        6 - 1,
                        ["match", ["get", "coef"], 0, 0, 2.5],
                        6,
                        ["match", ["get", "coef"], 0, 0, 4],
                        6 + 1,
                        ["match", ["get", "coef"], 0, 0, 7],
                    ],
                    "circle-color": [
                        "interpolate",
                        ["linear"],
                        ["get", "coef"],
                        0,
                        "#FFA726",
                        0.5,
                        "#E65100",
                        1,
                        "#3E2723",
                    ],
                    "circle-opacity": [
                        "interpolate",
                        ["linear"],
                        ["zoom"],
                        6 - 2,
                        0.5,
                        6 - 1,
                        1,
                        6 + 0.75,
                        1,
                        6 + 1,
                        0,
                    ],
                },
            },
        ],
    }
