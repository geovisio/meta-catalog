# All tests in this module will use the same data
# Note: no test in this module should load data, if different data are needed, create a new module
import psycopg
from ...conftest import (
    harvest,
    import_collection_to_geovisio,
    ExternalServicesUrl,
    FIXTURE_DIR,
    cleanup_geovisio_instance,
    cleanup_db,
)
from ..conftest import Dataset
import pytest
from harvester import Instance


@pytest.fixture(scope="module")
def cleanup_all_databases(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers, geovision_instance_2_auth_headers):
    """Delete all collections"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)
    cleanup_geovisio_instance(external_services.geovisio_instance_2_url, geovision_instance_2_auth_headers)
    cleanup_db(external_services)


@pytest.fixture(scope="module")
def dataset(external_services, cleanup_all_databases):
    """
    This dataset loads:
    * col1 collection with 3 pics in instance 1, associated to the default user
    * col2 (2 pics) and col3 (3 pics) collections in instance 2, associated to the 'bob'
    * col4 collections in instance 2, associated to the 'bobette'

    Note: to avoid too much burden, the users are changed by modifying the harvested database
    """
    """
    This dataset loads 1 collection with 3 pictures in instance 1
    and 2 collections with 2 and 3 pictures in instance 2
    """
    col1_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="collection 1",
        pic_names=[FIXTURE_DIR / "col1" / "e1.jpg", FIXTURE_DIR / "col1" / "e2.jpg", FIXTURE_DIR / "col1" / "e3.jpg"],
    )
    col1_id = col1_location.split("/")[-1]
    col2_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 2",
        pic_names=[FIXTURE_DIR / "col2" / "b1.jpg", FIXTURE_DIR / "col2" / "b2.jpg"],
    )
    col2_id = col2_location.split("/")[-1]
    col3_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 3",
        pic_names=[FIXTURE_DIR / "col3" / "1.jpg", FIXTURE_DIR / "col3" / "2.jpg", FIXTURE_DIR / "col3" / "3.jpg"],
    )
    col3_id = col3_location.split("/")[-1]

    col4_location = import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_2_url,
        title="collection 3",
        pic_names=[FIXTURE_DIR / "col4" / "e5.jpg"],
    )
    col4_id = col4_location.split("/")[-1]
    # harvest both instances
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )
    harvest(
        instance=Instance(name="geovisio_instance_2", root_url=external_services.geovisio_instance_2_url),
        external_services=external_services,
    )
    with psycopg.connect(external_services.db_url) as db:
        # to get something consistent over time, we change the uploading time to a fixed date
        db.execute(
            "UPDATE items SET content = jsonb_set(content, '{properties,created}', '\"2023-02-09T10:12:37.356010+00:00\"', false) WHERE collection_id = %s;",
            [col1_id],
        )
        db.execute(
            "UPDATE collections SET content = jsonb_set(content, '{created}', '\"2023-02-09T10:12:37.356010+00:00\"', false) WHERE id = %s;",
            [col1_id],
        )
        db.execute(
            "UPDATE items SET content = jsonb_set(content, '{properties,created}', '\"2024-01-09T10:12:37.356010+00:00\"', false) WHERE collection_id = %s;",
            [col2_id],
        )
        db.execute(
            "UPDATE collections SET content = jsonb_set(content, '{created}', '\"2024-01-09T10:12:37.356010+00:00\"', false) WHERE id = %s;",
            [col2_id],
        )
        db.execute(
            "UPDATE items SET content = jsonb_set(content, '{properties,created}', '\"2023-02-19T12:12:37.356010+00:00\"', false) WHERE collection_id = %s;",
            [col3_id],
        )
        db.execute(
            "UPDATE collections SET content = jsonb_set(content, '{created}', '\"2023-02-19T12:12:37.356010+00:00\"', false) WHERE id = %s;",
            [col3_id],
        )

        instance_ids = db.execute("SELECT id, name FROM instances;").fetchall()
        instance_ids = {name: id for id, name in instance_ids}
        db.execute(
            "UPDATE providers SET name = 'bob' WHERE instance_id = %s",
            [instance_ids["geovisio_instance_2"]],
        )
        bobette_id = "12345678-1234-5678-0000-567812345678"
        db.execute(
            "INSERT INTO providers (id, name, instance_id) VALUES (%s, 'bobette', %s);",
            [bobette_id, instance_ids["geovisio_instance_2"]],
        )
        provider_ids = db.execute("SELECT id, name FROM providers;").fetchall()

        db.execute(
            "UPDATE collections SET provider_id = %(bobette)s WHERE id = %(col4)s;",
            {"bobette": bobette_id, "col4": col4_id},
        )

        # refresh stats materialized views and grid
        db.execute("SELECT stats.refresh_all_views();")
        db.execute("REFRESH MATERIALIZED VIEW items_grid;")

        provider_ids = db.execute("SELECT p.id, p.name, i.name FROM providers p JOIN instances i on i.id = p.instance_id;").fetchall()

        return Dataset(
            provider_ids={f"{name}@{instance}": str(id) for id, name, instance in provider_ids},
            collection_ids={"col1": col1_id, "col2": col2_id, "col3": col3_id, "col4": col4_id},
        )
