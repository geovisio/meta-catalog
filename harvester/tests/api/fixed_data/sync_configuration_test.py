import psycopg
from harvester.instance_configuration import sync_instances_configuration


def test_sync_configuration(external_services, dataset):
    sync_instances_configuration(
        db=external_services.db_url,
        instances_name=["geovisio_instance_1", "geovisio_instance_2"],
    )
    with psycopg.connect(external_services.db_url) as db:
        assert db.execute("SELECT configuration FROM instances WHERE name = 'geovisio_instance_1';").fetchone()[0] == {
            "auth": {
                "enabled": False,
            },
            "color": "#bf360c",
            "description": {
                "label": "The open source photo mapping solution",
                "langs": {"en": "The open source photo mapping solution"},
            },
            "email": "panoramax@panoramax.fr",
            "geo_coverage": {
                "label": "Worldwide\nThe picture can be sent from anywhere in the world.",
                "langs": {"en": "Worldwide\nThe picture can be sent from anywhere in the world."},
            },
            "license": {
                "id": "proprietary",
            },
            "logo": "https://gitlab.com/panoramax/gitlab-profile/-/raw/main/images/logo.svg",
            "name": {
                "label": "GeoVisio",
                "langs": {"en": "GeoVisio"},
            },
            "pages": [],
            "version": "2.8.0",
        }
        assert db.execute("SELECT configuration FROM instances WHERE name = 'geovisio_instance_2';").fetchone()[0] == {
            "auth": {"enabled": False},
            "logo": "https://gitlab.com/panoramax/gitlab-profile/-/raw/main/images/logo.svg",
            "name": {"label": "GeoVisio", "langs": {"en": "GeoVisio"}},
            "color": "#bf360c",
            "email": "panoramax@panoramax.fr",
            "license": {"id": "proprietary"},
            "pages": [],
            "version": "2.8.0",
            "description": {
                "label": "The open source photo mapping solution",
                "langs": {"en": "The open source photo mapping solution"},
            },
            "geo_coverage": {
                "label": "Worldwide\nThe picture can be sent from anywhere in the world.",
                "langs": {"en": "Worldwide\nThe picture can be sent from anywhere in the world."},
            },
        }
        updated_ts = db.execute("SELECT configuration_updated_at FROM instances;").fetchall()
        assert all(u is not None for u in updated_ts)
