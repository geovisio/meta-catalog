from dataclasses import dataclass
from typing import Dict


@dataclass
class Dataset:
    provider_ids: Dict[str, str]
    collection_ids: Dict[str, str]
