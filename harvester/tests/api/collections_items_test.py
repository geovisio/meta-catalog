import requests
from ..conftest import harvest, import_collection_to_geovisio, ExternalServicesUrl, FIXTURE_DIR, cleanup_geovisio_instance, cleanup_db
import pytest
from harvester import Instance


# Tests in this module needs to import a dataset as a fixture to get the needed data loaded.
# Check all dataset to know which data is loaded
# Note: no test in this module should load data, if different data are needed, create a new dataset


@pytest.fixture(scope="module")
def cleanup_all_databases(external_services: ExternalServicesUrl, geovision_instance_1_auth_headers):
    """Delete all collections"""
    cleanup_geovisio_instance(external_services.geovisio_instance_1_url, geovision_instance_1_auth_headers)
    cleanup_db(external_services)


@pytest.fixture(scope="module", autouse=True)
def dataset_1(external_services, cleanup_all_databases):
    """
    In those tests, we want a collection with many items

    So we add all fixture's pictures in one collection
    """
    import_collection_to_geovisio(
        instance_url=external_services.geovisio_instance_1_url,
        title="another collections",
        pic_names=[
            FIXTURE_DIR / "col1" / "e1.jpg",
            FIXTURE_DIR / "col1" / "e2.jpg",
            FIXTURE_DIR / "col1" / "e3.jpg",
            FIXTURE_DIR / "col2" / "b1.jpg",
            FIXTURE_DIR / "col2" / "b2.jpg",
            FIXTURE_DIR / "col3" / "1.jpg",
            FIXTURE_DIR / "col3" / "2.jpg",
            FIXTURE_DIR / "col3" / "3.jpg",
            FIXTURE_DIR / "col3" / "4.jpg",
            FIXTURE_DIR / "col3" / "5.jpg",
        ],
    )
    harvest(
        instance=Instance(name="geovisio_instance_1", root_url=external_services.geovisio_instance_1_url),
        external_services=external_services,
    )


def get_link(links, rel):
    return next((li["href"] for li in links if li["rel"] == rel), None)


def test_get_collection_items(external_services):
    """We should be able to a specific collection items"""
    r = requests.get(f"{external_services.api_url}/api/collections")
    r.raise_for_status()
    col_id = r.json()["collections"][0]["id"]

    r = requests.get(f"{external_services.api_url}/api/collections/{col_id}/items", allow_redirects=False)
    r.raise_for_status()
    assert len(r.json()["features"]) == 10

    # since all returned items are here, no pagination links
    assert get_link(r.json()["links"], "next") is None

    # we also check that, if the link was relative in the initial instance, the link is now absolute
    instance_1_host = external_services.geovisio_instance_1_url.removesuffix("/api")
    for f in r.json()["features"]:
        assets = f["assets"]
        assert assets
        assert assets["hd"]["href"].startswith(f"{instance_1_host}/main-pictures/")
        # derivates, are not served through a relative path, their path should be unchanged
        assert assets["hd"]["href"].startswith(f"{instance_1_host}/main-pictures/")
        for k, a in assets.items():
            if k == "hd":
                continue
            assert a["href"].startswith(f"{instance_1_host}/api/pictures/")


def test_get_collection_limit_items(external_services):
    """We should be able to limit the returned items"""
    r = requests.get(f"{external_services.api_url}/api/collections")
    assert r.status_code == 200, r.json()
    col_id = r.json()["collections"][0]["id"]

    first_items = requests.get(f"{external_services.api_url}/api/collections/{col_id}/items?limit=3", allow_redirects=False)
    first_items.raise_for_status()
    assert len(first_items.json()["features"]) == 3

    # since all returned items are not here, we get a `next` link
    links = first_items.json()["links"]

    assert links == [
        {
            "href": f"{external_services.api_url}/api",
            "rel": "root",
            "type": "application/json",
        },
        {
            "href": f"{external_services.api_url}/api/collections",
            "rel": "parent",
            "type": "application/json",
        },
        {
            "href": f"{external_services.api_url}/api/collections/{col_id}",
            "rel": "self",
            "type": "application/json",
        },
        {
            "href": f"{external_services.api_url}/api/collections/{col_id}/items?page=datetime%3E%272021-07-29T09%3A16%3A54%2B00%3A00%27&limit=3",
            "rel": "next",
            "type": "application/json",
        },
    ]

    def capture_time(r):
        return [c["properties"]["datetime"] for c in r.json()["features"]]

    all_items = [capture_time(first_items)]
    next_link = get_link(links, "next")
    while next_link:
        r = requests.get(next_link)
        assert r.status_code == 200
        all_items.append(capture_time(r))
        links = r.json()["links"]
        next_link = get_link(links, "next")

    # we should have got the items, by batch of 3
    assert all_items == [
        ["2015-04-25T13:36:17+00:00", "2015-04-25T13:37:48+00:00", "2021-07-29T09:16:54+00:00"],
        ["2021-07-29T09:16:56+00:00", "2021-07-29T09:16:58+00:00", "2021-07-29T09:17:00+00:00"],
        ["2021-07-29T09:17:02+00:00", "2022-10-19T07:56:34+00:00", "2022-10-19T07:56:36+00:00"],
        ["2022-10-19T07:56:38+00:00"],
    ]
