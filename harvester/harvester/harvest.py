from contextlib import asynccontextmanager

import tenacity
from harvester import instance_configuration
from harvester.model import Instance, Config, USER_AGENT
from psycopg_pool import AsyncConnectionPool
from psycopg import AsyncConnection, AsyncCursor
import orjson
from uuid import UUID
from psycopg import sql
from typing import Dict, Any, List, Optional, Set
import logging
from dataclasses import dataclass
from tqdm import tqdm
from datetime import timedelta
from time import perf_counter
import aiohttp
import asyncio
from yarl import URL
import sys


@dataclass
class Harvest:
    id: UUID
    instance: Instance
    instance_id: UUID
    is_incremental: bool = True


NB_RETRIES = 3

logger = logging.getLogger(__name__)


@tenacity.retry(
    stop=tenacity.stop_after_attempt(NB_RETRIES),
    wait=tenacity.wait_exponential(1),
    before_sleep=tenacity.before_sleep_log(logger, logging.DEBUG),
    reraise=True,
)
async def _get_http(session: aiohttp.ClientSession, query: str):
    async with session.get(query, headers={"User-Agent": USER_AGENT}) as response:
        response.raise_for_status()
        return await response.json(encoding="utf-8")


async def get_collections(pool: AsyncConnectionPool, harvest: Harvest):
    """
    Crawl all collections from an instance, using the `next` link to get the next pages
    """
    async with aiohttp.ClientSession() as session:
        params = ""
        if harvest.is_incremental:
            last_harvest_dt = await _get_last_harvest_dt(pool, harvest)
            if last_harvest_dt:
                formated_dt = last_harvest_dt.isoformat().replace("+00:00", "Z")
                params = f"?filter=status IN ('deleted','ready') AND updated > '{formated_dt}'"
        url = URL(f"{harvest.instance.root_url}/collections{params}")
        next_query: Optional[str] = str(url)
        while next_query:
            collections = await _get_http(session, next_query)
            for c in collections["collections"]:
                yield c

            next_query = next(
                (li["href"] for li in collections["links"] if li["rel"] == "next"),
                None,
            )


async def _get_last_harvest_dt(pool: AsyncConnectionPool, harvest: Harvest):
    async with pool.connection() as conn, conn.cursor() as cur:
        await cur.execute(
            """SELECT start
    FROM harvests 
    WHERE 
        instance_id = %(instance)s
    AND id != %(current_harvest)s
    ORDER BY start DESC
    LIMIT 1""",
            {"instance": harvest.instance_id, "current_harvest": harvest.id},
        )
        r = await cur.fetchone()

        return r[0] if r else None


async def get_items(collection_url: str, config: Config):
    async with aiohttp.ClientSession() as session:
        next_query: Optional[str] = f"{collection_url}/items?limit={config.max_items_per_collection}"
        while next_query:
            items = await _get_http(session, next_query)
            for i in items["features"]:
                yield i
            next_query = next(
                (li["href"] for li in items["links"] if li["rel"] == "next"),
                None,
            )


async def _delete_collection(
    cur: AsyncCursor,
    collection: Dict[str, Any],
):
    logging.info(f"Deleting collection {collection['id']}")
    await cur.execute("DELETE FROM collections WHERE id = %(id)s;", {"id": collection["id"]})


async def _import_collection(
    cur: AsyncCursor,
    collection: Dict[str, Any],
    harvest: Harvest,
    config: Config,
):
    col_id = collection["id"]
    logging.debug(f"Loading collection {col_id}")

    collection_url = next(li["href"] for li in collection["links"] if li["rel"] == "self")
    _update_collection_links(collection)

    await cur.execute(
        "SELECT true FROM collections WHERE id = %(id)s",
        {
            "id": col_id,
        },
    )
    existing = True if await cur.fetchone() else False

    await cur.execute(
        """
        INSERT INTO collections (content, instance_id)
        VALUES (%(col)s, %(instance)s)
        ON CONFLICT (id) DO
        UPDATE SET content=EXCLUDED.content;
        """,
        {
            "col": orjson.dumps(collection).decode(),
            "instance": harvest.instance_id,
        },
    )

    status = "updated" if existing else "created"
    await cur.execute(
        """
        INSERT INTO collection_harvest (collection_id, harvest_id, status)
        VALUES (%(col)s, %(harvest)s, %(status)s); -- TODO Handle update/delete
        """,
        {"col": col_id, "harvest": harvest.id, "status": status},
    )

    all_items = get_items(collection_url, config)
    # creating a temporary table, only visible for this transaction, inited with the same schema as the `items` table.
    await cur.execute("CREATE TEMP TABLE items_ingest_temp ON COMMIT DROP AS SELECT * FROM items LIMIT 0;")
    async with cur.copy(
        """
            COPY items_ingest_temp
            (content, datetime)
            FROM stdin;
            """,
    ) as copy:
        async for item in all_items:
            _update_links(item, harvest.instance, config.host_name)
            _update_asset_links(item, harvest.instance)
            await copy.write_row(
                (
                    orjson.dumps(item).decode(),
                    item.get("properties", {})["datetime"],
                ),
            )

    await cur.execute(
        sql.SQL("DELETE FROM items WHERE collection_id = %(col)s"),
        params={"col": col_id},
    )
    await cur.execute(
        sql.SQL(
            """
            INSERT INTO items (content, datetime) SELECT content, datetime FROM items_ingest_temp;
            """,
        )
    )
    # compute the collection fields that needs all the collection's item
    await cur.execute("SELECT compute_collections_fields(%(id)s)", {"id": col_id})


async def _mark_collection_harvest_as_failed(
    cur: AsyncCursor,
    collection: Dict[str, Any],
    harvest: Harvest,
    error: Exception,
):
    await cur.execute(
        sql.SQL(
            """INSERT INTO harvest_errors (collection_id, harvest_id, error)
        VALUES (%(collection_id)s, %(harvest_id)s, %(error)s)""",
        ),
        {
            "collection_id": collection["id"],
            "harvest_id": harvest.id,
            "error": str(error),
        },
    )


async def sync_collection(
    pool: AsyncConnectionPool,
    collection: Dict[str, Any],
    harvest: Harvest,
    config: Config,
):
    async with pool.connection() as conn:
        async with conn.cursor() as cur:
            try:
                async with conn.transaction():
                    if collection.get("geovisio:status") == "deleted":
                        await _delete_collection(cur, collection)
                    else:
                        await _import_collection(cur, collection, harvest, config)
            except Exception as e:
                logging.exception(f"Impossible to harvest collection {collection['id']}, marking its harvest as failed")
                await _mark_collection_harvest_as_failed(cur, collection, harvest, e)


def _update_links(item: Dict[str, Any], instance: Instance, new_hostname: str):
    for li in item.get("links", []):
        li["href"] = li["href"].replace(instance.root_url, new_hostname)


def _update_collection_links(item: Dict[str, Any]):
    # collection links are recomputed by the API
    item.pop("links", None)


def _update_asset_links(item: Dict[str, Any], instance: Instance):
    for a in item.get("assets", {}).values():
        href = a["href"]
        if href.startswith("/"):
            # handle relative assets href, they should point to their origin instance
            href = instance.origin_url + href

        a["href"] = href


@asynccontextmanager
async def init_harvest(conn: AsyncConnection, instance: Instance, incremental: bool):
    async with conn.cursor() as cur:
        instance_id = await (
            await cur.execute(
                """WITH res as (
 INSERT INTO instances (name, url) 
 VALUES (%(name)s, %(url)s) 
 ON CONFLICT (name) DO NOTHING 
 RETURNING ID
 )
SELECT id FROM res
 UNION ALL
SELECT id FROM instances WHERE name=%(name)s
""",
                {"name": instance.name, "url": instance.root_url},
            )
        ).fetchone()
        assert instance_id
        # acquire an advisory lock on the instance, to prevent concurent harvests
        # this lock will be released at the end of the session, so at the end of the harvest
        lock_available = await (
            await cur.execute(
                "SELECT pg_try_advisory_lock(idx) FROM instances WHERE id = %(id)s;",
                {"id": instance_id[0]},
            )
        ).fetchone()

        if lock_available and not lock_available[0]:
            logging.info("Another crawl is already in progress for this instance, nothing to do")
            sys.exit()

        harvest_id_r = await (
            await cur.execute(
                "INSERT INTO harvests (instance_id, is_incremental) VALUES (%(id)s, %(is_incremental)s) RETURNING id;",
                {
                    "id": instance_id[0],
                    "is_incremental": incremental,
                },
            )
        ).fetchone()
        assert harvest_id_r
        harvest_id = harvest_id_r[0]

        yield Harvest(
            id=harvest_id,
            instance=instance,
            instance_id=instance_id[0],
            is_incremental=incremental,
        )

        await cur.execute(
            "UPDATE harvests SET finished_at = now() WHERE id = %(id)s",
            {"id": harvest_id},
        )


async def cleanup_after_harvest(conn: AsyncConnection, harvest: Harvest):
    """Cleanup after harvest. For non incremental harvests, it means deleting collection that are not anymore in the harvested instance"""
    async with conn.cursor() as cur:
        if harvest.is_incremental:
            return  # nothing to do, deleted collections have beeen handled during import

        r = await cur.execute(
            """WITH
last_harvested_collections AS (
SELECT * from collection_harvest WHERE harvest_id = %(harvest_id)s
)
, deleted_collections AS (
    SELECT id from collections WHERE id not IN (SELECT collection_id from last_harvested_collections) AND instance_id = %(instance_id)s
)
, cleanup_old_collection_harvest AS (
-- also cleanup collection_harvest as it is useless to keep the old ones for non incremmental updates
DELETE FROM collection_harvest WHERE harvest_id NOT IN (SELECT harvest_id from last_harvested_collections) AND collection_id IN (SELECT collection_id from last_harvested_collections)
)
DELETE FROM collections WHERE id IN (select id from deleted_collections);""",
            {"harvest_id": harvest.id, "instance_id": harvest.instance_id},
        )
        logging.info(f"🧹 {r.rowcount} collections deleted because they have been deleted from their instance since last harvest")


class Harvester:
    def __init__(self, cnx_pool) -> None:
        self.db: AsyncConnectionPool = cnx_pool

    async def harvest(self, instance: Instance, config: Config):
        async with (
            self.db.connection() as conn,
            init_harvest(conn, instance, incremental=config.incremental_harvest) as harvest,
        ):
            logging.info(
                f"🚀 Starting {'⚡ incremental' if harvest.is_incremental else '🌀 full'} harvest {harvest.id} of instance {harvest.instance.name} ({harvest.instance.root_url})"
            )
            await instance_configuration.sync_if_needed(conn, instance.name)

            all_col = get_collections(self.db, harvest)
            with (
                tqdm(
                    desc="⚙️ Task to import collections",
                    disable=config.raw_log,
                    unit=" tasks",
                ) as pb_task_launched,
                tqdm(
                    desc="✅ Collections imported",
                    disable=config.raw_log,
                    unit=" col",
                ) as pb_imported,
            ):
                ongoing_tasks: Set[asyncio.Task] = set()

                start = perf_counter()
                collection_idx = 0
                async for c in all_col:
                    if config.collections_limit and collection_idx >= config.collections_limit:
                        logging.info("Reaching limit of collections to import, stopping harvest")
                        break

                    logging.debug(f"asking to import {c['id']}")
                    if len(ongoing_tasks) >= config.nb_concurrent_collections:
                        # Note: to limit the number of concurent task to run, I did not find better than this home made solution
                        # Wait for some import to finish before adding a new one
                        _done, ongoing_tasks = await asyncio.wait(ongoing_tasks, return_when=asyncio.FIRST_COMPLETED)
                    task = asyncio.ensure_future(sync_collection(self.db, c, harvest, config))

                    # when the task is finished, we update the progress bar
                    task.add_done_callback(lambda _t: pb_imported.update(1))
                    ongoing_tasks.add(task)

                    pb_task_launched.update(1)
                    collection_idx += 1

                # Wait for the remaining downloads to finish
                if ongoing_tasks:
                    await asyncio.wait(ongoing_tasks)

            await cleanup_after_harvest(conn, harvest)

            logging.info(f"🎉 All collections imported in {timedelta(seconds=perf_counter() - start)}")


def harvest_instances(db: str, instances: List[Instance], config: Config):
    async def _harvest_all():
        from psycopg_pool import AsyncConnectionPool

        async with AsyncConnectionPool(db, open=False, min_size=2, max_size=30, kwargs={"autocommit": True}) as pool:
            await pool.open()
            harvester = Harvester(cnx_pool=pool)

            for i in instances:
                await harvester.harvest(i, config)

    asyncio.run(_harvest_all())
