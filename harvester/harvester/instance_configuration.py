import asyncio
from datetime import datetime, timedelta
import logging
from typing import Dict, List, Optional, Tuple
import zoneinfo
import aiohttp
from psycopg import AsyncConnection
from psycopg.types.json import Jsonb

UTC = zoneinfo.ZoneInfo(key="Etc/UTC")

CONFIGURATION_SYNC_INTERVAL = timedelta(days=1)  # we want to sync the configuration every day


async def get_config(instance_root_url: str):
    async with aiohttp.ClientSession() as session:
        async with session.get(f"{instance_root_url}/configuration") as response:
            response.raise_for_status()
            return await response.json(encoding="utf-8")


async def get_configuration(conn: AsyncConnection, instance_name: str) -> Tuple[Optional[Dict], Optional[datetime]]:
    async with conn.cursor() as cur:
        await cur.execute(
            "SELECT configuration, configuration_updated_at AT TIME ZONE 'UTC' FROM instances WHERE name = %(name)s",
            {"name": instance_name},
        )
        r = await cur.fetchone()
        if not r:
            return None, None
        conf, updated_at = r
        if updated_at is not None:
            updated_at = updated_at.astimezone(UTC)
        return conf, updated_at


def need_update(updated_at: Optional[datetime]) -> bool:
    """Update if the configuration is older than expected"""
    return updated_at is None or updated_at < datetime.now(tz=UTC) - CONFIGURATION_SYNC_INTERVAL


async def sync_if_needed(conn: AsyncConnection, instance_name: str):
    config, updated_at = await get_configuration(conn, instance_name)
    if need_update(updated_at):
        await sync_configuration(conn, instance_name)


async def sync_configuration(conn, instance_name: str):
    logging.info(f"🔄 Synchronizing configuration of instance {instance_name}")
    async with conn.cursor() as cur:
        await cur.execute(
            "SELECT url FROM instances WHERE name = %(name)s",
            {"name": instance_name},
        )
        instance_root_url = await cur.fetchone()
        if not instance_root_url:
            raise Exception(f"Instance {instance_name} not found")
        config = await get_config(instance_root_url[0])
        await cur.execute(
            "UPDATE instances SET configuration = %(config)s, configuration_updated_at = now() WHERE name = %(name)s",
            {"config": Jsonb(config), "name": instance_name},
        )


def sync_instances_configuration(db: str, instances_name: List[str]):
    """Synchronize the configuration of the instances"""

    async def _harvest_configuration():
        from psycopg_pool import AsyncConnectionPool

        async with AsyncConnectionPool(db, open=False, min_size=1, max_size=10, kwargs={"autocommit": True}) as pool:
            await pool.open()
            async with pool.connection() as conn:
                for instance_name in instances_name:
                    await sync_configuration(conn=conn, instance_name=instance_name)

    asyncio.run(_harvest_configuration())
