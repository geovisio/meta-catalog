from harvester import model, harvest_instances
import typer
from typing_extensions import Annotated
from typing import Optional, List
from pathlib import Path
import logging


def get_instances(instances_file: Path) -> List[model.Instance]:
    """Read instances from a CSV file.
    The CSV should have the columns:
    * `id`: identifier of the instance
    * `api_url`: Root url of the stac catalog. Note: for geovisio instances, don't forget the trailing `/api`
    """
    from csv import DictReader

    with open(instances_file, "r") as f:
        reader = DictReader(f)
        return [
            model.Instance(
                name=r["id"],
                root_url=r["api_url"],
            )
            for r in reader
        ]


app = typer.Typer()


@app.command()
def harvest(
    db: Annotated[
        str,
        typer.Option(
            help="Connection string to a pgstac database. Should be formated as postgresql://{username}:{password}@{host}:{port}/{database}",
            envvar="DB_URL",
        ),
    ],
    target_hostname: Annotated[
        str,
        typer.Option(
            help="Host where the date will be accessed from. It will be used to change all STAC links.",
            envvar="TARGET_HOSTNAME",
        ),
    ],
    instances_files: Annotated[
        Optional[Path],
        typer.Option(help="Instances list csv file", envvar="INSTANCES_FILE"),
    ] = None,
    instance_name: Annotated[
        Optional[str],
        typer.Option(
            help="Id of the unique instance to harvest. If several instances needs to be harvested, use `--instances-files` instead.",
            envvar="INSTANCE_NAME",
        ),
    ] = None,
    instance_url: Annotated[
        Optional[str],
        typer.Option(
            help="URL of the unique instance to harvest. If several instances needs to be harvested, use `--instances-files` instead.",
            envvar="INSTANCE_URL",
        ),
    ] = None,
    incremental_harvest: Annotated[
        bool,
        typer.Option(
            "--incremental-harvest/--full-harvest",
            help="Tell if the harvest should be incremental or if all the data should be crawled again.",
            envvar="INCREMENTAL_HARVEST",
        ),
    ] = True,
    debug: Annotated[bool, typer.Option(help="Bool to have more logs", envvar="DEBUG")] = False,
    collections_limit: Annotated[
        Optional[int],
        typer.Option(
            help="Maximum number of collection to import. Mainly used for debug",
            envvar="COLLECTIONS_LIMIT",
        ),
    ] = None,
    raw_log: Annotated[
        bool,
        typer.Option(
            help="Only raw log, no progress bas",
            envvar="RAW_LOG",
        ),
    ] = False,
    nb_concurrent_collections: Annotated[
        int,
        typer.Option(
            help="Number of collection to import concurrently (but only on 1 thread)",
            envvar="NB_CONCURRENT_COLLECTIONS",
        ),
    ] = 5,
):
    """Harvest a STAC catalog, and import it in the database"""

    base_log_level = logging.DEBUG if debug else logging.INFO
    logging.basicConfig(level=base_log_level)

    if instances_files is not None:
        instances = get_instances(instances_files)
    else:
        if instance_name is None or instance_url is None:
            raise Exception("An instances csv file should be provided, or the id/url of the instance to harvest")
        instances = [
            model.Instance(
                name=instance_name,
                root_url=instance_url,
            )
        ]
    logging.info(f"harvesting data from {[i.name for i in instances]}")

    config = model.Config(
        host_name=target_hostname,
        collections_limit=collections_limit,
        raw_log=raw_log,
        nb_concurrent_collections=nb_concurrent_collections,
        incremental_harvest=incremental_harvest,
    )

    harvest_instances(db, instances, config)


@app.command()
def sync_configuration(
    db: Annotated[
        str,
        typer.Option(
            help="Connection string to a pgstac database. Should be formated as postgresql://{username}:{password}@{host}:{port}/{database}",
            envvar="DB_URL",
        ),
    ],
    instance: Annotated[
        List[str],
        typer.Option(help="Instances name", envvar="INSTANCES_NAME"),
    ],
):
    """Synchronize the configuration of the instances
    This configuration is fetched from the /api/configuration of each instance and is usefull to give all the instance information (geographic coverage, license, ...)
    """
    from harvester.instance_configuration import sync_instances_configuration

    logging.basicConfig(level=logging.INFO)
    sync_instances_configuration(db, instances_name=instance)


if __name__ == "__main__":
    app()
