from dataclasses import dataclass, field
from typing import Optional
import harvester


@dataclass
class Instance:
    name: str
    root_url: str

    # origin is the scheme + host of the instance (without any path to the url)
    origin_url: str = field(init=False)

    def __post_init__(self):
        from yarl import URL

        new_url = URL(self.root_url)
        self.origin_url = str(new_url.origin())


@dataclass
class Config:
    host_name: str
    collections_limit: Optional[int] = None
    raw_log: bool = False
    nb_concurrent_collections: int = 1
    incremental_harvest: bool = True
    max_items_per_collection: int = 2000


USER_AGENT = f"stac-harvester_{harvester.__version__}"
