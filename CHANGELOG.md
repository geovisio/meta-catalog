# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

This project is based on [eoAPI](https://github.com/developmentseed/eoAPI), this file only lists changes since the fork.

## [Unreleased]

### Added

- Add a `/api/reports` endpoint to post a report. The report is forwarded to the picture's instance.
- Add the number of 360/flat items in the grid features of the vector tiles. Also add a new metadata property `panoramax:fields` to lists all available properties in each layer (pictures, sequences, grid).
- Add a `/api/instances` endpoint to list all instances.
- Add the instance configuration in the `/api/instances` and `/api/intances/:id` endpoints.

### Changed

- The harvester now uses the collections's item pagination to avoid very big responses.
- Now the default harvest is incremental, full harvest can be asked with the `--full-harvest` option (or with `INCREMENTAL_HARVEST=false` environment variable). Incremental harvest ask the instance what have changed since the last harvest. Full harvest can be usefull when there is a major update in the instance and we want to crawl all over again.

### Removed

- Removed the computation of the coverage of the pictures `approximated_coverage_km` from the `/api/stats` endpoint. It is too buggy for the moment, and we'll need time to compute it correctly.

## [0.1.0] - 2024-11-05

### Changed

- STAC API moved to `api/metacatalog`
- Pictures's are now displayed from zoom level 15 in the tiles
- Limit to 1000 for number of items that can be retrieved in one call in `/api/collections/:id/items`. The default limit is 100.
- stats are refresh concurrently
- Change some `data` links to `child` links for better STAC conformance
- Bump minimal required Rust version to 1.78.0
- In vector tiles style, `interpolate-hcl` expression for grid colouring has been replaced into `interpolate` for broader compatibility with QGIS versions.
- Now correctly handle relative url in harvested instances

### Added

- Collections filtering by bounding box
- Add a `sortby` parameter to `/api/collections` to sort the results. For the moment we can only sort by creation date (`sortby=created`) or update date (`sortby=updated`). Sort order can be defined by prefixing either `+` for ascending sort (or nothing, since ascending is the default), or `-` for descending sort. Note that this parameter needs to be url encoded (so `%2B` instead of `+`).
- Add pagination links on `/api/collections`
- Validation for `/api/map` parameters
- Add pagination links to the items of a collection (`next` link in the `links` section).
- prometheus metrics endpoint en `/metrics`
- Status endpoint: `/status` with some metrics and `/live` just to know if the service is live.
- provider search endpoint: `/api/users/searchq=some_search`
- collection filtering with a provider id: `/api/collections?provider_id=<some_uuid>`
- user's tiles: `/api/users/:id/map/x/y/z.mvt`
- Add users's catalog endpoints: `/api/users` and `/api/users/:id/catalog`
- Add stats on `/api/stats` and `/api/stats/by_contributor`
- Add CQL2 filter support for /api/search

### Deleted

- Removed `vector` and `raster` Docker components to only keep the STAC stack.
- Removed the index on the items's `datetime` field in the database.

[Unreleased]: https://gitlab.com/panoramax/server/meta-catalog/-/compare/0.1.0...develop
[0.1.0]: https://gitlab.com/panoramax/meta-catalog/-/commits/0.1.0
