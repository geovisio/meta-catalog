# ![Panoramax](https://upload.wikimedia.org/wikipedia/commons/thumb/a/a9/Panoramax.svg/40px-Panoramax.svg.png) Panoramax

__Panoramax__ is a digital resource for sharing and using 📍📷 field photos. Anyone can take photographs of places visible from the public streets and contribute them to the Panoramax database. This data is then freely accessible and reusable by all. More information available at [gitlab.com/panoramax](https://gitlab.com/panoramax) and [panoramax.fr](https://panoramax.fr/).


# 📖 GeoVisio Meta-Catalog

This repository only contains the **meta-catalog**, ie the search engine to find pictures accross all [Panoramax](https://panoramax.fr/) STAC instances.

## Features

### Crawler

An **API crawler** to download and import STAC instances metadata inside the database (code in `./harvester`).

The crawler stores the data in a PostgreSQL database directly in JSONB.

### API

A **web API** to search pictures collections (code in `./api`)

  - Compatible with [SpatioTemporal Asset Catalog](https://stacspec.org/) (abbreviated as STAC) and [OGC WFS 3](https://github.com/opengeospatial/WFS_FES) specifications
  - Also able to serve vector tiles to easily display the data on a map.

The API is a thin wrapper around the PostgreSQL database and expose the data crawled.

The API documentation is available in [OpenAPI format](https://api.panoramax.xyz/api/docs/swagger) (or if you prefer, the documentation is also available in [ReDoc](https://api.panoramax.xyz/redoc)).

## Contributing

Pull requests are welcome. For major changes, please open an [issue](https://gitlab.com/panoramax/server/meta-catalog/-/issues) first to discuss what you would like to change.

More specific documentation on development and contribution is available in [contributing doc](./CONTRIBUTING.md).

## ⚖️ License

This project is based on [Development Seed](http://developmentseed.org)'s [eoAPI](https://github.com/developmentseed/eoAPI) solution. It was forked to meet Panoramax needs.

Copyright (c) Panoramax team 2023-2024, Copyright (c) 2021 Development Seed, [released under MIT license](https://gitlab.com/panoramax/server/meta-catalog/-/blob/develop/LICENSE).
