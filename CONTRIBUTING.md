# Development - Contributing

Issues and pull requests are more than welcome: https://gitlab.com/panoramax/server/meta-catalog/-/issues

## Install for local development

```bash
# Download the code
git clone https://gitlab.com/panoramax/server/meta-catalog.git
cd meta-catalog

# Create a virtual environment
python -m pip install --upgrade virtualenv
virtualenv .venv
source .venv/bin/activate
```

Note: services might have incompatible dependencies which you can resolve by using virtual environnement per service

### Mock data

For specific development needs, some mock data can be imported in database instead of fetching it from Panoramax instances. Run the following commands to proceed:

```bash
# These commands are based on a local development environment
#   If you work with Docker, you may install pypgstac before
pypgstac load collections tests/fixtures/osmfr_collections --dsn postgresql://{db-user}:{db-password}@{db-host}:{db-port}/{db-name}
pypgstac load items tests/fixtures/osmfr_items --dsn postgresql://{db-user}:{db-password}@{db-host}:{db-port}/{db-name}
```
