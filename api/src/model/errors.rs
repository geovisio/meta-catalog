use actix_web::{
    error::QueryPayloadError, http::StatusCode, HttpRequest, HttpResponse, ResponseError,
};
use apistos::ApiErrorComponent;
use serde::Serialize;
use thiserror::Error;

#[derive(Debug, Error, ApiErrorComponent)]
#[openapi_error(status(code = 400), status(code = 404))]
pub enum APIError {
    #[error("Invalid query")]
    InvalidQuery(#[from] sqlx::Error),
    #[error("Invalid parameter: {0}")]
    InvalidParameter(String),
    #[error("{obj_type} not found")]
    NotFound { id: String, obj_type: &'static str },
    #[error("Impossible to find items for collection {id}")]
    ItemNotFoundForCollection { id: uuid::Uuid },
    #[error("{0}")]
    QueryParameterError(String), // errors from actix query parameter deserialization
    #[error("Impossible to query instance {url}")]
    ImpossibleToQueryInstance { url: String },
}

#[derive(Serialize, Debug)]
struct JsonError {
    status: u16,
    message: String,
    #[serde(skip)]
    detail: Option<String>,
}

impl ResponseError for APIError {
    fn error_response(&self) -> HttpResponse {
        let status = self.status_code();
        log::info!("error: {:?}", self);

        HttpResponse::build(status).json(JsonError::from(self))
    }

    fn status_code(&self) -> StatusCode {
        match *self {
            APIError::InvalidQuery(ref sql_error) => match sql_error {
                sqlx::Error::PoolTimedOut | sqlx::Error::PoolClosed => {
                    log::error!("impossible to connect to database: {}", self);
                    StatusCode::INTERNAL_SERVER_ERROR
                }
                _ => StatusCode::INTERNAL_SERVER_ERROR,
            },
            APIError::NotFound { id: _, obj_type: _ } => StatusCode::NOT_FOUND,
            APIError::ItemNotFoundForCollection { id: _ } => StatusCode::NOT_FOUND,
            APIError::InvalidParameter(_) => StatusCode::BAD_REQUEST,
            APIError::QueryParameterError(_) => StatusCode::BAD_REQUEST,
            APIError::ImpossibleToQueryInstance { url: _ } => StatusCode::BAD_GATEWAY,
        }
    }
}

impl JsonError {
    fn new(err: &APIError) -> Self {
        JsonError {
            status: err.status_code().as_u16(),
            message: err.to_string(),
            detail: None,
        }
    }

    fn detail(mut self, d: String) -> Self {
        self.detail = Some(d);
        self
    }
}

impl From<&APIError> for JsonError {
    fn from(e: &APIError) -> JsonError {
        match e {
            APIError::InvalidQuery(sql_error) => match sql_error {
                sqlx::Error::RowNotFound => JsonError {
                    status: e.status_code().as_u16(),
                    message: "Impossible to find object".to_string(),
                    detail: None,
                },
                _ => JsonError::new(e).detail(sql_error.to_string()),
            },

            APIError::NotFound { id: _, obj_type: _ } => JsonError::new(e),
            APIError::ItemNotFoundForCollection { id: _ } => JsonError::new(e),
            APIError::InvalidParameter(detail) => JsonError::new(e).detail(detail.clone()),
            APIError::QueryParameterError(detail) => JsonError::new(e).detail(detail.clone()),
            APIError::ImpossibleToQueryInstance { url: _ } => JsonError::new(e),
        }
    }
}

// Customize how query error are represented
pub fn query_error_handler(err: QueryPayloadError, _req: &HttpRequest) -> actix_web::Error {
    APIError::QueryParameterError(err.to_string()).into()
}
