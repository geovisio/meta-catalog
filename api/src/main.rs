use actix_cors::Cors;
use actix_web::web::{get, head, Data, QueryConfig};
use actix_web::{http::header, middleware, App, HttpServer};
use actix_web_opentelemetry::{RequestMetrics, RequestTracing};
use anyhow::Context;
use apistos::app::{BuildConfig, OpenApiWrapper};
use apistos::{RedocConfig, SwaggerUIConfig};
use awc::Client;
use clap::Parser;
use sqlx::postgres::PgPoolOptions;
use std::net::Ipv4Addr;
use utils::matomo::MatomoConfig;

mod model;
mod routes;
mod utils;

#[derive(Parser, Clone)]
#[command(author, version, about, long_about = None)]
pub struct Config {
    /// Connection string to the database (like `postgres://username:password@localhost:5432/panoramax`)
    #[clap(long, env)]
    pub db_url: String,

    /// Port to bind the API on
    #[clap(long, short, env, default_value = "9000")]
    pub port: u16,

    /// Limit the number of workers used to handle API requests.
    /// By default uses the number of available CPUs.
    #[clap(long, short, env)]
    pub nb_workers: Option<usize>,

    /// URL to a matomo instance to send usage on
    #[clap(long, env)]
    pub matomo_url: Option<String>,
    /// ID site in the matomo instance
    #[clap(long, env)]
    pub matomo_site_id: Option<u16>,
}

#[actix_web::main]
async fn main() -> Result<(), anyhow::Error> {
    let config = Config::parse();
    let (metrics_handler, metrics_provider) = crate::utils::tracing::init_tracing_and_metrics();

    let db = PgPoolOptions::new()
        .min_connections(2)
        .connect(&config.db_url)
        .await
        .expect("impossible to connect to db");

    let matomo_config = MatomoConfig {
        url: config.matomo_url,
        site_id: config.matomo_site_id.map(|s| s.to_string()),
    };

    let mut s = HttpServer::new(move || {
        let cors = Cors::default()
            .allow_any_origin()
            .allowed_methods(vec!["GET"])
            .allowed_headers(vec![
                header::CONTENT_TYPE,
                header::AUTHORIZATION,
                header::ACCEPT,
            ]);

        // This factory closure is called on each worker thread independently.
        App::new()
            .route("/metrics", get().to(metrics_handler.clone()))
            .route("/metrics", head().to(metrics_handler.clone()))
            .document(utils::doc::doc())
            .app_data(Data::new(db.clone()))
            .app_data(Data::new(Client::default()))
            .app_data(Data::new(matomo_config.clone()))
            .app_data(QueryConfig::default().error_handler(model::query_error_handler))
            .wrap(middleware::Logger::default().exclude("/metrics"))
            .wrap(middleware::NormalizePath::new(
                middleware::TrailingSlash::Trim,
            ))
            .wrap(middleware::Compress::default())
            .wrap(cors)
            .wrap(RequestTracing::new())
            .wrap(RequestMetrics::default())
            .configure(routes::mount)
            .build_with(
                "/openapi.json",
                BuildConfig::default()
                    .with(RedocConfig::new(&"/redoc"))
                    .with(SwaggerUIConfig::new(&"/swagger")),
            )
    });

    if let Some(nb_workers) = config.nb_workers {
        s = s.workers(nb_workers);
    }

    s.bind((Ipv4Addr::UNSPECIFIED, config.port))
        .context("impossible to bind port")?
        .run()
        .await?;

    // Ensure all spans have been reported
    opentelemetry::global::shutdown_tracer_provider();

    metrics_provider
        .shutdown()
        .context("impossible to shutdown metrics provider")
}
