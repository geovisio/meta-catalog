use actix_web::{
    web::{Data, Json, Path},
    HttpRequest,
};
use apistos::{api_operation, ApiComponent, JsonSchema};
use chrono::{DateTime, Utc};
use serde::Serialize;
use sqlx::PgPool;
use stac::{Link, TemporalExtent};
use uuid::Uuid;

use crate::{model::APIError, utils::link};

/// List of Panoramax instance
#[derive(Serialize, ApiComponent, JsonSchema)]
pub struct Instances {
    pub instances: Vec<Instance>,
}

/// Panoramax instance
#[derive(Serialize, ApiComponent, JsonSchema)]
pub struct Instance {
    /// Id of the instance
    pub id: Uuid,
    /// Name of the instance
    pub name: String,
    /// Url of the instance
    pub url: String,
    #[schemars(schema_with = "extent_schema")]
    pub extent: Extent,

    /// Additional links on the instance.
    #[schemars(schema_with = "links_schema")]
    pub links: Vec<Link>,

    pub configuration: Option<serde_json::Value>,
}

/// The object describes the spatio-temporal extents of the [Collection](crate::Collection).
#[derive(Debug, Default, Serialize, PartialEq, Clone)]
pub struct Extent {
    /// Temporal extents covered by the `Collection`.
    pub temporal: TemporalExtent,
}

fn extent_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
    apistos::Schema::new_ref(
        "https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/extent"
            .into(),
    )
}

fn links_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
    apistos::Schema::new_ref(
        "https://api.stacspec.org/v1.0.0/ogcapi-features/openapi.yaml#/components/schemas/links"
            .into(),
    )
}

#[derive(sqlx::FromRow)]
pub struct InstanceRow {
    id: Uuid,
    name: String,
    url: String,
    min_datetime: Option<DateTime<Utc>>,
    max_datetime: Option<DateTime<Utc>>,
    configuration: Option<serde_json::Value>,
}

impl From<(InstanceRow, &HttpRequest)> for Instance {
    fn from((row, req): (InstanceRow, &HttpRequest)) -> Self {
        Instance {
            id: row.id,
            name: row.name.clone(),
            url: row.url,
            extent: Extent {
                temporal: stac::TemporalExtent {
                    interval: vec![[row.min_datetime, row.max_datetime]],
                },
                ..Default::default()
            },
            links: vec![
                link::from_path(req, &format!("/api/instances/{}", row.name), "self").json(),
            ],
            configuration: row.configuration,
        }
    }
}

/// List of the Panoramax instances harvested
#[api_operation(tag = "Metadata")]
pub async fn list_instances(
    db: Data<PgPool>,
    req: HttpRequest,
) -> Result<Json<Instances>, APIError> {
    let instances = sqlx::query_as(
        r#"SELECT
    id,
    name,
    url,
    min_datetime,
    max_datetime,
    configuration
FROM instances
"#,
    )
    .fetch_all(db.as_ref())
    .await?
    .into_iter()
    .map(|i: InstanceRow| (i, &req).into())
    .collect();
    Ok(Json(Instances { instances }))
}

/// Get one Panoramax instance, by its name
#[api_operation(tag = "Metadata")]
pub async fn get_instance(
    db: Data<PgPool>,
    name: Path<String>,
    req: HttpRequest,
) -> Result<Json<Instance>, APIError> {
    let instance: InstanceRow = sqlx::query_as(
        r#"SELECT
    id,
    name,
    url,
    min_datetime,
    max_datetime,
    configuration
FROM instances
WHERE name = $1
"#,
    )
    .bind(name.as_str())
    .fetch_optional(db.as_ref())
    .await?
    .ok_or(APIError::NotFound {
        id: name.as_str().to_string(),
        obj_type: "Instance",
    })?;
    Ok(Json((instance, &req).into()))
}
