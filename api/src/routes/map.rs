use crate::model::APIError;
use crate::utils::doc::make_response;
use crate::utils::link;
use actix_web::body::BoxBody;
use actix_web::http::StatusCode;
use actix_web::{web, HttpRequest, HttpResponse, Responder, Result};
use apistos::paths::Responses;
use apistos::reference_or::ReferenceOr;
use apistos::{
    api_operation, json_schema, ApiComponent, ApistosSchema, JsonSchema, OpenApiVersion,
};
use serde::{self, Deserialize};
use sqlx::postgres::PgPool;
use sqlx::Row;

const ZOOM_GRID_SEQUENCES: u8 = 6;

#[derive(Deserialize, JsonSchema, ApiComponent)]
pub struct TileRequest {
    /// Zoom level
    z: u8,
    /// X coordinate
    x: u32,
    /// Y coordinate
    y: u32,
    /// Get tile for a specific provider
    provider_id: Option<uuid::Uuid>,
}

impl TileRequest {
    fn validate(self) -> Result<Self, APIError> {
        let size = 2u32.pow(self.z as u32);

        if self.x >= size || self.y >= size {
            return Err(APIError::InvalidParameter(
                "X or Y parameter is out of bound".to_string(),
            ));
        }
        if self.z > 15 {
            return Err(APIError::InvalidParameter(
                "Z parameter is out of bounds (should be 0-15)".to_string(),
            ));
        }

        Ok(self)
    }
}

/// Vector tiles contains different layers based on zoom level : sequences, pictures or grid.

pub struct ApiTile(pub Vec<u8>);

impl Responder for ApiTile {
    type Body = BoxBody;

    fn respond_to(self, _req: &HttpRequest) -> HttpResponse<Self::Body> {
        if self.0.is_empty() {
            HttpResponse::NoContent()
                .content_type("application/vnd.mapbox-vector-tile")
                .finish()
        } else {
            HttpResponse::Ok()
                .content_type("application/vnd.mapbox-vector-tile")
                .body(self.0)
        }
    }
}

impl ApiComponent for ApiTile {
    fn child_schemas(_oas_version: OpenApiVersion) -> Vec<(String, ReferenceOr<ApistosSchema>)> {
        vec![]
    }

    fn schema(_oas_version: OpenApiVersion) -> Option<(String, ReferenceOr<ApistosSchema>)> {
        None
    }

    fn responses(oas_version: OpenApiVersion, _content_type: Option<String>) -> Option<Responses> {
        make_response(
            StatusCode::OK,
            "application/vnd.mapbox-vector-tile",
            json_schema!({"type": "string", "format": "binary"}),
            oas_version,
            r#"
 Layer "sequences":
   - Available on zoom levels >= 6
   - Available properties:
     - id (sequence ID)
     - account_id
     - model (camera make and model)
     - type (flat or equirectangular)
     - date (capture date, as YYYY-MM-DD)

 Layer "pictures":
   - Available on zoom levels >= 13
   - Available properties:
     - id (picture ID)
     - account_id
     - ts (picture date/time)
     - heading (picture heading in degrees)
     - sequences (list of sequences ID this pictures belongs to)
     - type (flat or equirectangular)
     - model (camera make and model)

 Layer "grid":
   - Available on zoom levels 0 to 5 (included)
   - Available properties:
     - id
     - nb_pictures
     - nb_360_pictures (number of 360° pictures)
     - nb_flat_pictures (number of flat pictures)
     - coef (value from 0 to 1, relative quantity of available pictures)
     - coef_360_pictures (value from 0 to 1, relative quantity of available 360° pictures)
     - coef_flat_pictures (value from 0 to 1, relative quantity of available flat pictures)
"#,
        )
    }
}

/// Get a MapBox vector tile containing the collections and the items in the tile's geometry
#[api_operation(tag = "Map")]
pub async fn get_tile(
    db: web::Data<PgPool>,
    r: web::Path<TileRequest>,
) -> Result<ApiTile, APIError> {
    let r = r.into_inner().validate()?;
    let query = tile_query(&r);

    let pg_tile = sqlx::query(&query)
        .bind(r.z as i32)
        .bind(r.x as i32)
        .bind(r.y as i32)
        .fetch_one(db.as_ref())
        .await?;
    Ok(ApiTile(pg_tile.get(0)))
}

const GRID_QUERY: &'static str = r#"SELECT ST_AsMVT(mvtgrid.*, 'grid') AS mvt
FROM (
    SELECT
        ST_AsMVTGeom(ST_Transform(ST_Centroid(geom), 3857), ST_TileEnvelope($1, $2, $3)) AS geom,
        id,
        nb_items AS nb_pictures,
        nb_360_items AS nb_360_pictures,
        nb_items - nb_360_items AS nb_flat_pictures,
        (
            (CASE 
                WHEN nb_items <= (select PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY nb_items) from items_grid)
                    THEN nb_items::float / (select PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY nb_items) from items_grid) * 0.5
                WHEN (SELECT MAX(nb_items) FROM items_grid) = 0
                    THEN 0
                ELSE 
                    0.5 + nb_items::float / (SELECT MAX(nb_items) FROM items_grid) * 0.5
            END
        ) * 10)::int / 10::float AS coef,
        (
            (CASE 
                WHEN (select PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY nb_360_items) from items_grid) = 0
                    THEN 0
                WHEN nb_360_items <= (select PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY nb_360_items) from items_grid)
                    THEN nb_360_items::float / (select PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY nb_360_items) from items_grid) * 0.5
                WHEN (SELECT MAX(nb_360_items) FROM items_grid) = 0
                    THEN 0
                ELSE 
                    0.5 + nb_360_items::float / (SELECT MAX(nb_360_items) FROM items_grid) * 0.5
                END) 
            * 10)::int / 10::float AS coef_360_pictures,
        (
            (CASE 
                WHEN (select PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY (nb_items - nb_360_items)) from items_grid) = 0
                    THEN 0
                WHEN (nb_items - nb_360_items) = 0 
                    THEN 0 
                WHEN (SELECT MAX((nb_items - nb_360_items)) FROM items_grid) = 0
                    THEN 0
                WHEN (nb_items - nb_360_items) <= (select PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY (nb_items - nb_360_items)) from items_grid)
                    THEN (nb_items - nb_360_items)::float / (select PERCENTILE_CONT(0.5) WITHIN GROUP(ORDER BY (nb_items - nb_360_items)) from items_grid) * 0.5
                ELSE 0.5 + (nb_items - nb_360_items)::float / (SELECT MAX((nb_items - nb_360_items)) FROM items_grid) * 0.5
            END)
            * 10
        )::int / 10::float AS coef_flat_pictures
    FROM items_grid g
    WHERE geom && ST_Transform(ST_TileEnvelope($1, $2, $3), 4326)
) mvtgrid"#;

fn tile_query(request: &TileRequest) -> String {
    let col_filter = if let Some(provider_id) = request.provider_id {
        format!("c.provider_id = '{}'", provider_id)
    } else {
        "TRUE".to_string()
    };
    // if we only want one user's data, we never display the grid, always the collections
    let only_for_one_user = request.provider_id.is_some();
    if request.z >= 15 {
        format!(
            r#"
SELECT mvtsequences.mvt || mvtpictures.mvt
FROM (
    SELECT ST_AsMVT(mvtgeomseqs.*, 'sequences') AS mvt
    FROM (
        SELECT
            ST_AsMVTGeom(ST_Transform(computed_geom, 3857), ST_TileEnvelope($1, $2, $3)) AS geom,
            id,
            provider_id AS account_id,
            computed_model AS model, computed_picture_type AS type, computed_capture_date AS date,
            (content#>> '{{summaries,panoramax:horizontal_pixel_density,0}}')::int AS h_pixel_density,
            (content->> 'quality:horizontal_accuracy')::float AS gps_accuracy
            FROM collections c
            WHERE computed_geom && ST_Transform(ST_TileEnvelope($1, $2, $3), 4326) AND {col_filter}
    ) mvtgeomseqs
) mvtsequences,
(
SELECT ST_AsMVT(mvtgeompics.*, 'pictures') AS mvt
FROM (
    SELECT
        ST_AsMVTGeom(ST_Transform(i.geom, 3857), ST_TileEnvelope($1, $2, $3)) AS geom,
        i.id, i.datetime AS ts,
        i.content #>> '{{properties,view:azimuth}}' AS heading,
        c.provider_id AS account_id,
        array_to_json(ARRAY[i.collection_id]) AS sequences,
        i.picture_type AS type,
        TRIM(CONCAT(i.content #>> '{{properties,pers:interior_orientation,camera_manufacturer}}', 
                    ' ', 
                    i.content #>> '{{properties,pers:interior_orientation,camera_model}}')) AS model,
        (i.content#>> '{{properties,panoramax:horizontal_pixel_density}}')::int as h_pixel_density,
        (i.content#>> '{{properties,quality:horizontal_accuracy}}')::float AS gps_accuracy
    FROM items i
    JOIN collections c ON i.collection_id = c.id
    WHERE i.geom && ST_Transform(ST_TileEnvelope($1, $2, $3), 4326) AND {col_filter}
    ) mvtgeompics
) mvtpictures
"#,
            col_filter = col_filter
        )
    } else if request.z >= ZOOM_GRID_SEQUENCES + 1 || only_for_one_user {
        format!(
            r#"
SELECT ST_AsMVT(mvtsequences.*, 'sequences') AS mvt
FROM (
    SELECT
        ST_AsMVTGeom(ST_Transform(computed_geom, 3857), ST_TileEnvelope($1, $2, $3)) AS geom,
        id,
        provider_id AS account_id,
        computed_model AS model, computed_picture_type AS type, computed_capture_date AS date,
        (content#>> '{{summaries,panoramax:horizontal_pixel_density,0}}')::int AS h_pixel_density,
        (content->> 'quality:horizontal_accuracy')::float AS gps_accuracy
        FROM collections c
    WHERE computed_geom && ST_Transform(ST_TileEnvelope($1, $2, $3), 4326) AND {col_filter}
) mvtsequences
"#,
            col_filter = col_filter
        )
    } else if request.z >= ZOOM_GRID_SEQUENCES {
        format!(
            r#"
SELECT mvtsequences.mvt || mvtgrid.mvt
FROM (
    SELECT ST_AsMVT(mvtgeomseqs.*, 'sequences') AS mvt
    FROM (
        SELECT
            ST_AsMVTGeom(ST_Transform(computed_geom, 3857), ST_TileEnvelope($1, $2, $3)) AS geom,
            id,
            provider_id AS account_id,
            computed_model AS model, computed_picture_type AS type, computed_capture_date AS date,
            (content#>> '{{summaries,panoramax:horizontal_pixel_density,0}}')::int AS h_pixel_density,
            (content->> 'quality:horizontal_accuracy')::float AS gps_accuracy
            FROM collections
        WHERE computed_geom && ST_Transform(ST_TileEnvelope($1, $2, $3), 4326)
        ) mvtgeomseqs
    ) mvtsequences,
    (
            {GRID_QUERY}
    ) mvtgrid"#
        )
    } else {
        GRID_QUERY.to_string()
    }
}

fn get_json_style(tile_url: &str, provider_id: Option<uuid::Uuid>) -> serde_json::Value {
    use serde_json::json;
    let only_for_one_user = provider_id.is_some();
    let source_id = if let Some(provider_id) = provider_id {
        format!("geovisio_{}", provider_id)
    } else {
        "geovisio".to_string()
    };

    let line_opacity = if only_for_one_user {
        json!(1)
    } else {
        json!([
            "interpolate",
            ["linear"],
            ["zoom"],
            (ZOOM_GRID_SEQUENCES as f32) + 0.25,
            0,
            ZOOM_GRID_SEQUENCES + 1,
            1
        ])
    };

    let grid = if only_for_one_user {
        json!({})
    } else {
        json!({
            "id": format!("{source_id}_grid"),
            "paint": {
                "circle-radius": [
                    "interpolate",
                    ["linear"],
                    ["zoom"],
                    1,
                    // The match get coef rule allows to hide circle if coef is set to 0
                    ["match", ["get", "coef"], 0, 0, 1],
                    ZOOM_GRID_SEQUENCES - 2,
                    ["match", ["get", "coef"], 0, 0, 6],
                    ZOOM_GRID_SEQUENCES - 1,
                    ["match", ["get", "coef"], 0, 0, 2.5],
                    ZOOM_GRID_SEQUENCES,
                    ["match", ["get", "coef"], 0, 0, 4],
                    ZOOM_GRID_SEQUENCES + 1,
                    ["match", ["get", "coef"], 0, 0, 7],

                ],
                "circle-color": ["interpolate", ["linear"], ["get", "coef"], 0, "#FFA726", 0.5, "#E65100", 1, "#3E2723"],
                "circle-opacity": [
                    "interpolate",
                    ["linear"],
                    ["zoom"],
                    ZOOM_GRID_SEQUENCES - 2,
                    0.5,
                    ZOOM_GRID_SEQUENCES - 1,
                    1,
                    (ZOOM_GRID_SEQUENCES as f32) + 0.75,
                    1,
                    ZOOM_GRID_SEQUENCES + 1,
                    0,
                ],
            },
            "source": source_id,
            "source-layer": "grid",
            "type": "circle",
            "layout": {
                "circle-sort-key": ["get", "coef"],
            },
        })
    };

    let mut fields = json!(
        {
            "sequences": ["id", "account_id", "model", "type", "date", "gps_accuracy", "h_pixel_density"],
            "pictures": ["id", "account_id", "ts", "heading", "sequences", "type", "model", "gps_accuracy", "h_pixel_density"],
        }

    );
    if !only_for_one_user {
        fields
            .as_object_mut()
            .expect("fields should be an object")
            .insert(
                "grid".to_string(),
                json!([
                    "id",
                    "nb_pictures",
                    "nb_360_pictures",
                    "nb_flat_pictures",
                    "coef",
                    "coef_360_pictures",
                    "coef_flat_pictures",
                ]),
            );
    }
    json!({
        "layers": [
            {
            "id": format!("{source_id}_sequences"),
            "layout": {
                "line-cap": "square"
            },
            "paint": {
                "line-color": "#FF6F00",
                "line-width": ["interpolate", ["linear"],["zoom"], 0, 0.5, 10, 2, 14, 4, 16, 5, 22, 3],
                "line-opacity": line_opacity,
            },
            "source": source_id,
            "source-layer": "sequences",
            "type": "line"
            },
            {
            "id": format!("{source_id}_pictures"),
            "paint": {
                "circle-color": "#FF6F00",
                "circle-opacity": ["interpolate", ["linear"], ["zoom"], 15, 0, 16, 1],
                "circle-radius": ["interpolate", ["linear"], ["zoom"], 15, 4.5, 17, 8, 22, 12],
                "circle-stroke-color": "#ffffff",
                "circle-stroke-width": ["interpolate", ["linear"], ["zoom"], 17, 0, 20, 2]
            },
            "source": source_id,
            "source-layer": "pictures",
            "type": "circle"
            },
            grid
        ],
        "name": "GeoVisio Vector Tiles",
        "sources": {
            source_id: {
            "maxzoom": 15,
            "minzoom": 0,
            "tiles": [
                tile_url
            ],
            "type": "vector"
            }
        },
        "metadata": {
            "panoramax:fields": fields,
        },
        "version": 8
    })
}

///Get vector tiles style.
///
/// This style file follows MapLibre Style Spec : https://maplibre.org/maplibre-style-spec/
#[api_operation(tag = "Map")]
pub async fn get_style(req: HttpRequest) -> HttpResponse {
    let tile_url = link::href_from_path(&req, "/api/map/{z}/{x}/{y}.mvt");
    let style = get_json_style(&tile_url, None);
    HttpResponse::Ok().json(style)
}

/// Get a MapBox vector tile containing the collections and the items in the tile's geometry for a given provider
#[api_operation(tag = "Map")]
pub async fn get_provider_tile(
    db: web::Data<PgPool>,
    r: web::Path<TileRequest>,
) -> Result<ApiTile, APIError> {
    let r = r.into_inner().validate()?;
    let query = tile_query(&r);

    let pg_tile = sqlx::query(&query)
        .bind(r.z as i32)
        .bind(r.x as i32)
        .bind(r.y as i32)
        .fetch_one(db.as_ref())
        .await?;
    Ok(ApiTile(pg_tile.get(0)))
}

///Get vector tiles style for a given provider.
///
/// This style file follows MapLibre Style Spec : https://maplibre.org/maplibre-style-spec/
#[api_operation(tag = "Map")]
pub async fn get_provider_style(
    req: HttpRequest,
    provider_id: web::Path<uuid::Uuid>,
) -> HttpResponse {
    let tile_url = link::href_from_path(
        &req,
        &format!(
            "/api/users/{userId}/map/{{z}}/{{x}}/{{y}}.mvt",
            userId = provider_id
        ),
    );
    let style = get_json_style(&tile_url, Some(*provider_id));
    HttpResponse::Ok().json(style)
}
