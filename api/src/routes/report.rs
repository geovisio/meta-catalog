use crate::model::APIError;
use actix_web::{web, HttpResponse};
use apistos::{api_operation, ApiComponent, JsonSchema};
use awc::Client;
use serde::{Deserialize, Serialize};
use serde_json::Value;
use sqlx::PgPool;
use uuid::Uuid;

#[derive(Debug, Deserialize, Serialize, JsonSchema, ApiComponent)]
pub struct Report {
    /// The ID of the picture (item) concerned by this report. You should either set picture_id or sequence_id.
    picture_id: Option<Uuid>,
    /// The ID of the sequence (collection) concerned by this report. You should either set picture_id or sequence_id. If no picture_id is set, report will concern the whole sequence.
    sequence_id: Option<Uuid>,

    /// Nature of the issue you want to report. Check the instance documentation for more information.
    issue: String,
    /// The reporter email, optional but can be useful to get an answer or if precisions are necessary.
    reported_email: Option<String>,
    /// Optional details about the issue.
    reporter_comments: Option<String>,

    /// Other report parameters that will be forwarded to the picture's instance
    #[serde(flatten)]
    other_params: Option<Value>,
}

#[derive(sqlx::FromRow)]
struct Instance {
    url: String,
}

/// Forward a report to the instance
///
/// Check the instance documentation for more information about the response.
#[api_operation(tag = "Report")]
pub async fn post_report(
    db: web::Data<PgPool>,
    report: web::Json<Report>,
    client: web::Data<Client>,
) -> Result<HttpResponse, APIError> {
    let instance = get_instance(db.as_ref(), &report).await?;
    let url = format!("{}/reports", instance.url);

    let r = client
        .as_ref()
        .post(url)
        .send_json(&report)
        .await
        .map_err(|e| {
            tracing::warn!(
                "Impossible to send report to instance {:?}: {e}",
                &instance.url
            );

            APIError::ImpossibleToQueryInstance { url: instance.url }
        })?;

    // stream the instance response to the client
    let mut response = HttpResponse::build(r.status());

    for header in r
        .headers()
        .iter()
        // remove content encoding header since it will be decoded
        .filter(|(name, _)| *name != "content-encoding")
    {
        response.append_header(header);
    }

    Ok(response.streaming(r))
}

async fn get_instance(db: &PgPool, report: &Report) -> Result<Instance, APIError> {
    let (id, obj_type) = if let Some(pic) = report.picture_id {
        (pic, stac::Type::Item.as_str())
    } else if let Some(seq) = report.sequence_id {
        (seq, stac::Type::Collection.as_str())
    } else {
        return Err(APIError::InvalidParameter(
            "Impossible to send report, either a picture_id or a sequence_id should be set"
                .to_string(),
        ));
    };

    if let Some(pic) = report.picture_id {
        sqlx::query_as(
            r#"SELECT url FROM instances i 
JOIN collections c ON c.instance_id = i.id 
JOIN items ON items.collection_id = c.id 
WHERE items.id = $1"#,
        )
        .bind(pic)
    } else if let Some(seq) = report.sequence_id {
        sqlx::query_as(
            r#"SELECT url FROM instances i 
JOIN collections c ON c.instance_id = i.id 
WHERE c.id = $1"#,
        )
        .bind(seq)
    } else {
        unreachable!()
    }
    .fetch_optional(db)
    .await?
    .ok_or(APIError::NotFound {
        id: id.to_string(),
        obj_type: &obj_type,
    })
}
