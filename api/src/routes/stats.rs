use std::collections::{BTreeMap, HashMap};

use actix_web::web;
use apistos::{api_operation, ApiComponent, JsonSchema};
use num_traits::cast::ToPrimitive;
use serde::Serialize;
use sqlx::{types::Decimal, PgPool};

use crate::model::APIError;

/// Broken down statistics by instances
#[derive(sqlx::FromRow)]
struct MetricsByInstanceSQL {
    #[sqlx(flatten)]
    pub metrics: GenericMetricsSQL,
    pub instance_name: String,
}

/// Broken down statistics by picture upload month and instance
#[derive(sqlx::FromRow)]
struct MetricsByUploadMonthAndInstanceSQL {
    #[sqlx(flatten)]
    pub metrics: GenericMetricsSQL,
    pub nb_active_contributors: i64,
    pub upload_month: String,
    pub instance_name: String,
}

/// SQL row of metrics
#[derive(sqlx::FromRow, Default)]
struct GenericMetricsSQL {
    /// Number of pictures in the instance
    pub nb_pictures: Decimal,
    /// Original size of the picture when uploaded (does not take the derivated pictures into account (the blurred pictures, the small def pictures, the tiles pictures, ...))
    pub pictures_original_size: Decimal,

    /// Number of unique contributors
    pub nb_contributors: i64,
    /// Approximate the coverage of the pictures. Since the pictures are 'points' on a coordinate, we use an heuristics to compute a coverage
    /// When consider that each picture covers 20m (seems reasonable), and each pictures are rounded so if there is several pictures less than 20m appart we only count 1 picture
    /// This deduplication makes it possible not to increase this metrics if we have lots of pictures of the same road at different time
    pub approximated_coverage_km: i64,
    /// Length of the collections geometry
    pub collections_length_km: i64,
}

/// Generic statistics about the data harvested
#[derive(Serialize, Default, JsonSchema, ApiComponent)]
pub struct GenericMetrics {
    /// Number of pictures
    pub nb_pictures: u64,
    /// Original size of the picture when uploaded (does not take the derivated pictures into account (the blurred pictures, the small def pictures, the tiles pictures, ...))
    pub pictures_original_size: u64,

    /// Number of unique contributors
    pub nb_contributors: u64,
    /// Approximate the coverage of the pictures. Since the pictures are 'points' on a coordinate, we use an heuristics to compute a coverage
    /// We consider that each picture covers 20m (seems reasonable), and each pictures are rounded so if there is several pictures less than 20m appart we only count 1 picture
    /// This deduplication makes it possible not to increase this metrics if we have lots of pictures of the same road at different time
    ///
    /// Note that this field is deprecated because the computation is wrong, and we'll need time to compute it correctly.
    /// Use `collections_length_km` instead.
    #[deprecated]
    pub approximated_coverage_km: u64,

    /// Total length of the collections in km
    pub collections_length_km: u64,
}

impl From<GenericMetricsSQL> for GenericMetrics {
    fn from(sql: GenericMetricsSQL) -> Self {
        #[allow(deprecated)]
        Self {
            nb_pictures: sql.nb_pictures.to_u64().unwrap_or(0),
            pictures_original_size: sql.pictures_original_size.to_u64().unwrap_or(0),
            approximated_coverage_km: sql.approximated_coverage_km as u64,
            nb_contributors: sql.nb_contributors as u64,
            collections_length_km: sql.collections_length_km as u64,
        }
    }
}

/// Broken down statistics by picture upload month and instance
#[derive(Serialize, Default, JsonSchema, ApiComponent)]
pub struct MetricsByUploadMonthAndInstance {
    #[serde(flatten)]
    pub metrics: GenericMetrics,
    pub nb_active_contributors: u64,
}
impl From<MetricsByUploadMonthAndInstanceSQL> for MetricsByUploadMonthAndInstance {
    fn from(sql: MetricsByUploadMonthAndInstanceSQL) -> Self {
        Self {
            metrics: sql.metrics.into(),
            nb_active_contributors: sql.nb_active_contributors as u64,
        }
    }
}

/// Generic statistics about the data harvested
#[derive(Serialize, Default, JsonSchema, ApiComponent)]
pub struct Metrics {
    /// Number of pictures in the instance
    pub generic_stats: GenericMetrics,
    /// Some disaggregated statistics by instances
    pub stats_by_instance: HashMap<String, GenericMetrics>,
    /// Some disaggregated statistics by picture upload month and instance
    pub stats_by_upload_month: BTreeMap<String, HashMap<String, MetricsByUploadMonthAndInstance>>,
}

/// Get stats
#[api_operation(tag = "Stats")]
pub async fn stats(db: web::Data<PgPool>) -> Result<web::Json<Metrics>, APIError> {
    let generic_stats = generic_stats(db.as_ref()).await?;

    let stats_by_instance = stats_by_instance(db.as_ref()).await?;
    let stats_by_upload_month = stats_by_upload_month(db.as_ref()).await?;

    Ok(web::Json(Metrics {
        generic_stats,
        stats_by_instance,
        stats_by_upload_month,
    }))
}

async fn generic_stats(db: &PgPool) -> Result<GenericMetrics, APIError> {
    let generic_stats = sqlx::query_as::<_, GenericMetricsSQL>(
        r#"WITH
i AS (
    SELECT
        COALESCE(SUM(nb_items), 0) AS nb_pictures, 
        COALESCE(SUM(original_file_size), 0) AS pictures_original_size, 
        COALESCE(COUNT(DISTINCT(contributor, instance_id)), 0) AS nb_contributors, 
        COALESCE(SUM(approximated_coverage_km), 0)::INT8 AS approximated_coverage_km
    FROM stats.items_by_instance_creation_month_contributor
)
, c AS (
    SELECT
        COALESCE(SUM(collections_length_km), 0)::INT8 AS collections_length_km
    FROM stats.collections_by_instance_creation_month_contributor s
)
SELECT
    i.nb_pictures AS nb_pictures, 
    i.pictures_original_size AS pictures_original_size, 
    i.nb_contributors AS nb_contributors, 
    i.approximated_coverage_km::INT8 AS approximated_coverage_km,
    c.collections_length_km::INT8 AS collections_length_km
FROM i, c
"#,
    )
    .fetch_optional(db)
    .await?
    .unwrap_or_else(|| {
        log::info!("No data loaded, no statistics can be computed");
        GenericMetricsSQL::default()
    });

    Ok(generic_stats.into())
}

async fn stats_by_instance(db: &PgPool) -> Result<HashMap<String, GenericMetrics>, APIError> {
    let items_stats_by_instance = sqlx::query_as::<_, MetricsByInstanceSQL>(
        r#"WITH
i as (
    SELECT
        COALESCE(SUM(nb_items), 0) AS nb_pictures,
        COALESCE(SUM(original_file_size), 0) AS pictures_original_size,
        COALESCE(COUNT(DISTINCT(contributor, instance_id)), 0) AS nb_contributors,
        COALESCE(SUM(approximated_coverage_km), 0)::INT8 AS approximated_coverage_km,
        instance_id
    FROM stats.items_by_instance_creation_month_contributor s
    GROUP BY instance_id
)
, c AS (
    SELECT
        COALESCE(SUM(collections_length_km), 0)::INT8 AS collections_length_km,
        instance_id
    FROM stats.collections_by_instance_creation_month_contributor s
    GROUP BY instance_id
)
SELECT
    i.nb_pictures AS nb_pictures, 
    i.pictures_original_size AS pictures_original_size, 
    i.nb_contributors AS nb_contributors, 
    i.approximated_coverage_km::INT8 AS approximated_coverage_km,
    c.collections_length_km::INT8 AS collections_length_km,
    instances.name AS instance_name
FROM i
JOIN c ON i.instance_id = c.instance_id
JOIN instances  ON i.instance_id = instances.id
"#,
    )
    .fetch_all(db)
    .await?;

    Ok(items_stats_by_instance
        .into_iter()
        .map(|s| (s.instance_name.clone(), s.metrics.into()))
        .collect())
}

async fn stats_by_upload_month(
    db: &PgPool,
) -> Result<BTreeMap<String, HashMap<String, MetricsByUploadMonthAndInstance>>, APIError> {
    let stats_by_upload_month_sql = sqlx::query_as::<_, MetricsByUploadMonthAndInstanceSQL>(
    r#"WITH 
item_created_month_stats AS (
    SELECT
        COALESCE(SUM(nb_items), 0) AS nb_pictures,
        COALESCE(COUNT(DISTINCT(contributor, instance_id)), 0) AS nb_contributors,
        COALESCE(SUM(original_file_size), 0) AS pictures_original_size,
        COALESCE(SUM(approximated_coverage_km), 0)::INT8 AS approximated_coverage_km,
        created_month,
        instance_id
    FROM stats.items_by_instance_creation_month_contributor s
    GROUP BY instance_id, created_month
)
, items_aggregated_month_stats AS (
    SELECT
        SUM(nb_pictures) OVER (PARTITION BY instance_id ORDER BY created_month) AS nb_pictures,
        SUM(pictures_original_size) OVER (PARTITION BY instance_id ORDER BY created_month) AS pictures_original_size,
        SUM(approximated_coverage_km) OVER (PARTITION BY instance_id ORDER BY created_month) AS approximated_coverage_km,
        nb_contributors AS nb_active_contributors,
        0::INT8 AS nb_contributors, --TODO
        created_month,
        instance_id
    FROM item_created_month_stats
)
, collections_created_month_stats AS (
    SELECT
        COALESCE(SUM(collections_length_km), 0) AS collections_length_km,
        created_month,
        instance_id
    FROM stats.collections_by_instance_creation_month_contributor s
    GROUP BY instance_id, created_month
)
, collections_aggregated_month_stats AS (
    SELECT 
        SUM(collections_length_km) OVER (PARTITION BY instance_id ORDER BY created_month) AS collections_length_km,
        created_month,
        instance_id
    FROM collections_created_month_stats
)
SELECT 
    i.nb_pictures AS nb_pictures, 
    i.pictures_original_size AS pictures_original_size, 
    i.nb_contributors AS nb_contributors, 
    i.nb_active_contributors AS nb_active_contributors, 
    i.approximated_coverage_km::INT8 AS approximated_coverage_km,
    c.collections_length_km::INT8 AS collections_length_km,
    instances.name AS instance_name,
    i.created_month AS upload_month
FROM items_aggregated_month_stats i
JOIN collections_aggregated_month_stats c ON c.instance_id = i.instance_id AND c.created_month = i.created_month
JOIN instances on instances.id = i.instance_id
;
"#,
)
.fetch_all(db)
.await?;

    let mut stats_by_upload_month: BTreeMap<
        String,
        HashMap<String, MetricsByUploadMonthAndInstance>,
    > = BTreeMap::default();
    for s in stats_by_upload_month_sql.into_iter() {
        stats_by_upload_month
            .entry(s.upload_month.clone())
            .or_default()
            .insert(s.instance_name.clone(), s.into());
    }
    Ok(stats_by_upload_month)
}

#[derive(Serialize, ApiComponent, JsonSchema)]
pub struct ContributionStats {
    /// Number of pictures contributed
    pub nb_items: i64,
    /// Approximate the number of kilometers done contributing. Computed based on the sequences geometries.
    pub km_captured: i64,
}

/// Statistics aggregate
#[derive(Serialize, ApiComponent, JsonSchema, Eq, PartialEq, PartialOrd, Ord)]
pub enum ContributionAggregate {
    /// Contributions over the last 7 days
    #[serde(rename = "last_7_days")]
    Last7Days,
    /// Contributions over the last 30 days
    #[serde(rename = "last_30_days")]
    Last30Days,
}

#[derive(Serialize, ApiComponent, JsonSchema)]
pub struct ContributorStat {
    /// Name of the contributor
    pub name: String,
    /// Name of the instance
    pub instance_name: String,
    /// Id of the contributor
    pub id: uuid::Uuid,
    #[serde(flatten)]
    pub contribution_stats: ContributionStats,
    /// History of the contribution
    pub history: BTreeMap<ContributionAggregate, ContributionStats>,
}

#[derive(sqlx::FromRow)]
pub struct ContributorStatSQL {
    pub km_captured: Option<i64>,
    pub nb_items: Option<i64>,
    pub name: String,
    pub id: uuid::Uuid,
    pub instance_name: String,
    pub last_7_days_km_captured: Option<i64>,
    pub last_7_days_nb_items: Option<i64>,
    pub last_30_days_km_captured: Option<i64>,
    pub last_30_days_nb_items: Option<i64>,
}

impl From<ContributorStatSQL> for ContributorStat {
    fn from(sql: ContributorStatSQL) -> Self {
        Self {
            name: sql.name,
            instance_name: sql.instance_name,
            id: sql.id,
            contribution_stats: ContributionStats {
                nb_items: sql.nb_items.unwrap_or_default(),
                km_captured: sql.km_captured.unwrap_or_default(),
            },
            history: vec![
                (
                    ContributionAggregate::Last7Days,
                    ContributionStats {
                        nb_items: sql.last_7_days_nb_items.unwrap_or_default(),
                        km_captured: sql.last_7_days_km_captured.unwrap_or_default(),
                    },
                ),
                (
                    ContributionAggregate::Last30Days,
                    ContributionStats {
                        nb_items: sql.last_30_days_nb_items.unwrap_or_default(),
                        km_captured: sql.last_30_days_km_captured.unwrap_or_default(),
                    },
                ),
            ]
            .into_iter()
            .collect(),
        }
    }
}

#[derive(Serialize, ApiComponent, JsonSchema)]
pub struct StatsByContributor {
    pub stats_by_contributor: Vec<ContributorStat>,
}
/// Get stats
#[api_operation(tag = "Stats")]
pub async fn stats_by_contributor(
    db: web::Data<PgPool>,
) -> Result<web::Json<StatsByContributor>, APIError> {
    let stats_by_contributor = sqlx::query_as::<_, ContributorStatSQL>(
        r#"WITH
global_stats AS (
    SELECT provider_id, SUM(km_captured) AS km_captured, SUM(nb_items) AS nb_items FROM stats.contribution_by_day
    GROUP BY provider_id
)
, last_7_days AS (
    SELECT provider_id, SUM(km_captured) AS km_captured, SUM(nb_items) AS nb_items FROM stats.contribution_by_day
    WHERE created_day >= (NOW() - '7 days'::interval)
    GROUP BY provider_id
)
, last_30_days AS (
    SELECT provider_id, SUM(km_captured) AS km_captured, SUM(nb_items) AS nb_items FROM stats.contribution_by_day
    WHERE created_day >= (NOW() - '30 days'::interval)
    GROUP BY provider_id
)
SELECT 
    p.name,
    p.id,
    i.name AS instance_name,
    global_stats.nb_items::INT8,
    global_stats.km_captured::INT8,
    last_7_days.nb_items::INT8 AS last_7_days_nb_items,
    last_7_days.km_captured::INT8 AS last_7_days_km_captured,
    last_30_days.nb_items::INT8 AS last_30_days_nb_items,
    last_30_days.km_captured::INT8 AS last_30_days_km_captured
FROM global_stats
JOIN providers p ON global_stats.provider_id = p.id
JOIN instances i ON p.instance_id = i.id
LEFT JOIN last_7_days ON global_stats.provider_id = last_7_days.provider_id
LEFT JOIN last_30_days ON global_stats.provider_id = last_30_days.provider_id
ORDER BY global_stats.nb_items DESC;"#)
    .fetch_all(db.as_ref())
    .await?;

    Ok(web::Json(StatsByContributor {
        stats_by_contributor: stats_by_contributor.into_iter().map(|s| s.into()).collect(),
    }))
}
