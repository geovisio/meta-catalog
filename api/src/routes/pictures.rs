use crate::{model::APIError, utils::params::PicturePermanentRedirect};
use actix_web::{web, Result};
use apistos::api_operation;
use sqlx::postgres::PgPool;

fn validate_asset(asset: &str) -> Result<&str, APIError> {
    match asset {
        "hd" | "sd" | "thumb" => Ok(asset),
        _ => Err(APIError::InvalidParameter(format!(
            "{} is not a valid asset, should be either hd, sd or thumb",
            asset
        ))),
    }
}

#[derive(sqlx::FromRow)]
pub struct DBPic {
    pub href: String,
}

/// Redirect to the picture file with its ID
#[api_operation(tag = "Items")]
pub async fn get_picture_asset(
    db: web::Data<PgPool>,
    qs: web::Path<(uuid::Uuid, String)>,
) -> Result<PicturePermanentRedirect, APIError> {
    let (id, asset) = qs.as_ref();
    validate_asset(asset)?;

    let db_pic = sqlx::query_as::<_, DBPic>(&format!(
        "SELECT content->'assets'->'{asset}'->>'href' AS href FROM items WHERE id = $1"
    ))
    .bind(id)
    .fetch_optional(db.as_ref())
    .await?
    .ok_or(APIError::NotFound {
        id: id.to_string(),
        obj_type: stac::Type::Item.as_str(),
    })?;

    Ok(PicturePermanentRedirect(db_pic.href))
}
