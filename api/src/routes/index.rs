use actix_files::NamedFile;
use actix_web::web;
use actix_web::web::Redirect;
use actix_web::HttpResponse;
use apistos::api_operation;

use crate::utils::matomo::{matomo_html, MatomoConfig};

const VIEWER: &str = r#"
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />

	<link rel="shortcut icon" href="/favicon.ico" />
	<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/@panoramax/web-viewer@{geovisio_version}/build/index.css" />
	
	<title>All Panoramax street level imagery</title>
	<meta name="twitter:title" content="All Panoramax street level imagery" />
	<meta name="og:title" content="All Panoramax street level imagery" />

	<meta name="twitter:description"
		content="Explore and reuse street level imagery from all public Panoramax instances."
	/>
	<meta name="og:description"
		content="Explore and reuse street level imagery from all public Panoramax instances."
	/>
	<meta name="description"
		content="Explore and reuse street level imagery from all public Panoramax instances."
	/>

	<link href="https://mapstodon.space/@panoramax" rel="me"/>
	<meta name="og:image" content="/meta-img.jpg"/>
	<meta name="twitter:image" content="/meta-img.jpg"/>

	<style>
		#viewer {
			position: absolute;
			top: 0;
			bottom: 0;
			left: 0;
			right: 0;
		}
	</style>
</head>
<body>
	<div id="viewer">
		<noscript>You need to enable JavaScript to run this app.</noscript>
	</div>

	<script src="https://cdn.jsdelivr.net/npm/@panoramax/web-viewer@{geovisio_version}/build/index.js"></script>
	{matomo}
	<script>
		var instance = new Panoramax.default(
			"viewer",
			"/api",
			{ map: { startWide: true } }
		);
	</script>
</body>
</html>
"#;

// Note: to update the geovisio-viewer version, just update this version number
const GEOVISIO_VERSION: &str = "3.2";

/// Landing page
///
/// Simple HTML page loading the geovisio viewer configured with the API
#[api_operation(tag = "Root")]
pub async fn index(config: web::Data<MatomoConfig>) -> HttpResponse {
    HttpResponse::Ok()
        .content_type("text/html; charset=utf-8")
        .append_header(("X-Hdr", "sample"))
        .body(
            VIEWER
                .replace("{matomo}", &matomo_html(&config))
                .replace("{geovisio_version}", GEOVISIO_VERSION),
        )
}

#[api_operation(skip)]
pub async fn favicon() -> actix_web::Result<NamedFile> {
    Ok(NamedFile::open("./static/favicon.ico")?)
}
#[api_operation(skip)]
pub async fn meta() -> actix_web::Result<NamedFile> {
    Ok(NamedFile::open("./static/meta-img.jpg")?)
}

#[api_operation(skip)]
pub async fn robots() -> actix_web::Result<NamedFile> {
    Ok(NamedFile::open("./static/robots.txt")?)
}

#[api_operation(skip)]
pub async fn swagger_doc() -> Redirect {
    // simple redirect to have the same route as the instance's documentation
    Redirect::to("/swagger").permanent()
}
