use crate::model::APIError;
use crate::utils::link;
use actix_web::{web, HttpRequest, Result};
use apistos::{api_operation, ApiComponent, JsonSchema};
use sqlx::postgres::PgPool;
use stac::Link;

#[derive(sqlx::FromRow)]
struct DBProvider {
    id: uuid::Uuid,
    name: String,
    instance_name: String,
}

#[derive(serde::Serialize, JsonSchema, ApiComponent)]
pub struct Provider {
    pub id: uuid::Uuid,
    pub name: String,
    pub instance_name: String,
    pub label: String,

    #[schemars(schema_with = "extent_links")]
    pub links: Vec<stac::Link>,
}

fn extent_links(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
    apistos::Schema::new_ref(
        "https://api.stacspec.org/v1.0.0/ogcapi-features/openapi.yaml#/components/schemas/links"
            .into(),
    )
}

impl From<(DBProvider, &HttpRequest)> for Provider {
    fn from((p, req): (DBProvider, &HttpRequest)) -> Self {
        Provider {
            label: format!("{}@{}", p.name, p.instance_name),
            id: p.id,
            name: p.name,
            instance_name: p.instance_name,
            links: vec![
                link::from_path(req, &format!("/api/users/{}", p.id), "user-info").json(),
                link::from_path(req, &format!("/api/users/{}/catalog", p.id), "child").json(),
            ],
        }
    }
}

#[derive(serde::Serialize, JsonSchema, ApiComponent)]
pub struct Providers {
    features: Vec<Provider>,
}

fn default_search_limit() -> i16 {
    10
}

#[derive(Debug, serde::Deserialize, JsonSchema, ApiComponent)]
pub struct SearchParameters {
    /// Limit the number of results, Defaults to 10
    #[serde(default = "default_search_limit")]
    limit: i16,

    /// Search for providers with those IDs
    q: String,
}

// Wrapper around a Stac catalog for better OpenApi
#[derive(serde::Serialize, JsonSchema, ApiComponent)]
pub struct Catalog {
    #[serde(flatten)]
    #[schemars(schema_with = "catalog_schema")]
    catalog: stac::Catalog,
}

fn catalog_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
    apistos::Schema::new_ref(
        "https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/catalog"
            .into(),
    )
}

impl Catalog {
    pub fn new(id: impl ToString, description: impl ToString, links: Vec<Link>) -> Catalog {
        let mut c = Catalog {
            catalog: stac::Catalog::new(id, description),
        };
        c.catalog.links = links;
        c
    }
}

/// Search for some providers
#[api_operation(tag = "Providers")]
#[tracing::instrument]
pub async fn search_providers(
    db: web::Data<PgPool>,
    req: HttpRequest,
    params: web::Query<SearchParameters>,
) -> Result<web::Json<Providers>, APIError> {
    let query = r#"WITH ranked AS (
    SELECT p.name, p.id, i.name AS instance_name, similarity($1, p.name) AS similarity
    FROM providers p
    JOIN instances i on i.id = p.instance_id
)
SELECT name, id, instance_name from ranked 
WHERE similarity > 0.1
ORDER BY similarity DESC
LIMIT $2;"#;

    let providers: Vec<_> = sqlx::query_as(&query)
        .bind(params.q.clone())
        .bind(params.limit)
        .fetch_all(db.as_ref())
        .await?
        .into_iter()
        .map(|p: DBProvider| (p, &req).into())
        .collect();
    Ok(web::Json(Providers {
        features: providers,
    }))
}

/// Get a provider by its ID
#[api_operation(tag = "Providers")]
#[tracing::instrument]
pub async fn get_provider(
    db: web::Data<PgPool>,
    req: HttpRequest,
    id: web::Path<uuid::Uuid>,
) -> Result<web::Json<Provider>, APIError> {
    let query = r#"SELECT p.name, p.id, i.name AS instance_name
    FROM providers p
    JOIN instances i on i.id = p.instance_id
    WHERE p.id = $1"#;

    let provider = sqlx::query_as(&query)
        .bind(*id)
        .fetch_optional(db.as_ref())
        .await?
        .ok_or(APIError::NotFound {
            id: id.to_string(),
            obj_type: "Provider",
        })
        .map(|p: DBProvider| (p, &req).into())?;
    Ok(web::Json(provider))
}

/// Get all the providers as a STAC catalog
#[api_operation(tag = "Providers")]
#[tracing::instrument]
pub async fn get_providers_catalog(
    db: web::Data<PgPool>,
    req: HttpRequest,
) -> Result<web::Json<Catalog>, APIError> {
    let query = r#"SELECT p.name, p.id, i.name AS instance_name
    FROM providers p
    JOIN instances i on i.id = p.instance_id"#;

    let mut links: Vec<_> = sqlx::query_as(&query)
        .fetch_all(db.as_ref())
        .await?
        .into_iter()
        .map(|p: DBProvider| {
            link::from_path(&req, &format!("/api/users/{}/catalog", p.id), "child")
                .title(Some(format!(
                    "Collection for user {} on {}",
                    p.name, p.instance_name
                )))
                .json()
        })
        .collect();
    links.push(link::from_service(&req, "root", "root").json());
    links.push(link::from_service(&req, "get_providers_catalog", "self").json());

    let catalog = Catalog::new("users:catalog", "Users catalog", links);
    Ok(web::Json(catalog))
}
