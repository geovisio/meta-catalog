use crate::{model::APIError, utils::link};
use actix_web::{
    web::{Data, Json},
    HttpRequest, Result,
};
use apistos::{api_operation, ApiComponent, JsonSchema};
use chrono::{DateTime, Utc};
use schemars;
use serde::Serialize;
use sqlx::PgPool;
use stac::{Bbox, Catalog, Extent, SpatialExtent, TemporalExtent};
use stac_api::{Conformance, Root};

#[derive(Serialize, ApiComponent, JsonSchema)]
pub struct RootCatalog {
    #[serde(flatten)]
    #[schemars(schema_with = "root_schema")]
    root: Root,

    #[schemars(schema_with = "extent_schema")]
    extent: Extent,
}

fn root_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
    apistos::Schema::new_ref(
        "https://api.stacspec.org/v1.0.0/core/openapi.yaml#/components/schemas/landingPage".into(),
    )
}
fn extent_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
    apistos::Schema::new_ref(
        "https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/extent"
            .into(),
    )
}
#[derive(sqlx::FromRow)]
struct Query {
    st_xmin: Option<f64>,
    st_ymin: Option<f64>,
    st_xmax: Option<f64>,
    st_ymax: Option<f64>,
    min: Option<DateTime<Utc>>,
    max: Option<DateTime<Utc>>,
}

/// Root of Panoramax api
#[api_operation(tag = "Metadata")]
pub async fn root(db: Data<PgPool>, req: HttpRequest) -> Result<Json<RootCatalog>, APIError> {
    let query: Query = sqlx::query_as(
        r#"SELECT
    ST_XMin(ST_EstimatedExtent('items', 'geom')),
    ST_YMin(ST_EstimatedExtent('items', 'geom')),
    ST_XMax(ST_EstimatedExtent('items', 'geom')),
    ST_YMax(ST_EstimatedExtent('items', 'geom')),
    MIN(min_datetime),
    MAX(max_datetime)
    FROM instances
"#,
    )
    .fetch_one(db.as_ref())
    .await?;

    let stac_api_version = "1.0.0";
    let mut catalog = Catalog::new(
        "panoramax",
        "This catalog list all geolocated pictures available in this Panoramax instance",
    );
    catalog.links = vec![
        link::from_service(&req, "root", "root").json(),
        link::from_service(&req, "root", "self").json(),
        link::from_path(&req, "/openapi.json", "service-desc").json(),
        link::from_path(&req, "/redoc", "service-doc-redoc").r#type("text/html".to_string()),
        link::from_path(&req, "/api/docs/swagger", "service-doc").r#type("text/html".to_string()),
        link::from_service(&req, "get_collections", "data").json(),
        link::from_service(&req, "get_providers_catalog", "child").json(),
        link::from_service(&req, "search_providers", "user-search").json(),
        link::from_service(&req, "get_search_items", "search").geojson(),
        link::from_path(&req, "/api/map/{z}/{x}/{y}.mvt", "xyz")
            .title("Pictures and sequences vector tiles".to_string())
            .r#type("application/vnd.mapbox-vector-tile".to_string()),
        link::from_path(&req, "/api/map/style.json", "xyz-style")
            .title("MapLibre Style JSON".to_string())
            .json(),
        link::from_path(&req, "/api/users/{userId}/map/{z}/{x}/{y}.mvt", "user-xyz")
            .title("Pictures and sequences vector tiles for a given user".to_string())
            .r#type("application/vnd.mapbox-vector-tile".to_string()),
        link::from_path(&req, "/api/users/{userId}/map/style.json", "user-xyz-style")
            .title("MapLibre Style JSON for a given user".to_string())
            .json(),
        link::from_path(
            &req,
            "/api/collections/{id}/thumb.jpg",
            "collection-preview",
        )
        .title("Thumbnail URL for a given sequence".to_string())
        .r#type("image/jpeg".to_string()),
        link::from_path(&req, "/api/pictures/{id}/thumb.jpg", "item-preview")
            .title("Thumbnail URL for a given picture".to_string())
            .r#type("image/jpeg".to_string()),
        link::from_path(&req, "/api/reports", "report")
            .title("Post feedback/report about picture or sequence".to_string())
            .json(),
        link::from_path(&req, "/api/instances", "instances")
            .title("Get the list of instances".to_string())
            .json(),
    ];

    // Build the root (landing page) endpoint.
    let root = RootCatalog {
        root: Root {
            catalog,
            conformance: Conformance {
                // TODO : should the conformance be computed from the harvested geovisio instance?
                conforms_to: vec![
                    stac_api::CORE_URI.to_string(),
                    "http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/core".to_string(),
                    "http://www.opengis.net/spec/ogcapi-features-1/1.0/conf/geojson".to_string(),
                    format!("https://api.stacspec.org/v{stac_api_version}/browseable"),
                    format!("https://api.stacspec.org/v{stac_api_version}/collections"),
                    format!("https://api.stacspec.org/v{stac_api_version}/ogcapi-features"),
                    format!("https://api.stacspec.org/v{stac_api_version}/item-search"),
                ],
            },
        },
        extent: stac::Extent {
            spatial: SpatialExtent {
                bbox: vec![Bbox::TwoDimensional([
                    query.st_xmin.unwrap_or(-180.0),
                    query.st_ymin.unwrap_or(-90.0),
                    query.st_xmax.unwrap_or(180.0),
                    query.st_ymax.unwrap_or(90.0),
                ])],
            },
            temporal: TemporalExtent {
                interval: vec![[query.min, query.max]],
            },
            ..Default::default()
        },
    };

    Ok(Json(root))
}
