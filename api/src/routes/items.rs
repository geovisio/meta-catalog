use std::borrow::Cow;
use std::vec;

use crate::model::APIError;
use crate::utils::link;
use crate::utils::params::{Bounds, CollectionItemsFilter, Coord, ItemField, ItemFilter, Range};
use crate::utils::serde_utils::deserialize_as_stac_opt_list;
use actix_web::http::Uri;
use actix_web::web::Data;
use actix_web::{web, HttpRequest, Result};
use apistos::{api_operation, ApiComponent, JsonSchema};
use chrono::{DateTime, Utc};
use serde::Serialize;
use serde_json::{json, Value};
use sqlx::postgres::{PgPool, Postgres};
use sqlx::QueryBuilder;

#[derive(serde::Serialize, serde::Deserialize, ApiComponent)]
pub struct Items {
    pub features: Vec<Value>,
    pub links: Vec<stac::Link>,
}

impl JsonSchema for Items {
    fn schema_name() -> Cow<'static, str> {
        "Items".into()
    }
    fn json_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
        apistos::Schema::new_ref(
            "https://api.stacspec.org/v1.0.0/item-search/openapi.yaml#/components/schemas/itemCollection"
                .into(),
        )
    }
}

#[derive(sqlx::FromRow)]
struct CollectionItem {
    content: Value,
    datetime: DateTime<Utc>,
}
impl CollectionItem {
    fn update_links(&mut self, req: &HttpRequest) {
        update_links(&mut self.content, req);
    }
}
#[derive(sqlx::FromRow)]
struct DbItem {
    content: Value,
}

fn update_links(value: &mut Value, req: &HttpRequest) {
    // replace the host in the links by the one of the request
    // at one point, all links should be computed by the api and not stored in the database
    // this will improve cross instance links handling
    value.as_object_mut().map(|o| {
        o["links"].as_array_mut().map(|links| {
            for l in links.iter_mut() {
                l.as_object_mut().map(|o| {
                    let href = o["href"]
                        .as_str()
                        .expect("all links should have a string href");
                    o["href"] = json!(href
                        .parse::<Uri>()
                        .ok()
                        .and_then(|uri| {
                            // change the scheme and host to match the request
                            let mut new_url = Uri::builder()
                                .scheme(req.connection_info().scheme())
                                .authority(req.connection_info().host());
                            if let Some(path) = uri.path_and_query() {
                                new_url = new_url.path_and_query(path.clone());
                            }

                            new_url.build().ok().map(|u| u.to_string())
                        })
                        .unwrap_or(href.to_string()));
                });
            }
        });
    });
}
impl DbItem {
    fn update_links(&mut self, req: &HttpRequest) {
        update_links(&mut self.content, req);
    }
}

fn default_items_limit() -> u16 {
    100
}

#[derive(serde::Deserialize, ApiComponent, JsonSchema, Debug)]
pub struct CollectionItemsQuery {
    /// Limit the number of collections
    #[serde(default = "default_items_limit")]
    #[schemars(range(min = 1, max = 1000))]
    pub limit: u16,

    #[serde(rename = "page")]
    pub pagination_filter: Option<CollectionItemsFilter>,
}
impl CollectionItemsQuery {
    pub fn validate(self) -> Result<Self, APIError> {
        if self.limit > 1000 {
            return Err(APIError::InvalidParameter(
                "The limit cannot exceed 1000".to_string(),
            ));
        };
        Ok(self)
    }
}

/// List all items of a collection
#[api_operation(tag = "Items")]
#[tracing::instrument]
pub async fn get_collection_items(
    req: HttpRequest,
    db: web::Data<PgPool>,
    id: web::Path<uuid::Uuid>,
    params: web::Query<CollectionItemsQuery>,
) -> Result<web::Json<Items>, APIError> {
    let params = params.0.validate()?;

    let mut query = format!(
        "SELECT content, datetime FROM items WHERE collection_id = '{}'",
        id
    );

    if let Some(pagination_filter) = &params.pagination_filter {
        query.push_str(&format!(" AND {f}", f = pagination_filter.as_sql()));
    }

    query.push_str(&format!(" ORDER BY datetime LIMIT {p}", p = params.limit));
    // log::info!("collection sql query = {}", query);

    // compute bounds for pagination
    let items_bounds: Bounds<DateTime<Utc>> =
        if let Some(d) = get_collection_items_bounds(db.as_ref(), &id).await? {
            d
        } else {
            // if no bounds found, it means there is not items in this collection, it's not an error, we can return an empty result
            return Ok(web::Json(Items {
                features: vec![],
                links: vec![],
            }));
        };

    let mut result_bounds = Bounds {
        min: DateTime::<Utc>::MAX_UTC,
        max: DateTime::<Utc>::MIN_UTC,
    };

    let items: Vec<_> = sqlx::query_as(&query)
        .fetch_all(db.as_ref())
        .await?
        .into_iter()
        .map(|mut i: CollectionItem| {
            result_bounds.update(i.datetime);
            i.update_links(&req);

            i.content
        })
        .collect();

    let mut links = vec![
        link::from_service(&req, "root", "root").json(),
        link::from_service(&req, "get_collections", "parent").json(),
        link::from_path(&req, &format!("/api/collections/{id}"), "self").json(),
    ];
    links.extend(compute_pagination_links(
        &req,
        result_bounds,
        items_bounds,
        id.as_ref(),
        params.limit,
    ));

    Ok(web::Json(Items {
        features: items,
        links,
    }))
}

fn compute_pagination_links(
    req: &HttpRequest,
    result_bounds: Bounds<DateTime<Utc>>,
    dataset_bounds: Bounds<DateTime<Utc>>,
    collection_id: &uuid::Uuid,
    limit: u16,
) -> Vec<stac::Link> {
    // Note: handle only `next` links for the moment
    let mut links = vec![];

    let api_filter = |cmp, dt_bound: DateTime<Utc>| {
        urlencoding::encode(&format!(
            "{field}{cmp}'{bound}'",
            field = ItemField::CaptureTime.as_api_parameter(),
            bound = dt_bound.to_rfc3339()
        ))
        .into_owned()
    };

    if dataset_bounds.max > result_bounds.max {
        // There are results after, we need a next link
        let next_filter = api_filter(">", result_bounds.max);
        links.push(
            link::from_path(
                req,
                &format!("/api/collections/{collection_id}/items?page={next_filter}&limit={limit}",),
                "next",
            )
            .json(),
        );
    }
    links
}

async fn get_collection_items_bounds(
    db: &PgPool,
    collection_id: &uuid::Uuid,
) -> Result<Option<Bounds<DateTime<Utc>>>, APIError> {
    let r: Option<Bounds<Option<DateTime<Utc>>>> = sqlx::query_as(
        "SELECT min(datetime) as min, max(datetime) as max FROM items WHERE collection_id = $1",
    )
    .bind(collection_id)
    .fetch_optional(db)
    .await?;

    Ok(r.and_then(|b| match (b.min, b.max) {
        (Some(min), Some(max)) => Some(Bounds { min, max }),
        _ => None,
    }))
}

#[derive(Serialize, JsonSchema, ApiComponent)]
#[serde(transparent)]
pub struct Item(#[schemars(schema_with = "item_schema")] Value);

fn item_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
    apistos::Schema::new_ref(
        "https://api.stacspec.org/v1.0.0/ogcapi-features/openapi.yaml#/components/responses/Feature"
            .into(),
    )
}

#[derive(serde::Serialize, serde::Deserialize, JsonSchema, ApiComponent)]
pub struct GetItem {
    /// Id of the collection
    pub collection_id: uuid::Uuid,
    /// Id of the item
    pub id: uuid::Uuid,
}

/// Get a specific item of a collection
#[api_operation(tag = "Items")]
pub async fn get_collection_item(
    db: Data<PgPool>,
    params: web::Path<GetItem>,
    req: HttpRequest,
) -> Result<web::Json<Item>, APIError> {
    let mut item = sqlx::query_as::<_, DbItem>(
        "SELECT content FROM items WHERE id = $1 AND collection_id = $2",
    )
    .bind(params.id)
    .bind(params.collection_id)
    .fetch_optional(db.as_ref())
    .await?
    .ok_or(APIError::NotFound {
        id: params.id.to_string(),
        obj_type: stac::Type::Item.as_str(),
    })?;
    item.update_links(&req);

    Ok(web::Json(Item(item.content)))
}

fn default_search_limit() -> i16 {
    10
}

fn default_place_distance() -> Range {
    Range { min: 3, max: 15 }
}

fn default_place_fov_tolerance() -> u64 {
    30
}

#[derive(Debug, serde::Deserialize, JsonSchema, ApiComponent)]
pub struct SearchParameters {
    /// Limit the number of results, Defaults to 10
    #[serde(default = "default_search_limit")]
    limit: i16,

    /// Search for items with those IDs
    #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
    ids: Option<Vec<uuid::Uuid>>,

    /// Search for items belonging to those collections
    #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
    collections: Option<Vec<uuid::Uuid>>,

    /// Search for items inside this bounding box
    #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
    pub bbox: Option<Vec<f64>>,

    /// Geographical coordinates (lat,lon) of a place you'd like to have pictures of.
    /// Returned pictures are either 360° or looking in direction of wanted place.
    pub place_position: Option<Coord>,

    /// Distance range (in meters) to search pictures for a particular place (place_position).
    /// Default range is 3-15. Only used if place_position parameter is defined.
    #[serde(default = "default_place_distance")]
    pub place_distance: Range,

    /// Tolerance on how much the place should be centered in nearby pictures:
    ///  * A lower value means place have to be at the very center of picture
    ///  * A higher value means place could be more in picture sides
    ///
    /// Value is expressed in degrees (from 2 to 180, defaults to 30°), and represents the acceptable field of view relative to picture heading. Only used if place_position parameter is defined.
    ///
    /// Example values are:
    ///
    ///  * <= 30° for place to be in the very center of picture
    ///  * 60° for place to be in recognizable human field of view
    ///  * 180° for place to be anywhere in a wide-angle picture
    ///
    /// Note that this parameter is not taken in account for 360° pictures, as by definition a nearby place would be theorically always visible in it.
    #[serde(default = "default_place_fov_tolerance")]
    pub place_fov_tolerance: u64,

    /// CQL2 filter for the search
    /// Allowed properties are:
    /// * field_of_view: field of view in degree.
    ///
    /// Usage doc can be found here: https://docs.geoserver.org/2.23.x/en/user/tutorials/cql/cql_tutorial.html
    ///
    /// Examples:
    ///
    /// * field_of_view = 360
    pub filter: Option<ItemFilter>,
    // TODO: datetime, intersect
}

/// Search for some items
#[api_operation(tag = "Items")]
#[tracing::instrument]
pub async fn get_search_items(
    db: Data<PgPool>,
    params: web::Query<SearchParameters>,
    req: HttpRequest,
) -> Result<web::Json<Items>, APIError> {
    search(db, params.into_inner(), &req).await
}

/// Search for some items by POSTing a json
#[api_operation(tag = "Items")]
#[tracing::instrument]
pub async fn post_search_items(
    db: Data<PgPool>,
    params: web::Json<SearchParameters>,
    req: HttpRequest,
) -> Result<web::Json<Items>, APIError> {
    search(db, params.into_inner(), &req).await
}

async fn search(
    db: Data<PgPool>,
    params: SearchParameters,
    req: &HttpRequest,
) -> Result<web::Json<Items>, APIError> {
    let mut query = QueryBuilder::<Postgres>::new("SELECT content FROM items WHERE TRUE");
    if let Some(col_id) = &params.collections {
        query
            .push(" AND collection_id = ANY(")
            .push_bind(col_id.as_slice())
            .push(")");
    }
    if let Some(ids) = &params.ids {
        query
            .push(" AND id = ANY(")
            .push_bind(ids.as_slice())
            .push(")");
    }
    if let Some(bbox) = &params.bbox {
        if bbox.len() != 4 {
            return Err(APIError::InvalidParameter(
                "Invalid bbox, the boundbing box should be an array with 4 values".to_string(),
            ));
        }

        query.push(format_args!(
            " AND geom && ST_MakeEnvelope({minx}, {miny}, {maxx}, {maxy}, 4326)",
            minx = bbox[0],
            miny = bbox[1],
            maxx = bbox[2],
            maxy = bbox[3],
        ));
    }
    if let Some(item_filter) = &params.filter {
        query.push(" AND ");
        item_filter.fill_sql_builder(&mut query).map_err(|e| {
            APIError::InvalidParameter(format!(
                "Impossible to convert the filter to valid sql: {e}"
            ))
        })?;
    }
    if let Some(place_position) = &params.place_position {
        let distance = &params.place_distance;
        let tolerance = params.place_fov_tolerance;

        if !(2..=180).contains(&tolerance) {
            return Err(APIError::InvalidParameter(
                "Parameter place_fov_tolerance must be either empty or a number between 2 and 180"
                    .to_string(),
            ));
        }

        // we want to search for pictures that contains a given position
        // Note: we use string formating instead since all arguments are numbers, there cannot be any SQL injections here
        // and the query is way easier to understand
        query.push(format_args!(
            r#" AND ST_Intersects(
                geom,
                ST_Difference(
                ST_Buffer(ST_Point({lon}, {lat})::geography, {max})::geometry,
                    ST_Buffer(ST_Point({lon}, {lat})::geography, {min})::geometry
                )
            )
            AND (
                content#>>'{{properties,pers:interior_orientation,field_of_view}}' = '360' 
                OR 
                ST_Azimuth(geom, ST_Point({lon}, {lat}, 4326)) 
                    BETWEEN 
                        radians((content#>>'{{properties,view:azimuth}}')::int - {tolerance})
                        AND radians((content#>>'{{properties,view:azimuth}}')::int + {tolerance})
            )
            "#,
            lon = place_position.lon,
            lat = place_position.lat,
            min = distance.min,
            max = distance.max,
            tolerance = (tolerance / 2) as i32,
        ));
    }

    // if we have a place, we want to order the result by proximity to it
    if let Some(place_position) = &params.place_position {
        query.push(format_args!(
            " ORDER BY geom <-> ST_Point({lon}, {lat}, 4326)",
            lon = place_position.lon,
            lat = place_position.lat,
        ));
    } else if let Some(bbox) = &params.bbox {
        query.push(format_args!(
            " ORDER BY geom <-> ST_Centroid(ST_MakeEnvelope({minx}, {miny}, {maxx}, {maxy}, 4326))",
            minx = bbox[0],
            miny = bbox[1],
            maxx = bbox[2],
            maxy = bbox[3],
        ));
    }

    query.push(" LIMIT ").push_bind(params.limit);

    // log::info!("sql = {:?}", &query.sql());

    let items: Vec<_> = query
        .build_query_as::<DbItem>()
        .fetch_all(db.as_ref())
        .await?
        .into_iter()
        .map(|mut i| {
            i.update_links(req);
            i.content
        })
        .collect();

    Ok(web::Json(Items {
        features: items,
        links: vec![],
    }))
}
