use crate::model::APIError;
use crate::utils::link::{self, from_path};
use crate::utils::params::{
    Bounds, CollectionField, CollectionFilter, CollectionSortBy, Comparison,
    PicturePermanentRedirect, SortByOrder,
};
use crate::utils::serde_utils::deserialize_as_stac_opt_list;
use actix_web::web::Data;
use actix_web::{web, HttpRequest, Result};
use apistos::{api_operation, ApiComponent, JsonSchema};
use chrono::{DateTime, NaiveDateTime, NaiveTime, Utc};
use serde::Serialize;
use serde_json::{json, Value};
use sqlx::postgres::PgPool;
use std::borrow::Cow;

fn default_limit() -> u16 {
    100
}

fn default_sort_by() -> CollectionSortBy {
    CollectionSortBy {
        field: CollectionField::Created,
        order: SortByOrder::Asc,
    }
}

#[derive(serde::Deserialize, Debug, ApiComponent, JsonSchema)]
pub struct CollectionsQuery {
    /// Limit the number of collections
    #[serde(default = "default_limit")]
    #[schemars(range(min = 1, max = 1000))]
    pub limit: u16,

    /// Limit to collections inside this bounding box
    #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
    pub bbox: Option<Vec<f64>>,

    /// CQL2 filter, only used for pagination filter. User's filters should be passed in the `filter` parameter
    #[serde(rename = "page")]
    pub pagination_filter: Option<CollectionFilter>,

    /// CQL2 Filter
    /// Note: the provider is named 'user' or 'account' in GeoVisio
    /// Filter should be like `{field} {comparator} '{value}'` (Note the `'` around the value)
    /// like `created > '2021-01-01T00:00:00Z'`
    ///
    /// Allowed fields are:
    ///  - `created`
    ///  - `updated`
    ///  - `provider_id`
    ///
    /// Only one field is allowed for the moment, no AND/OR combination yet
    pub filter: Option<CollectionFilter>,

    /// Define the sort order based on given property.
    /// Sort order is defined based on preceding '+' (asc) or '-' (desc).
    /// If there is no prefix, default is ascending order.
    ///
    /// Available properties are: "created" and "updated".
    ///
    /// This parameter MUST be urlencoded (%2B instead of '+')
    #[serde(default = "default_sort_by")]
    pub sortby: CollectionSortBy,
}

impl CollectionsQuery {
    pub fn validate(self) -> Result<Self, APIError> {
        if self.limit > 1000 {
            return Err(APIError::InvalidParameter(
                "The limit cannot exceed 1000".to_string(),
            ));
        };
        if let Some(bbox) = &self.bbox {
            if bbox.len() != 4 {
                return Err(APIError::InvalidParameter(
                    "Invalid bbox, the boundbing box should be an array with 4 values".to_string(),
                ));
            }
        }

        if let Some(p) = &self.pagination_filter {
            if self.sortby.field != p.field {
                // for pagination, the results MUST be sorted by the same field, else the filter is meaningless
                return Err(APIError::InvalidParameter(format!("When using `page` parameter, the results must be sorted by the same field as the page filter (`sortby={}`)", p.field.as_api_parameter())));
            }
        }
        Ok(self)
    }
}

#[derive(serde::Serialize, serde::Deserialize, ApiComponent)]
pub struct Collections {
    pub collections: Vec<Value>,
    pub links: Vec<stac::Link>,
}

impl JsonSchema for Collections {
    fn schema_name() -> Cow<'static, str> {
        "Collections".into()
    }
    fn json_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
        apistos::Schema::new_ref(
            "https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/collections"
                .into(),
        )
    }
}

#[derive(sqlx::FromRow)]
struct SqlCollection {
    content: Value,
}

impl SqlCollection {
    fn update_links(&mut self, req: &HttpRequest) {
        let id = self.content["id"]
            .as_str()
            .expect("all collection should have a string id")
            .to_string();
        self.content.as_object_mut().map(|o| {
            o.insert(
                "links".to_string(),
                json!([
                    link::from_service(req, "root", "root").json(),
                    link::from_service(req, "get_collections", "parent").json(),
                    link::from_path(req, &format!("/api/collections/{id}", id = id), "self",)
                        .json(),
                    link::from_path(
                        req,
                        &format!("/api/collections/{id}/items", id = id),
                        "items",
                    )
                    .json(),
                ]),
            )
        });
    }
}

struct CollectionsAndBounds {
    collections: Vec<Value>,
    bounds: Bounds<DateTime<Utc>>,
    dataset_bounds: Bounds<DateTime<Utc>>,
}

async fn get_collections_and_bounds(
    db: &PgPool,
    params: &CollectionsQuery,
    req: &HttpRequest,
) -> Result<CollectionsAndBounds, APIError> {
    // Note: we use string concatenate, so make sure all parameters are validated and cannot be used for sql injection
    let mut query = String::from("SELECT content FROM collections WHERE TRUE");

    if let Some(bbox) = &params.bbox {
        query.push_str(&format!(
            " AND computed_geom && ST_MakeEnvelope({}, {}, {}, {}, 4326)",
            bbox[0], bbox[1], bbox[2], bbox[3]
        ));
    }
    if let Some(pagination_filter) = &params.pagination_filter {
        query.push_str(&format!(" AND {f}", f = pagination_filter.as_sql()));
    }

    if let Some(filter) = &params.filter {
        query.push_str(&format!(" AND {f}", f = filter.as_sql()));
    }

    // Detect if sortby and the filter are in opposite direction, meaning the query is used the get the last n results
    //
    // Order   -->
    // Dataset [----------------------------------]
    // Filter                 |< All results before this limit
    // Result            [--->]
    //
    // In those case, we first get the {limit} results in reverse order from the filter, then we sort again the results by the real sort
    let reverted_sort = is_reverted_sort(&params);
    let initial_sort = if reverted_sort {
        params.sortby.to_reverted_sql()
    } else {
        params.sortby.to_sql()
    };
    query.push_str(&format!(" ORDER BY {initial_sort}"));

    query.push_str(&format!(" LIMIT {p}", p = params.limit));

    if reverted_sort {
        // We revert the whole query to sort again the results by the real sort
        query = format!(
            r#"SELECT content
    FROM ({query}) s
    ORDER BY {real_order_by}"#,
            real_order_by = params.sortby.to_sql(),
        );
    }

    // compute bounds for pagination
    let dataset_bounds: Bounds<DateTime<Utc>> =
        if let Some(d) = get_dataset_bounds(db, &params.sortby.field).await? {
            d
        } else {
            // if no bounds found, it means there is not collections, it's not an error, we can return an empty result
            let def_bounds = Bounds {
                min: DateTime::<Utc>::MIN_UTC,
                max: DateTime::<Utc>::MAX_UTC,
            };
            return Ok(CollectionsAndBounds {
                collections: vec![],
                bounds: def_bounds.clone(),
                dataset_bounds: def_bounds,
            });
        };

    // log::info!("collection sql query = {}", query);
    let collections: Vec<Value> = sqlx::query_as(&query)
        .fetch_all(db)
        .await?
        .into_iter()
        .map(|mut c: SqlCollection| {
            c.update_links(&req);
            c.content
        })
        .collect();

    let result_bounds = compute_result_bound(&collections, &params.sortby.field);
    Ok(CollectionsAndBounds {
        collections,
        bounds: result_bounds,
        dataset_bounds,
    })
}

/// Get list of collections.
#[api_operation(tag = "Collections")]
#[tracing::instrument]
pub async fn get_collections(
    req: HttpRequest,
    db: Data<PgPool>,
    params: web::Query<CollectionsQuery>,
) -> Result<web::Json<Collections>, APIError> {
    let params = params.0.validate()?;

    let cols = get_collections_and_bounds(db.as_ref(), &params, &req).await?;

    let mut links = compute_pagination_links(
        &req,
        cols.bounds,
        cols.dataset_bounds,
        &params.sortby,
        params.limit,
    );
    links.extend(vec![
        link::from_service(&req, "root", "root").json(),
        link::from_service(&req, "get_collections", "self").json(),
    ]);

    Ok(web::Json(Collections {
        collections: cols.collections,
        links,
    }))
}

// detect if sortby and the filter are in opposite direction, meaning the query is used the get the last n results
//
// Order   -->
// Dataset [----------------------------------]
// Filter                 |< All results before this limit
// Result            [--->]
fn is_reverted_sort(params: &CollectionsQuery) -> bool {
    if let Some(p) = &params.pagination_filter {
        if params.sortby.order == SortByOrder::Asc
            && (p.comparator == Comparison::Less || p.comparator == Comparison::LessOrEqual)
        {
            return true;
        }
        if params.sortby.order == SortByOrder::Desc
            && (p.comparator == Comparison::Greater || p.comparator == Comparison::GreaterOrEqual)
        {
            return true;
        }
    }
    false
}

fn compute_pagination_links(
    req: &HttpRequest,
    result_bounds: Bounds<DateTime<Utc>>,
    dataset_bounds: Bounds<DateTime<Utc>>,
    sort_by: &CollectionSortBy,
    limit: u16,
) -> Vec<stac::Link> {
    let mut links = vec![];
    let api_sort_by = sort_by.as_api_parameter();

    let api_filter = |cmp, dt_bound: DateTime<Utc>| {
        urlencoding::encode(&format!(
            "{field}{cmp}'{bound}'",
            field = sort_by.field.as_api_parameter(),
            bound = dt_bound.to_rfc3339()
        ))
        .into_owned()
    };

    if (sort_by.order == SortByOrder::Asc && dataset_bounds.min < result_bounds.min)
        || (sort_by.order == SortByOrder::Desc && dataset_bounds.max > result_bounds.max)
    {
        // There are results "before", we need a `first` and a `prev` link
        links.push(
            from_path(
                req,
                &format!("/api/collections?sortby={api_sort_by}&limit={limit}"),
                "first",
            )
            .json(),
        );

        let filter = if sort_by.order == SortByOrder::Asc {
            api_filter("<", result_bounds.min)
        } else {
            api_filter(">", result_bounds.max)
        };
        links.push(
            from_path(
                req,
                &format!("/api/collections?sortby={api_sort_by}&page={filter}&limit={limit}"),
                "prev",
            )
            .json(),
        );
    }

    if (sort_by.order == SortByOrder::Asc && dataset_bounds.max > result_bounds.max)
        || (sort_by.order == SortByOrder::Desc && dataset_bounds.min < result_bounds.min)
    {
        // There are results after, we need a next link
        let next_filter = if sort_by.order == SortByOrder::Asc {
            api_filter(">", result_bounds.max)
        } else {
            api_filter("<", result_bounds.min)
        };
        links.push(
            from_path(
                req,
                &format!("/api/collections?sortby={api_sort_by}&page={next_filter}&limit={limit}",),
                "next",
            )
            .json(),
        );
        let last_filter = if sort_by.order == SortByOrder::Asc {
            api_filter("<=", dataset_bounds.max)
        } else {
            api_filter(">=", dataset_bounds.min)
        };
        links.push(
            from_path(
                req,
                &format!("/api/collections?sortby={api_sort_by}&page={last_filter}&limit={limit}",),
                "last",
            )
            .json(),
        );
    }
    links
}

fn compute_result_bound(col: &[Value], field: &CollectionField) -> Bounds<DateTime<Utc>> {
    let (mut min, mut max) = (DateTime::<Utc>::MAX_UTC, DateTime::<Utc>::MIN_UTC);
    for v in col {
        if let Some(v) = v
            .get(field.as_str())
            .and_then(|v| v.as_str())
            .and_then(|f| {
                {
                    dateparser::parse_with(f, &Utc, NaiveTime::default()).map_err(|e| {
                        log::warn!("Impossible to parse date in collection json: {:?}", e)
                    })
                }
                .ok()
            })
        {
            min = std::cmp::min(min, v);
            max = std::cmp::max(max, v);
        }
    }
    Bounds { min, max }
}

#[derive(ApiComponent, JsonSchema, serde::Serialize)]
pub struct Collection {
    #[serde(flatten)]
    #[schemars(schema_with = "collection_schema")]
    pub content: Value,
}
fn collection_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
    apistos::Schema::new_ref(
        "https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/collection"
            .into(),
    )
}

/// Get a specific collection.
#[api_operation(tag = "Collections")]
#[tracing::instrument]
pub async fn get_collection(
    db: Data<PgPool>,
    id: web::Path<uuid::Uuid>,
    req: HttpRequest,
) -> Result<web::Json<Collection>, APIError> {
    let mut col: SqlCollection =
        sqlx::query_as::<_, SqlCollection>("SELECT content FROM collections WHERE id = $1")
            .bind(*id)
            .fetch_optional(db.as_ref())
            .await?
            .ok_or(APIError::NotFound {
                id: (*id).to_string(),
                obj_type: stac::Type::Collection.as_str(),
            })?;

    col.update_links(&req);

    Ok(web::Json(Collection {
        content: col.content,
    }))
}

#[derive(sqlx::FromRow)]
pub struct DBPic {
    pub href: String,
}

/// Get the thumbnail representing a single collection
/// Redirection to a 500px wide ready-for-display image
#[api_operation(tag = "Collections", produces = "image/jpeg")]
pub async fn get_collection_thumbnail(
    db: Data<PgPool>,
    id: web::Path<uuid::Uuid>,
) -> Result<PicturePermanentRedirect, APIError> {
    let db_pic = sqlx::query_as::<_, DBPic>(
        "SELECT content->'assets'->'thumb'->>'href' AS href FROM items WHERE collection_id = $1 LIMIT 1"
    )
    .bind(*id)
    .fetch_optional(db.as_ref())
    .await?
    .ok_or(APIError::ItemNotFoundForCollection {
        id: id.to_owned(),
    })?;

    Ok(PicturePermanentRedirect(db_pic.href))
}

async fn get_dataset_bounds(
    db: &PgPool,
    field: &CollectionField,
) -> Result<Option<Bounds<DateTime<Utc>>>, APIError> {
    let sql_field = field.as_sql();

    let r: Option<Bounds<Option<NaiveDateTime>>> = sqlx::query_as(&format!(
        "SELECT min({sql_field}) as min, max({sql_field}) as max FROM collections",
    ))
    .fetch_optional(db)
    .await?;

    Ok(r.and_then(|b| match (b.min, b.max) {
        (Some(min), Some(max)) => Some(Bounds {
            min: min.and_utc(),
            max: max.and_utc(),
        }),
        _ => None,
    }))
}

// Note: for a user, we cannot yet filter of other parameters
#[derive(serde::Deserialize, ApiComponent, JsonSchema, Debug)]
pub struct UserCollectionQuery {
    /// Limit the number of collections
    #[serde(default = "default_limit")]
    #[schemars(range(max = 1000))]
    pub limit: u16,

    /// Limit to collections inside this bounding box
    #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
    pub bbox: Option<Vec<f64>>,

    /// CQL2 filter, only used for pagination filter. User's filters should be passed in the `filter` parameter
    #[serde(rename = "page")]
    pub pagination_filter: Option<CollectionFilter>,

    /// Define the sort order based on given property.
    /// Sort order is defined based on preceding '+' (asc) or '-' (desc).
    /// If there is no prefix, default is ascending order.
    ///
    /// Available properties are: "created" and "updated".
    ///
    /// This parameter MUST be urlencoded (%2B instead of '+')
    // #[param(default = "created")]
    #[serde(default = "default_sort_by")]
    pub sortby: CollectionSortBy,
}

#[derive(Serialize, ApiComponent, JsonSchema)]
pub struct Catalog {
    #[serde(flatten)]
    #[schemars(schema_with = "catalog_schema")]
    catalog: stac::Catalog,
}

fn catalog_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
    apistos::Schema::new_ref(
        "https://api.stacspec.org/v1.0.0/collections/openapi.yaml#/components/schemas/catalog"
            .into(),
    )
}

/// Get a STAC catalog for a given user
///
/// The STAC catalog is a single catalog with links to all collections of the user
/// The collections can be filterd with the different parameters
#[api_operation(tag = "Providers")]
#[tracing::instrument]
pub async fn get_user_catalog(
    req: HttpRequest,
    db: Data<PgPool>,
    id: web::Path<uuid::Uuid>,
    params: web::Query<UserCollectionQuery>,
) -> Result<web::Json<Catalog>, APIError> {
    let collection_params = CollectionsQuery {
        limit: params.limit,
        bbox: params.0.bbox,
        pagination_filter: params.0.pagination_filter,
        filter: Some(CollectionFilter {
            field: CollectionField::ProviderId,
            comparator: Comparison::Equal,
            value: format!("'{}'", id),
        }),
        sortby: params.0.sortby,
    };
    let collection_params = collection_params.validate()?;

    // filter for a given user

    let cols = get_collections_and_bounds(db.as_ref(), &collection_params, &req).await?;

    let mut links = compute_pagination_links(
        &req,
        cols.bounds,
        cols.dataset_bounds,
        &collection_params.sortby,
        collection_params.limit,
    );

    links.push(link::from_service(&req, "root", "root").json());
    links.push(link::from_path(&req, &format!("/api/users/{}/catalog", &id), "self").json());

    for c in cols.collections {
        let id = c
            .get("id")
            .and_then(|v| v.as_str())
            .map(|s| s.to_string())
            .expect("id is a mandatory field");
        let name = c
            .get("name")
            .and_then(|v| v.as_str())
            .map(|s| s.to_string());
        links.push(
            link::from_path(&req, &format!("/api/collections/{id}"), "child")
                .json()
                .title(name),
        );
    }

    let mut cat = stac::Catalog::new(format!("user:{id}"), format!("user's catalog"));
    cat.links = links;

    Ok(web::Json(Catalog { catalog: cat }))
}

#[cfg(test)]
mod test {
    use crate::routes::collections::compute_result_bound;
    use crate::utils::params::CollectionField;
    use crate::utils::params::CollectionSortBy;
    use crate::utils::params::SortByOrder;
    use actix_web::test::TestRequest;
    use chrono::DateTime;
    use chrono::TimeZone;
    use chrono::Timelike;
    use chrono::Utc;
    use pretty_assertions::assert_eq;
    use stac::{Href, Link};

    use super::Bounds;

    use super::compute_pagination_links;

    // helper method for easier datetime creation
    fn dt(year: i32, month: u32, day: u32) -> DateTime<Utc> {
        Utc.with_ymd_and_hms(year, month, day, 10, 12, 36).unwrap()
    }

    #[test]
    fn test_pagination_without_other_results() {
        // if the dataset bounds and the results bounds are the same, all results are already there, we don't need pagination

        // Order   -->
        // Dataset [----------------------------------]
        // Result  [----------------------------------]
        let dataset_bounds = Bounds {
            min: dt(2020, 1, 12),
            max: dt(2021, 1, 12),
        };
        let result_bounds = dataset_bounds.clone();

        let links = compute_pagination_links(
            &TestRequest::get().to_http_request(),
            result_bounds,
            dataset_bounds,
            &CollectionSortBy {
                field: CollectionField::Created,
                order: SortByOrder::Asc,
            },
            10,
        );

        assert_eq!(links, vec![]);
    }

    #[test]
    fn test_pagination_with_results_before() {
        // if there is data before, we should have a first/prev link
        // Order   -->
        // Dataset [----------------------------------]
        // Result                     [---------------]
        let dataset_bounds = Bounds {
            min: dt(2019, 1, 12),
            max: dt(2021, 1, 12),
        };
        let result_bounds = Bounds {
            min: dt(2020, 1, 12),
            max: dt(2021, 1, 12),
        };

        let links = compute_pagination_links(
            &TestRequest::get().to_http_request(),
            result_bounds,
            dataset_bounds,
            &CollectionSortBy {
                field: CollectionField::Created,
                order: SortByOrder::Asc,
            },
            10,
        );

        assert_eq!(
            links,
            vec![
                Link::new(
                    Href::String("http://localhost:8080/api/collections?sortby=created&limit=10".to_string()),
                    "first"
                )
                .json(),
                Link::new(
                    Href::String("http://localhost:8080/api/collections?sortby=created&page=created%3C%272020-01-12T10%3A12%3A36%2B00%3A00%27&limit=10".to_string()),
                    "prev"
                )
                .json(),
            ]
        );
    }

    #[test]
    fn test_pagination_with_results_after_reverse_order() {
        // if there is data after and we sort by reverse order, we should have 'prev'/'first' link

        //                                    <-- Order
        // Dataset [----------------------------------]
        // Result  [---------------]
        let dataset_bounds = Bounds {
            min: dt(2019, 1, 12),
            max: dt(2021, 1, 12),
        };
        let result_bounds = Bounds {
            min: dt(2019, 1, 12),
            max: dt(2020, 1, 12),
        };

        let links = compute_pagination_links(
            &TestRequest::get().to_http_request(),
            result_bounds,
            dataset_bounds,
            &CollectionSortBy {
                field: CollectionField::Created,
                order: SortByOrder::Desc,
            },
            100,
        );

        assert_eq!(
            links,
            vec![
                Link::new(
                    Href::String("http://localhost:8080/api/collections?sortby=-created&limit=100".to_string()),
                    "first"
                )
                .json(),
                Link::new(
                    Href::String( "http://localhost:8080/api/collections?sortby=-created&page=created%3E%272020-01-12T10%3A12%3A36%2B00%3A00%27&limit=100".to_string()),
                    "prev"
                )
                .json(),
            ]
        );
    }

    #[test]
    fn test_pagination_with_results_before_and_after() {
        // if there is data before and after, we should have a first/prev and next/last link
        // Order   -->
        // Dataset [----------------------------------]
        // Result           [---------------]
        let dataset_bounds = Bounds {
            min: dt(2019, 1, 12),
            max: dt(2022, 1, 12),
        };
        let result_bounds = Bounds {
            min: dt(2020, 1, 12),
            max: dt(2021, 1, 12),
        };

        let links = compute_pagination_links(
            &TestRequest::get().to_http_request(),
            result_bounds,
            dataset_bounds,
            &CollectionSortBy {
                field: CollectionField::Updated,
                order: SortByOrder::Asc,
            },
            12,
        );

        assert_eq!(
            links,
            vec![
                Link::new(
                    Href::String("http://localhost:8080/api/collections?sortby=updated&limit=12".to_string()),
                    "first"
                )
                .json(),
                Link::new(
                    Href::String("http://localhost:8080/api/collections?sortby=updated&page=updated%3C%272020-01-12T10%3A12%3A36%2B00%3A00%27&limit=12".to_string()),
                    "prev"
                )
                .json(),
                Link::new(
                    Href::String("http://localhost:8080/api/collections?sortby=updated&page=updated%3E%272021-01-12T10%3A12%3A36%2B00%3A00%27&limit=12".to_string()),
                    "next"
                )
                .json(),
                Link::new(
                    Href::String("http://localhost:8080/api/collections?sortby=updated&page=updated%3C%3D%272022-01-12T10%3A12%3A36%2B00%3A00%27&limit=12".to_string()),
                    "last"
                )
                .json(),
            ]
        );
    }

    #[test]
    fn test_compute_result_bound() {
        // feed a mock collection with fewer fields
        let cols = &vec![
            serde_json::from_str(
                r#"{
"id": "bd1f8896-0fbf-411c-82fa-e677db122742",                                                      
"type": "Collection",                                                                              
"title": "another collections",                                                                    
"created": "2024-01-01T13:58:03.515598+00:00",                                                     
"license": "proprietary",                                                                          
"updated": "2024-02-01T13:58:03.515598+00:00"}"#,
            )
            .unwrap(),
            serde_json::from_str(
                r#"{
"id": "bd1f8896-0fbf-411c-82fa-e677db122742",                                                      
"type": "Collection",                                                                              
"title": "another collections",                                                                    
"created": "2023-01-01T14:58:03.515598+00:00",                                                     
"license": "proprietary",                                                                          
"updated": "2023-02-01T14:58:03.515598+00:00"}"#,
            )
            .unwrap(),
        ];

        assert_eq!(
            compute_result_bound(cols, &CollectionField::Created),
            Bounds {
                min: Utc
                    .with_ymd_and_hms(2023, 1, 1, 14, 58, 03)
                    .unwrap()
                    .with_nanosecond(515598000)
                    .unwrap(),
                max: Utc
                    .with_ymd_and_hms(2024, 1, 1, 13, 58, 03)
                    .unwrap()
                    .with_nanosecond(515598000)
                    .unwrap()
            }
        );

        // if we ask for the updated bounds, we got the rights bounds
        assert_eq!(
            compute_result_bound(cols, &CollectionField::Updated),
            Bounds {
                min: Utc
                    .with_ymd_and_hms(2023, 2, 1, 14, 58, 03)
                    .unwrap()
                    .with_nanosecond(515598000)
                    .unwrap(),
                max: Utc
                    .with_ymd_and_hms(2024, 2, 1, 13, 58, 03)
                    .unwrap()
                    .with_nanosecond(515598000)
                    .unwrap()
            }
        );
    }
}
