use apistos::web::{get, head, post, resource, scope, ServiceConfig};

mod collections;
mod index;
mod instances;
mod items;
mod map;
mod pictures;
mod providers;
mod report;
mod root;
mod stats;
mod status;

pub fn mount(conf: &mut ServiceConfig) {
    let api_scope = scope("/api")
        .service(resource("").name("root").route(get().to(root::root)))
        .service(
            resource("/collections")
                .name("get_collections")
                .route(get().to(collections::get_collections)),
        )
        .service(
            resource("/collections/{id}")
                .name("get_collection")
                .route(get().to(collections::get_collection)),
        )
        .service(
            resource("/collections/{id}/thumb.jpg")
                .name("get_collection_thumbnail")
                .route(get().to(collections::get_collection_thumbnail)),
        )
        .service(
            resource("/users/{id}/catalog")
                .name("get_user_catalog")
                .route(get().to(collections::get_user_catalog)),
        )
        .service(
            resource("/collections/{id}/items")
                .name("get_collection_items")
                .route(get().to(items::get_collection_items)),
        )
        .service(
            resource("/collections/{collection_id}/items/{id}")
                .name("get_collection_item")
                .route(get().to(items::get_collection_item)),
        )
        .service(
            resource("/search")
                .name("get_search_items")
                .route(post().to(items::post_search_items))
                .route(get().to(items::get_search_items)),
        )
        .service(
            resource("/live")
                .name("live")
                .route(get().to(status::live))
                .route(head().to(status::live)),
        )
        .service(
            resource("/status")
                .name("status")
                .route(get().to(status::status))
                .route(head().to(status::status)),
        )
        .service(
            resource("/pictures/{id}/{asset}.jpg")
                .name("get_picture_asset")
                .route(get().to(pictures::get_picture_asset)),
        )
        .service(
            resource("/map/{z}/{x}/{y}.mvt")
                .name("get_tile")
                .route(get().to(map::get_tile)),
        )
        .service(
            resource("/map/style.json")
                .name("get_style")
                .route(get().to(map::get_style)),
        )
        .service(
            resource("/users/{provider_id}/map/{z}/{x}/{y}.mvt")
                .name("get_provider_tile")
                .route(get().to(map::get_provider_tile)),
        )
        .service(
            resource("/users/{provider_id}/map/style.json")
                .name("get_provider_style")
                .route(get().to(map::get_provider_style)),
        )
        .service(
            resource("/stats")
                .name("stats")
                .route(get().to(stats::stats)),
        )
        .service(
            resource("/stats/by_contributor")
                .name("stats_by_contributor")
                .route(get().to(stats::stats_by_contributor)),
        )
        .service(
            resource("/users")
                .name("get_providers_catalog")
                .route(get().to(providers::get_providers_catalog)),
        )
        .service(
            resource("/users/search")
                .name("search_providers")
                .route(get().to(providers::search_providers)),
        )
        .service(
            resource("/users/{user_id}")
                .name("get_provider")
                .route(get().to(providers::get_provider)),
        )
        .service(
            resource("/reports")
                .name("post_report")
                .route(post().to(report::post_report)),
        )
        .service(
            resource("/instances")
                .name("list_instances")
                .route(get().to(instances::list_instances)),
        )
        .service(
            resource("/instances/{name}")
                .name("get_instance")
                .route(get().to(instances::get_instance)),
        )
        .service(resource("/docs/swagger").route(get().to(index::swagger_doc)));

    conf.service(resource("/").name("index").route(get().to(index::index)))
        .service(resource("/favicon.ico").route(get().to(index::favicon)))
        .service(resource("/robots.txt").route(get().to(index::robots)))
        .service(resource("/static/meta-img.jpg").route(get().to(index::meta)))
        .service(api_scope);
}
