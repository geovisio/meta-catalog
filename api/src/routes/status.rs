use std::collections::HashMap;

use actix_web::{
    web::{self, Data, Json},
    HttpResponse, Result,
};
use apistos::{api_operation, ApiComponent, JsonSchema};
use chrono::{DateTime, Utc};
use serde::Serialize;
use sqlx::PgPool;

use crate::model::APIError;

/// check if the service is up
#[api_operation(tag = "Status")]
pub async fn live() -> HttpResponse {
    HttpResponse::Ok().finish()
}

#[derive(Serialize, JsonSchema, ApiComponent)]
pub struct Status {
    last_harvests: HashMap<String, DateTime<Utc>>,
    status: String,
}

#[derive(sqlx::FromRow)]
struct DBHarvest {
    instance: String,
    last_harvest: DateTime<Utc>,
}

#[api_operation(tag = "Status")]
pub async fn status(db: Data<PgPool>) -> Result<web::Json<Status>, APIError> {
    let query: Vec<DBHarvest> = sqlx::query_as(r#"select max(start) as last_harvest, i.name as instance from harvests h join instances i on h.instance_id = i.id group by i.name;"#)
        .fetch_all(db.as_ref())
        .await?;

    let harvest_by_instance = query
        .into_iter()
        .map(|h| (h.instance, h.last_harvest))
        .collect();

    Ok(Json(Status {
        status: "Ok".to_string(),

        last_harvests: harvest_by_instance,
    }))
}
