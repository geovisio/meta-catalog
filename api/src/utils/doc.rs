use std::collections::BTreeMap;

use actix_web::http::StatusCode;
use apistos::info::{Contact, Info};
use apistos::paths::{ExternalDocumentation, MediaType, Response, Responses};
use apistos::reference_or::ReferenceOr;
use apistos::spec::Spec;
use apistos::tag::Tag;
use apistos::{ApistosSchema, OpenApiVersion, Schema, VersionSpecificSchema};

pub fn doc() -> Spec {
    Spec {
        openapi: OpenApiVersion::OAS3_1,
        tags: vec![
            Tag {
                name: "Collections".to_string(),
                description: Some("STAC collections (also called sequences)".to_string()),
                ..Default::default()
            },
            Tag {
                name: "Items".to_string(),
                description: Some("STAC items (references Geolocated images)".to_string()),
                ..Default::default()
            },
            Tag {
                name: "Map".to_string(),
                description: Some("Tiles for web map display".to_string()),
                ..Default::default()
            },
            Tag {
                name: "Providers".to_string(),
                description: Some("Account management".to_string()),
                ..Default::default()
            },
            Tag {
                name: "Metadata".to_string(),
                description: Some("API metadata".to_string()),
                ..Default::default()
            },
            Tag {
                name: "Status".to_string(),
                description: Some("Api status".to_string()),
                ..Default::default()
            },
        ],
        info: Info {
            title: "Panoramax API".to_string(),
            description: Some(
                "This STAC API references data from several Panoramax instances".to_string(),
            ),
            contact: Some(Contact {
                email: Some("panoramax@panoramax.fr".to_string()),
                ..Default::default()
            }),
            ..Default::default()
        },
        external_docs: Some(ExternalDocumentation {
            description: Some("Panoramax documentation".to_string()),
            url: "https://docs.panoramax.fr/".to_string(),
            ..Default::default()
        }),
        ..Default::default()
    }
}

/// Make an OpenAPI response for a given status code, content type and schema
pub fn make_response(
    status_code: StatusCode,
    content_type: &str,
    schema: Schema,
    oas_version: OpenApiVersion,
    description: &str,
) -> Option<Responses> {
    let sch = ApistosSchema::new(schema, oas_version);
    let schema = match oas_version {
        OpenApiVersion::OAS3_0 => VersionSpecificSchema::OAS3_0(ReferenceOr::Object(sch)),
        OpenApiVersion::OAS3_1 => VersionSpecificSchema::OAS3_1(sch),
    };

    let response = Response {
        content: BTreeMap::from_iter(vec![(
            content_type.to_string(),
            MediaType {
                schema: Some(schema),
                ..Default::default()
            },
        )]),
        description: description.to_string(),
        ..Default::default()
    };

    Some(Responses {
        responses: BTreeMap::from_iter(vec![(
            status_code.as_str().to_string(),
            ReferenceOr::Object(response),
        )]),
        ..Default::default()
    })
}
