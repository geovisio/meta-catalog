use apistos::{json_schema, ApiComponent};
use schemars::JsonSchema;
use serde::{de::Deserializer, Deserialize};
use std::borrow::Cow;

/// Represent a coordinate
/// The coordinates can be deserialized from a string formated as `lon,lat`.
#[derive(Debug, PartialEq, ApiComponent)]
pub struct Coord {
    /// Longitude
    pub lon: f64,
    /// Latitude
    pub lat: f64,
}

impl JsonSchema for Coord {
    fn schema_name() -> Cow<'static, str> {
        "Coord".into()
    }
    fn json_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
        json_schema!({
            "type": "string",
            "pattern": r#"-?\d+\.\d+,-?\d+\.\d+"#
        })
    }
}

impl<'de> Deserialize<'de> for Coord {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        use crate::utils::serde_utils::deserialize_as_stac_opt_list;
        let values: Option<Vec<f64>> = deserialize_as_stac_opt_list(deserializer)?;
        match values.expect("cannot be null")[..] {
            [lon, lat] => {
                if !(-90.0..=90.0).contains(&lat) {
                    Err(serde::de::Error::custom(format!(
                        "Latitude needs to be between -90 and 90 (was {})",
                        lat
                    )))
                } else if !(-180.0..=180.0).contains(&lon) {
                    Err(serde::de::Error::custom(format!(
                        "Longitude needs to be between -180 and 180 (was {})",
                        lon
                    )))
                } else {
                    Ok(Coord { lon, lat })
                }
            }
            _ => Err(serde::de::Error::custom(
                "Parameter must be coordinates in lat,lon format)",
            )),
        }
    }
}

#[cfg(test)]
mod test {
    use super::Coord;
    use serde_json;

    #[derive(Debug, serde::Deserialize, PartialEq)]
    pub struct SomeCoordParams {
        coords: Coord,
    }
    #[test]
    fn test_coord_json() {
        let j = r#"{"coords": [1.42, 45.3]}"#;
        let parsed_params: SomeCoordParams = serde_json::from_str(j).expect("should be valid json");
        assert_eq!(
            parsed_params,
            SomeCoordParams {
                coords: Coord {
                    lon: 1.42,
                    lat: 45.3
                }
            }
        );
    }
    #[test]
    fn test_coord_qs() {
        let query = actix_web::web::Query::<SomeCoordParams>::from_query("coords=1.42,45.3")
            .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            SomeCoordParams {
                coords: Coord {
                    lon: 1.42,
                    lat: 45.3
                }
            }
        );
    }
    #[test]
    fn test_invalid_lat() {
        let j = r#"{"coords": [45.3, 91.42]}"#;
        let e = serde_json::from_str::<SomeCoordParams>(j).unwrap_err();
        assert_eq!(
            e.to_string(),
            "Latitude needs to be between -90 and 90 (was 91.42) at line 1 column 25".to_owned()
        )
    }
    #[test]
    fn test_invalid_lon() {
        let j = r#"{"coords": [192.3, 1.42]}"#;
        let e = serde_json::from_str::<SomeCoordParams>(j).unwrap_err();
        assert_eq!(
            e.to_string(),
            "Longitude needs to be between -180 and 180 (was 192.3) at line 1 column 25".to_owned()
        )
    }
    #[test]
    fn test_invalid_values() {
        let j = r#"{"coords": ["bob", "bobette"]}"#;
        let e = serde_json::from_str::<SomeCoordParams>(j).unwrap_err();
        assert_eq!(
            e.to_string(),
            "invalid type: string \"bob\", expected f64 at line 1 column 17".to_owned()
        )
    }
    #[test]
    fn test_invalid_coords() {
        let e = actix_web::web::Query::<SomeCoordParams>::from_query("coords=plop").unwrap_err();
        assert_eq!(
            e.to_string(),
            "Query deserialize error: Invalid parameter: plop is not a valid f64".to_owned()
        )
    }
}
