use crate::utils::params::CollectionField;
use anyhow::bail;
use anyhow::{anyhow, Context};
use apistos::{json_schema, ApiComponent, JsonSchema};
use lazy_static::lazy_static;
use regex::Regex;
use serde::{Deserialize, Deserializer};
use std::borrow::Cow;
use std::str::FromStr;

#[derive(Debug, PartialEq, Default, JsonSchema, ApiComponent)]
pub enum SortByOrder {
    #[default]
    Asc,
    Desc,
}

impl SortByOrder {
    /// Build from an API parameter
    pub fn from_api_parameter(s: &str) -> anyhow::Result<Self> {
        match s {
            "+" | "" => Ok(SortByOrder::Asc),
            "-" => Ok(SortByOrder::Desc),
            _ => bail!("invalid sort order: {}", s),
        }
    }
    /// Get the reverse order
    pub fn reverse(&self) -> Self {
        match self {
            SortByOrder::Asc => SortByOrder::Desc,
            SortByOrder::Desc => SortByOrder::Asc,
        }
    }
    /// Get SQL sort
    pub fn to_sql(&self) -> &'static str {
        match self {
            SortByOrder::Asc => "ASC",
            SortByOrder::Desc => "DESC",
        }
    }
    /// Get the order as used in the API
    pub fn as_api_parameter(&self) -> &'static str {
        match self {
            SortByOrder::Asc => "",
            SortByOrder::Desc => "-",
        }
    }
}

#[derive(Debug, PartialEq, ApiComponent)]
pub struct CollectionSortBy {
    pub field: CollectionField,
    pub order: SortByOrder,
}

impl FromStr for CollectionSortBy {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> anyhow::Result<Self> {
        lazy_static! {
            static ref RE: Regex = Regex::new(r"^(?P<order>[+-]?)(?P<field>.+)$").unwrap();
        }

        let cap = RE.captures(s).context("impossible to parse order")?;
        let raw_order = cap.name("order");

        let raw_field = cap
            .name("field")
            .ok_or_else(|| anyhow!("no field in sortby"))?;
        let order = if let Some(o) = raw_order {
            SortByOrder::from_api_parameter(o.as_str())?
        } else {
            SortByOrder::default()
        };
        let field =
            CollectionField::from_str(raw_field.as_str()).context("invalid field to sort on")?;
        Ok(CollectionSortBy { field, order })
    }
}

impl JsonSchema for CollectionSortBy {
    fn schema_name() -> Cow<'static, str> {
        "CollectionSortBy".into()
    }
    fn json_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
        json_schema!({
            "type": "string",
            "pattern": "[+|-]?[A-Za-z_].*"
        })
    }
}

impl CollectionSortBy {
    /// Get the sort as a SQL query
    pub fn to_sql(&self) -> String {
        format!(
            "{field} {order}",
            field = self.field.as_sql(),
            order = self.order.to_sql()
        )
    }
    /// Get the reverse sort
    pub fn to_reverted_sql(&self) -> String {
        format!(
            "{field} {rev_order}",
            field = self.field.as_sql(),
            rev_order = self.order.reverse().to_sql()
        )
    }

    /// Get the sort as used in the API
    pub fn as_api_parameter(&self) -> String {
        format!(
            "{order}{api_field}",
            order = self.order.as_api_parameter(),
            api_field = self.field.as_api_parameter()
        )
    }
}
impl Default for CollectionSortBy {
    fn default() -> Self {
        CollectionSortBy {
            field: CollectionField::Created,
            order: SortByOrder::default(),
        }
    }
}
impl<'de> Deserialize<'de> for CollectionSortBy {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: String = String::deserialize(deserializer)?;
        Self::from_str(s.as_str())
            .map_err(|e| serde::de::Error::custom(format!("Invalid sortby parameter: {:?}", e)))
    }
}

#[cfg(test)]
mod test {
    use super::CollectionSortBy;
    use crate::utils::params::sort_by::SortByOrder;
    use std::str::FromStr;

    #[test]
    fn simple() {
        assert_eq!(
            CollectionSortBy::from_str("-created").unwrap(),
            CollectionSortBy {
                field: crate::utils::params::CollectionField::Created,
                order: SortByOrder::Desc,
            }
        );
    }
    #[test]
    fn default_order() {
        assert_eq!(
            CollectionSortBy::from_str("updated").unwrap(),
            CollectionSortBy {
                field: crate::utils::params::CollectionField::Updated,
                order: SortByOrder::Asc,
            }
        );
    }

    #[test]
    fn invalid_order() {
        assert_eq!(
            CollectionSortBy::from_str("%created")
                .unwrap_err()
                .to_string(),
            "invalid field to sort on".to_string()
        );
    }

    #[test]
    fn test_from_query_params() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct Params {
            sortby: CollectionSortBy,
        }
        let query = actix_web::web::Query::<Params>::from_query("sortby=%2Bcreated")
            .expect("should be valid query");

        assert_eq!(
            query.into_inner(),
            Params {
                sortby: CollectionSortBy {
                    field: crate::utils::params::CollectionField::Created,
                    order: SortByOrder::Asc,
                }
            }
        );
    }
}
