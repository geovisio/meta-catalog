use std::str::FromStr;

/// Collection fields on which we can sort
#[derive(serde::Deserialize, Debug, PartialEq, Eq, Clone, Copy)]
#[serde(rename_all = "lowercase")]
pub enum ItemField {
    /// Capture time of the item
    CaptureTime,
}

impl ItemField {
    pub fn as_sql(&self) -> &'static str {
        match self {
            Self::CaptureTime => "datetime",
        }
    }
}

impl FromStr for ItemField {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "datetime" => Ok(ItemField::CaptureTime),
            _ => anyhow::bail!(
                "invalid item field: '{s}', only `datetime` is supported for the moment"
            ),
        }
    }
}

impl ItemField {
    pub fn as_str(&self) -> &'static str {
        match self {
            ItemField::CaptureTime => "datetime",
        }
    }

    /// Get the collection field by its API name
    pub fn as_api_parameter(&self) -> &'static str {
        self.as_str()
    }
}
