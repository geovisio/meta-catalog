use std::str::FromStr;

use anyhow::{anyhow, Context, Result};
use cql2::{Expr, Validator};
use sqlx::{Postgres, QueryBuilder};

#[derive(Debug, Clone)]
pub struct CqlFilter {
    pub expr: Expr,
}

impl FromStr for CqlFilter {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        let expr: Expr = s
            .parse()
            .with_context(|| format!("Impossible to parse '{s}' as a CQL2 filter"))?;

        Ok(Self { expr })
    }
}

/// Recursively traverse the expression tree
/// and call the given function on Expressions
fn traverse_expr(expr: &Expr, f: &impl Fn(&Expr) -> Result<()>) -> Result<()> {
    match expr {
        Expr::Array(v) => {
            for item in v {
                traverse_expr(item, f)?;
            }
            Ok(())
        }
        Expr::Operation { op: _, args } => {
            for arg in args {
                traverse_expr(arg, f)?;
            }
            Ok(())
        }
        e => f(e),
    }
}

impl CqlFilter {
    pub fn fill_sql_builder<'args>(
        &self,
        query_builder: &mut QueryBuilder<'args, Postgres>,
        properties_mapping: &phf::Map<&'static str, &'static str>,
    ) -> Result<()> {
        fill_sql_builder(&self.expr, query_builder, properties_mapping)
    }

    // Validate that the expression is a valid CQL2 and use only the allowed fields.
    // For performance reason, we might not want to be able to search on all possible fields for the moment
    pub fn validate(&self, validation_function: &impl Fn(&Expr) -> Result<()>) -> Result<()> {
        // we first check that it's a valid cql2 expression
        let value = serde_json::to_value(&self.expr)
            .with_context(|| "Impossible to validate expression")?;
        let validator = Validator::new().with_context(|| "Could not create default validator")?;
        validator.validate(&value).map_err(|_e| {
            anyhow!("Expression is not a valid cql2 filter: jsonschema validation failed")
        })?; // Note: we don't use the error message from the openapi validator, as it's too verbose

        // then that we only use allowed fields
        traverse_expr(&self.expr, validation_function)?;

        Ok(())
    }
}

// copied from https://github.com/developmentseed/cql2-rs/blob/main/src/expr.rs method `to_sql_inner`
// but adding directly to the sqlx query builder, this way we can keep type's on the fields
fn fill_sql_builder<'args>(
    expr: &Expr,
    query: &mut QueryBuilder<'args, Postgres>,
    properties: &phf::Map<&'static str, &'static str>,
) -> Result<()> {
    match expr {
        Expr::Bool(v) => {
            query.push_bind(*v);
        }
        Expr::Float(v) => {
            query.push_bind(*v);
        }
        Expr::Literal(v) => {
            query.push_bind(v.clone());
        }
        Expr::Date { date } => {
            fill_sql_builder(date, query, properties)?;
        }
        Expr::Timestamp { timestamp } => {
            fill_sql_builder(timestamp, query, properties)?;
        }

        Expr::Interval { interval: i } => {
            fill_sql_builder_from_iter("TSTZRANGE(", ")", ", ", i.iter(), query, properties)?;
        }
        Expr::Geometry(v) => {
            query.push("EPSG:4326;");
            query.push_bind(v.to_wkt()?);
        }
        Expr::Array(v) => {
            fill_sql_builder_from_iter("[", "]", ", ", v.iter(), query, properties)?;
        }
        Expr::Property { property } => {
            if let Some(new_prop_name) = properties.get(property) {
                query.push(format!("{new_prop_name}"));
            } else {
                anyhow::bail!("Property '{property}' not found in mapping");
            }
        }
        Expr::Operation { op, args } => {
            query.push("(");
            match op.as_str() {
                "between" => {
                    fill_sql_builder(&args[0], query, properties)?;
                    query.push(" BETWEEN ");
                    fill_sql_builder(&args[1], query, properties)?;
                    query.push(" AND ");
                    fill_sql_builder(&args[2], query, properties)?;
                }
                "not" => {
                    query.push("NOT ");
                    fill_sql_builder(&args[0], query, properties)?;
                }
                "is null" => {
                    fill_sql_builder(&args[0], query, properties)?;
                    query.push(" IS NULL");
                }
                "+" | "-" | "*" | "/" | "%" | "^" | "=" | "<=" | "<" | "<>" | ">" | ">=" => {
                    fill_sql_builder(&args[0], query, properties)?;
                    query.push(op);
                    fill_sql_builder(&args[1], query, properties)?;
                }
                _ => {
                    let sep = match op.as_str() {
                        "and" => " AND ",
                        "or" => " OR ",
                        _ => {
                            query.push(op);
                            ", "
                        }
                    };

                    fill_sql_builder_from_iter("(", ")", sep, args.iter(), query, properties)?;
                }
            }

            query.push(")");
        }
        Expr::BBox { bbox } => {
            fill_sql_builder_from_iter("[", "]", ", ", bbox.iter(), query, properties)?;
        }
    }
    Ok(())
}

/// Fill the sql builder with the given iterator of expressions
/// starting with `start`, ending with `end`, and using the given separator
fn fill_sql_builder_from_iter<'a, I: Iterator<Item = &'a Box<Expr>>>(
    start: &str,
    end: &str,
    separator: &str,
    exprs: I,
    query_builder: &mut QueryBuilder<'_, Postgres>,
    properties_mapping: &phf::Map<&'static str, &'static str>,
) -> Result<()> {
    query_builder.push(start);
    let mut first = true;

    for i in exprs {
        if !first {
            query_builder.push(separator);
        }
        first = false;

        fill_sql_builder(&i, query_builder, properties_mapping)?;
    }
    query_builder.push(end);
    Ok(())
}

#[cfg(test)]
mod test {
    use super::CqlFilter;
    use phf::phf_map;
    use sqlx::{Postgres, QueryBuilder};

    #[test]
    fn test_and_filter() {
        let query: CqlFilter = "field_of_view<360 and panoramax:horizontal_pixel_density >=  15"
            .parse()
            .expect("should be valid query");

        assert_eq!(
            query.expr.to_json().expect("impossible to convert to json"),
            r#"{
    "op": "and",
    "args": [
        {
            "op": "<",
            "args": [
                {
                    "property": "field_of_view"
                },
                360.0
            ]
        },
        {
            "op": ">=",
            "args": [
                {
                    "property": "panoramax:horizontal_pixel_density"
                },
                15.0
            ]
        }
    ]
}"#
            .replace(" ", "")
            .replace("\n", "")
            .to_string()
        );

        let s = query.expr.to_sql().expect("invalid sql");
        assert_eq!(
            s.query,
            r#"(("field_of_view" < $1) AND ("panoramax:horizontal_pixel_density" >= $2))"#
        );
        assert_eq!(s.params, vec!["360", "15"]);
    }

    const PROPERTIES_MAP: phf::Map<&'static str, &'static str> = phf_map! {
        "field_of_view" => "(content->'properties'->'pers:interior_orientation'->>'field_of_view')::int",
        "id" => "id",
    };

    #[test]
    fn test_item_filter_sql_builder_unknown_field() {
        // using a unknown property should be a valid filter, but shouldn't be translated to sql
        let filter = r#"((pouet > 90 AND field_of_view < 180)  OR id = 'bob')"#
            .parse::<CqlFilter>()
            .expect("should be a valid cql2 query");

        let mut query_builder = QueryBuilder::<Postgres>::new("SELECT content FROM items WHERE ");
        let err = filter
            .fill_sql_builder(&mut query_builder, &PROPERTIES_MAP)
            .expect_err("should not be valid sql");
        assert_eq!(
            format!("{:?}", err),
            r#"Property 'pouet' not found in mapping"#.to_string()
        );
    }

    #[test]
    fn test_sql_builder() {
        // try a complex query with and / or
        let query = r#"(field_of_view > 90 AND field_of_view < 180)  OR id = 'bob'"#
            .parse::<CqlFilter>()
            .expect("should be valid query");

        assert_eq!(
            query.expr.to_json().expect("impossible to convert to json"),
            r#"{
    "op": "or",
    "args": [
        {
            "op": "and",
            "args": [
                {
                    "op": ">",
                    "args": [
                        {
                            "property": "field_of_view"
                        },
                        90.0
                    ]
                },
                {
                    "op": "<",
                    "args": [
                        {
                            "property": "field_of_view"
                        },
                        180.0
                    ]
                }
            ]
        },
        {
            "op": "=",
            "args": [
                {
                    "property": "id"
                },
                "bob"
            ]
        }
    ]
}"#
            .replace(" ", "")
            .replace("\n", "")
            .to_string()
        );

        let mut query_builder = QueryBuilder::<Postgres>::new("SELECT content FROM items WHERE ");
        query
            .fill_sql_builder(&mut query_builder, &PROPERTIES_MAP)
            .expect("should be valid sql");
        assert_eq!(
                query_builder.sql(),
                r#"SELECT content FROM items WHERE ((((((content->'properties'->'pers:interior_orientation'->>'field_of_view')::int>$1) AND ((content->'properties'->'pers:interior_orientation'->>'field_of_view')::int<$2))) OR (id=$3)))"#
                    .to_string()
            );
    }

    #[test]
    fn test_sql_builder_between() {
        // try a complex query with and / or
        let query = r#"((field_of_view BETWEEN 90 AND 180)  OR id = 'bob')"#
            .parse::<CqlFilter>()
            .expect("should be valid query");

        assert_eq!(
            query.expr.to_json().expect("impossible to convert to json"),
            r#"{
        "op": "or",
        "args": [
            {
                "op": "between",
                "args": [
                    {
                        "property": "field_of_view"
                    },
                    90.0,
                    180.0
                ]
            },
            {
                "op": "=",
                "args": [
                    {
                        "property": "id"
                    },
                    "bob"
                ]
            }
        ]
    }"#
            .replace(" ", "")
            .replace("\n", "")
            .to_string()
        );

        let mut query_builder = QueryBuilder::<Postgres>::new("SELECT content FROM items WHERE ");
        query
            .fill_sql_builder(&mut query_builder, &PROPERTIES_MAP)
            .expect("should be valid sql");
        assert_eq!(
                query_builder.sql(),
                r#"SELECT content FROM items WHERE ((((content->'properties'->'pers:interior_orientation'->>'field_of_view')::int BETWEEN $1 AND $2) OR (id=$3)))"#
                    .to_string()
            );
    }
}
