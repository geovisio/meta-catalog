#[derive(sqlx::FromRow, Clone, PartialEq, Debug)]
pub struct Bounds<T> {
    pub min: T,
    pub max: T,
}

impl<T: Ord + Copy> Bounds<T> {
    pub fn update(&mut self, val: T) {
        self.min = std::cmp::min(self.min, val);
        self.max = std::cmp::max(self.max, val);
    }
}
