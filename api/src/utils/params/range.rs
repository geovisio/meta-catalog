use std::borrow::Cow;

use apistos::{json_schema, ApiComponent};
use schemars::JsonSchema;
use serde::{de::Deserializer, Deserialize};

/// Represent a range with a minimum and a maximum
/// The range can be deserialized from a string formated as `min-max`.
#[derive(Debug, PartialEq, Eq, ApiComponent)]
pub struct Range {
    // minimum
    pub min: u64,
    // maximum
    pub max: u64,
}

impl JsonSchema for Range {
    fn schema_name() -> Cow<'static, str> {
        "Range".into()
    }
    fn json_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
        json_schema!({
            "type": "string",
            "pattern": r#"\d-\d"#
        })
    }
}

impl<'de> Deserialize<'de> for Range {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: String = String::deserialize(deserializer)?;
        let split: Vec<u64> = s
            .split('-')
            .map(|v| {
                v.parse::<u64>().map_err(|_| {
                    serde::de::Error::custom(format!(
                        "Range values should be integers (was '{}')",
                        s
                    ))
                })
            })
            .collect::<Result<Vec<u64>, _>>()?;
        match split[..] {
            [min, max] => {
                if min > max {
                    Err(serde::de::Error::custom(format!(
                        "Range parameter has a min value greater than its max value (was '{}')",
                        s
                    )))
                } else {
                    Ok(Range { min, max })
                }
            }
            _ => Err(serde::de::Error::custom(
                r#"Parameter is invalid (should be a distance range in meters like "5-15")"#,
            )),
        }
    }
}

#[cfg(test)]
mod test {
    use super::Range;
    use serde_json;

    #[test]
    fn test_range_json() {
        let j = r#""3-15""#;
        let parsed_params: Range = serde_json::from_str(j).expect("should be valid json");
        assert_eq!(parsed_params, Range { min: 3, max: 15 });
    }
    #[test]
    fn test_invalid_range_order() {
        let j = r#""15-10""#;
        let e = serde_json::from_str::<Range>(j).unwrap_err();
        assert_eq!(
            e.to_string(),
            "Range parameter has a min value greater than its max value (was '15-10')".to_owned()
        )
    }
    #[test]
    fn test_invalid_range() {
        let j = r#""1510""#;
        let e = serde_json::from_str::<Range>(j).unwrap_err();
        assert_eq!(
            e.to_string(),
            "Parameter is invalid (should be a distance range in meters like \"5-15\")".to_owned()
        )
    }
    #[test]
    fn test_invalid_range_int() {
        let j = r#""bob-bobette""#;
        let e = serde_json::from_str::<Range>(j).unwrap_err();
        assert_eq!(
            e.to_string(),
            "Range values should be integers (was 'bob-bobette')".to_owned()
        )
    }
    #[test]
    fn test_range_query_str() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct SomeParams {
            range: Range,
        }
        let query = actix_web::web::Query::<SomeParams>::from_query("range=2-105")
            .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            SomeParams {
                range: Range { min: 2, max: 105 }
            }
        );
    }
}
