use actix_web::{http::StatusCode, web::Redirect, HttpRequest, HttpResponse, Responder};
use apistos::{
    json_schema, paths::Responses, reference_or::ReferenceOr, ApiComponent, ApistosSchema,
    OpenApiVersion,
};

use crate::utils::doc::make_response;

/// Redirect to a picture file
/// This struct is used to add documentation around the redirection
pub struct PicturePermanentRedirect(pub String);

impl Responder for PicturePermanentRedirect {
    type Body = ();

    fn respond_to(self, req: &HttpRequest) -> HttpResponse<Self::Body> {
        Redirect::to(self.0).permanent().respond_to(req)
    }
}

impl ApiComponent for PicturePermanentRedirect {
    fn child_schemas(_oas_version: OpenApiVersion) -> Vec<(String, ReferenceOr<ApistosSchema>)> {
        vec![]
    }

    fn schema(_oas_version: OpenApiVersion) -> Option<(String, ReferenceOr<ApistosSchema>)> {
        None
    }

    fn responses(oas_version: OpenApiVersion, _content_type: Option<String>) -> Option<Responses> {
        make_response(
            StatusCode::PERMANENT_REDIRECT,
            "image/jpeg",
            json_schema!({"type": "string", "format": "binary"}),
            oas_version,
            "",
        )
    }
}
