use apistos::{ApiComponent, JsonSchema};
use chrono::{NaiveTime, Utc};
use std::str::FromStr;
use uuid::Uuid;

use crate::model::APIError;

/// Collection fields on which we can sort
#[derive(serde::Deserialize, Debug, PartialEq, Eq, Clone, Copy, ApiComponent, JsonSchema)]
#[serde(rename_all = "lowercase")]
pub enum CollectionField {
    /// Creation date of the collection
    Created,

    /// Update date of the collection
    Updated,

    /// Provider ID of the collection
    ProviderId,
}

impl CollectionField {
    pub fn as_sql(&self) -> &'static str {
        match self {
            // we use a immutable sql function to convert the json text to a date to be able to use the indexes
            Self::Created => "jsonb_date(content->>'created')",
            Self::Updated => "jsonb_date(content->>'updated')",
            Self::ProviderId => "provider_id",
        }
    }

    pub fn validate_value(&self, value: &str) -> Result<String, anyhow::Error> {
        match self {
            Self::Created | Self::Updated => {
                // value should be a date, and we return it formated as rfc3339

                let d = dateparser::parse_with(value, &Utc, NaiveTime::default())
                    .map_err(|e| APIError::InvalidParameter(format!("Invalid date: {e}")))?
                    .to_rfc3339();

                Ok(format!("'{}'", d))
            }
            Self::ProviderId => {
                // value should be a uuid
                let id = Uuid::parse_str(value)
                    .map_err(|e| APIError::InvalidParameter(format!("Invalid UUID: {e}")))?
                    .to_string();

                Ok(format!("'{}'", id))
            }
        }
    }
}

impl FromStr for CollectionField {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        match s {
            "created" => Ok(CollectionField::Created),
            "updated" => Ok(CollectionField::Updated),
            "provider_id" => Ok(CollectionField::ProviderId),
            _ => anyhow::bail!("invalid collection field: `{s}`, only `created`, `updated` and `provider_id` are supported for the moment"),
        }
    }
}

impl CollectionField {
    pub fn as_str(&self) -> &'static str {
        match self {
            CollectionField::Created => "created",
            CollectionField::Updated => "updated",
            CollectionField::ProviderId => "provider_id",
        }
    }

    /// Get the collection field by its API name
    pub fn as_api_parameter(&self) -> &'static str {
        self.as_str()
    }
}
