use std::{borrow::Cow, str::FromStr};

use anyhow::{anyhow, Context, Result};
use apistos::{ApiComponent, JsonSchema};
use cql2::Expr;
use phf::phf_map;
use serde::{Deserialize, Deserializer};
use sqlx::{Postgres, QueryBuilder};

use crate::utils::params::cql2_filter::CqlFilter;

/// CQL2 filter for items.
///
/// Allowed properties are:
/// * field_of_view: field of view in degree.
///
/// Usage doc can be found here: https://docs.geoserver.org/2.23.x/en/user/tutorials/cql/cql_tutorial.html
///
/// Examples:
///
/// * field_of_view > 200
#[derive(Debug, Clone, ApiComponent)]
pub struct ItemFilter {
    filter: CqlFilter,
}

impl JsonSchema for ItemFilter {
    fn schema_name() -> Cow<'static, str> {
        "ItemFilter".into()
    }
    fn json_schema(_generator: &mut apistos::SchemaGenerator) -> apistos::Schema {
        apistos::Schema::new_ref("https://schemas.opengis.net/cql2/1.0/cql2.yml#".into())
    }
}

impl FromStr for ItemFilter {
    type Err = anyhow::Error;
    fn from_str(s: &str) -> Result<Self> {
        let filter: CqlFilter = s.parse()?;

        filter
            .validate(&validate_fields)
            .with_context(|| "Impossible to validate cql2 expression")?;
        Ok(Self { filter })
    }
}

impl<'de> Deserialize<'de> for ItemFilter {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: String = String::deserialize(deserializer)?;
        ItemFilter::from_str(s.as_str()).map_err(|e| serde::de::Error::custom(format!("{:?}", e)))
    }
}

const PROPERTIES_MAP: phf::Map<&'static str, &'static str> = phf_map! {
    "field_of_view" => "COALESCE((content->'properties'->'pers:interior_orientation'->>'field_of_view')::int, 0)",
};

fn validate_fields(expr: &Expr) -> Result<()> {
    match expr {
        Expr::Property { property } => PROPERTIES_MAP
            .contains_key(&property)
            .then(|| ())
            .ok_or_else(|| anyhow!("field '{property}' is not allowed in the item search query")),
        _ => Ok(()),
    }
}

impl ItemFilter {
    pub fn fill_sql_builder(&self, query_builder: &mut QueryBuilder<'_, Postgres>) -> Result<()> {
        self.filter.fill_sql_builder(query_builder, &PROPERTIES_MAP)
    }
}

#[cfg(test)]
mod test {
    use sqlx::{Postgres, QueryBuilder};

    use crate::utils::params::item_filter::ItemFilter;

    #[test]
    fn test_item_filter_invalid_field() {
        let err = "bob < 360"
            .parse::<ItemFilter>()
            .expect_err("should not be valid query");
        assert_eq!(
            format!("{:?}", err),
            format!(
                r#"Impossible to validate cql2 expression

Caused by:
    field 'bob' is not allowed in the item search query"#
            )
        )
    }

    #[test]
    fn test_item_filter_valid_field_more_complex() {
        let query = r#"field_of_view < 180 AND (field_of_view > 90)"#
            .parse::<ItemFilter>()
            .expect("should be valid query");

        assert_eq!(
            query
                .filter
                .expr
                .to_json()
                .expect("impossible to convert to json"),
            r#"{
    "op": "and",
    "args": [
        {
            "op": "<",
            "args": [
                {
                    "property": "field_of_view"
                },
                180.0
            ]
        },
        {
            "op": ">",
            "args": [
                {
                    "property": "field_of_view"
                },
                90.0
            ]
        }
    ]
}"#
            .replace(" ", "")
            .replace("\n", "")
            .to_string()
        );
    }

    #[test]
    fn test_item_filter_invalid_field_more_complex() {
        let err =
            r#""toto" = 12 AND (field_of_view > 90 OR panoramax:horizontal_pixel_density =12)"#
                .parse::<ItemFilter>()
                .expect_err("should not be valid query");
        assert_eq!(
            format!("{:?}", err),
            format!(
                r#"Impossible to validate cql2 expression

Caused by:
    field 'toto' is not allowed in the item search query"#
            )
        )
    }
    #[test]
    fn test_item_filter_cql2() {
        let err = "plop"
            .parse::<ItemFilter>()
            .expect_err("should not be valid query");
        assert_eq!(
            err.to_string(),
            format!("Impossible to validate cql2 expression")
        )
    }

    #[test]
    fn test_item_filter_sql_builder() {
        // try a complex query with and / or
        let query = r#"field_of_view > 90 AND field_of_view < 180 OR field_of_view = 360"#
            .parse::<ItemFilter>()
            .expect("should be valid query");

        assert_eq!(
            query
                .filter
                .expr
                .to_json()
                .expect("impossible to convert to json"),
            r#"{
    "op": "or",
    "args": [
        {
            "op": "and",
            "args": [
                {
                    "op": ">",
                    "args": [
                        {
                            "property": "field_of_view"
                        },
                        90.0
                    ]
                },
                {
                    "op": "<",
                    "args": [
                        {
                            "property": "field_of_view"
                        },
                        180.0
                    ]
                }
            ]
        },
        {
            "op": "=",
            "args": [
                {
                    "property": "field_of_view"
                },
                360.0
            ]
        }
    ]
}"#
            .replace(" ", "")
            .replace("\n", "")
            .to_string()
        );

        let mut query_builder = QueryBuilder::<Postgres>::new("SELECT content FROM items WHERE ");
        query
            .fill_sql_builder(&mut query_builder)
            .expect("should be valid sql");
        assert_eq!(
            query_builder.sql(),
            r#"SELECT content FROM items WHERE (((((COALESCE((content->'properties'->'pers:interior_orientation'->>'field_of_view')::int, 0)>$1) AND (COALESCE((content->'properties'->'pers:interior_orientation'->>'field_of_view')::int, 0)<$2))) OR (COALESCE((content->'properties'->'pers:interior_orientation'->>'field_of_view')::int, 0)=$3)))"#
                .to_string()
        );
    }
}
