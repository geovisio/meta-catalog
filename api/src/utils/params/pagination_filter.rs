use apistos::{ApiComponent, ApiType, JsonSchema, TypedSchema};
use chrono::{DateTime, NaiveTime, Utc};
use serde::{Deserialize, Deserializer};
use std::str::FromStr;

use super::{CollectionField, ItemField};
use crate::model::APIError;

/// Comparison operator
#[derive(PartialEq, Eq, Debug, Clone, Copy, ApiComponent, JsonSchema)]
pub enum Comparison {
    Equal,
    Less,
    LessOrEqual,
    Greater,
    GreaterOrEqual,
}

impl Comparison {
    pub fn as_sql(&self) -> &str {
        match self {
            Comparison::Equal => "=",
            Comparison::Greater => ">",
            Comparison::GreaterOrEqual => ">=",
            Comparison::Less => "<",
            Comparison::LessOrEqual => "<=",
        }
    }
}

/// Pagination filter
///
/// For the moment do not support full cql2 syntax, only a single filters.
///
/// Only handle `created`/`updated`/`provider_id`` filters, but should be enough for the moment
#[derive(PartialEq, Eq, Debug)]
pub struct CqlFilter {
    pub field: String,
    pub comparator: Comparison,
    pub value: String,
}

// add all parse logic into a separated module
mod parse {
    use super::{Comparison, CqlFilter};
    use anyhow::anyhow;
    use nom::branch::alt;
    use nom::bytes::complete::{tag, take_until, take_while1};
    use nom::character::complete::multispace0;
    use nom::combinator::{recognize, value};
    use nom::error::{context, VerboseError};
    use nom::sequence::delimited;
    use nom::{IResult, Parser};

    // parse the comparison operator
    fn comparator(i: &str) -> IResult<&str, Comparison, VerboseError<&str>> {
        context(
            "comparator",
            alt((
                value(Comparison::Equal, tag("=")),
                value(Comparison::GreaterOrEqual, tag(">=")),
                value(Comparison::LessOrEqual, tag("<=")),
                value(Comparison::Less, tag("<")),
                value(Comparison::Greater, tag(">")),
            )),
        )(i)
    }

    fn identifier(s: &str) -> IResult<&str, &str, VerboseError<&str>> {
        take_while1(|c: char| c.is_alphabetic() || c == '_')(s)
    }

    // parse the filtered field
    fn field(i: &str) -> IResult<&str, &str, VerboseError<&str>> {
        context("field", identifier)(i)
    }

    // parse the rhs value
    // the value will be between `'`
    fn rhs_value(i: &str) -> IResult<&str, &str, VerboseError<&str>> {
        context(
            "rhs",
            delimited(tag("'"), recognize(take_until("'")), tag("'")),
        )
        .parse(i)
    }

    /// Parse the pagination filter
    ///
    /// The filter will take the form:
    /// {field} {comparator} '{date}'
    ///
    /// like `created > '2021-01-12T10:12:36Z'`
    pub fn filter(i: &str) -> Result<CqlFilter, anyhow::Error> {
        // can start with as many spaces as we want
        let (remainder, field) = delimited(multispace0, field, multispace0)(i).map_err(|_| {
            anyhow!("Invalid field, only `created`, `updated` and `provider_id` are supported for the moment")
        })?;

        let (remainder, comparator) = delimited(multispace0, comparator, multispace0)(remainder)
            .map_err(|_| anyhow!("impossible to parse comparator"))?;
        let (_, val) = rhs_value(remainder)
            .map_err(|_| anyhow!("impossible to parse datetime: {remainder}"))?;

        Ok(CqlFilter {
            field: field.to_string(),
            comparator,
            value: val.to_string(),
        })
    }
}

impl FromStr for CqlFilter {
    type Err = APIError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        parse::filter(s).map_err(|e| APIError::InvalidParameter(format!("Invalid filter: {:?}", e)))
    }
}

impl<'de> Deserialize<'de> for CqlFilter {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        let s: String = String::deserialize(deserializer)?;
        CqlFilter::from_str(s.as_str()).map_err(|e| serde::de::Error::custom(format!("{e}")))
    }
}

#[derive(PartialEq, Eq, Debug, ApiType)]
pub struct CollectionFilter {
    pub field: CollectionField,
    pub comparator: Comparison,
    pub value: String,
}

impl TryFrom<CqlFilter> for CollectionFilter {
    type Error = APIError;
    fn try_from(value: CqlFilter) -> Result<Self, Self::Error> {
        let f = CollectionField::from_str(&value.field)
            .map_err(|e| APIError::InvalidParameter(e.to_string()))?;

        let v = f
            .validate_value(&value.value)
            .map_err(|e| APIError::InvalidParameter(e.to_string()))?;

        Ok(Self {
            field: f,
            comparator: value.comparator,
            value: v,
        })
    }
}

impl FromStr for CollectionFilter {
    type Err = APIError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        CqlFilter::from_str(s)?.try_into()
    }
}

impl<'de> Deserialize<'de> for CollectionFilter {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        CqlFilter::deserialize(deserializer).and_then(|r| {
            r.try_into()
                .map_err(|e| serde::de::Error::custom(format!("{e}")))
        })
    }
}

impl TypedSchema for CollectionFilter {
    fn schema_type() -> String {
        "string".into()
    }
    fn format() -> Option<String> {
        None
    }
}

impl CollectionFilter {
    pub fn as_sql(&self) -> String {
        format!(
            "{field} {cmp} {value}",
            field = self.field.as_sql(),
            cmp = self.comparator.as_sql(),
            value = self.value,
        )
    }
}

#[derive(PartialEq, Eq, Debug, ApiType)]
pub struct CollectionItemsFilter {
    pub field: ItemField,
    pub comparator: Comparison,
    pub datetime: DateTime<Utc>,
}

impl TryFrom<CqlFilter> for CollectionItemsFilter {
    type Error = APIError;
    fn try_from(value: CqlFilter) -> Result<Self, Self::Error> {
        let f = ItemField::from_str(&value.field)
            .map_err(|e| APIError::InvalidParameter(e.to_string()))?;
        let date = dateparser::parse_with(&value.value, &Utc, NaiveTime::default())
            .map_err(|e| APIError::InvalidParameter(format!("Invalid date: {e}")))?;
        Ok(Self {
            field: f,
            comparator: value.comparator,
            datetime: date,
        })
    }
}

impl TypedSchema for CollectionItemsFilter {
    fn schema_type() -> String {
        "string".into()
    }
    fn format() -> Option<String> {
        None
    }
}

impl FromStr for CollectionItemsFilter {
    type Err = APIError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        CqlFilter::from_str(s)?.try_into()
    }
}

impl<'de> Deserialize<'de> for CollectionItemsFilter {
    fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
    where
        D: Deserializer<'de>,
    {
        CqlFilter::deserialize(deserializer).and_then(|r| {
            r.try_into()
                .map_err(|e| serde::de::Error::custom(format!(r#"Invalid page parameter: {e}"#)))
        })
    }
}

impl CollectionItemsFilter {
    pub fn as_sql(&self) -> String {
        format!(
            "{field} {cmp} '{value}'",
            field = self.field.as_sql(),
            cmp = self.comparator.as_sql(),
            value = self.datetime.to_rfc3339()
        )
    }
}
#[cfg(test)]
mod test {
    use crate::utils::params::{
        pagination_filter::{CollectionFilter, Comparison, CqlFilter},
        CollectionField, CollectionItemsFilter, ItemField,
    };
    use chrono::{TimeZone, Utc};
    use std::str::FromStr;

    #[test]
    fn pagination_from_date() {
        let pagination_str = "created >  '2021-01-01'";
        assert_eq!(
            CollectionFilter::from_str(pagination_str).unwrap(),
            CollectionFilter {
                field: CollectionField::Created,
                comparator: Comparison::Greater,
                value: "'2021-01-01T00:00:00+00:00'".to_string()
            }
        );
    }

    #[test]
    fn raw_filter_pagination_from_date() {
        let pagination_str = "created >  '2021-01-01'";
        assert_eq!(
            CqlFilter::from_str(pagination_str).unwrap(),
            CqlFilter {
                field: "created".to_string(),
                comparator: Comparison::Greater,
                value: "2021-01-01".to_string()
            }
        );
    }
    #[test]
    fn pagination_from_full_datetime() {
        let pagination_str = "created<'2021-01-12T10:12:36Z'";
        assert_eq!(
            CollectionFilter::from_str(pagination_str).unwrap(),
            CollectionFilter {
                field: CollectionField::Created,
                comparator: Comparison::Less,
                value: "'2021-01-12T10:12:36+00:00'".to_string()
            }
        );
    }

    #[test]
    fn pagination_from_less_or_equal_full_datetime() {
        let pagination_str = "created <= '2021-01-12T10:12:36Z'";
        assert_eq!(
            CollectionFilter::from_str(pagination_str).unwrap(),
            CollectionFilter {
                field: CollectionField::Created,
                comparator: Comparison::LessOrEqual,
                value: "'2021-01-12T10:12:36+00:00'".to_string()
            }
        );
    }

    #[test]
    fn pagination_from_greater_or_equal_full_datetime() {
        let pagination_str = "created >= '2021-01-12T10:12:36Z'";
        assert_eq!(
            CollectionFilter::from_str(pagination_str).unwrap(),
            CollectionFilter {
                field: CollectionField::Created,
                comparator: Comparison::GreaterOrEqual,
                value: "'2021-01-12T10:12:36+00:00'".to_string()
            }
        );
    }

    #[test]
    fn pagination_from_full_datetime_tz_offset() {
        let pagination_str = "created>'2021-01-12T10:12:36+00:00'";
        assert_eq!(
            CollectionFilter::from_str(pagination_str).unwrap(),
            CollectionFilter {
                field: CollectionField::Created,
                comparator: Comparison::Greater,
                value: "'2021-01-12T10:12:36+00:00'".to_string()
            }
        );
    }

    #[test]
    fn pagination_from_full_datetime_without_tz() {
        // even without the trailing 'Z' we consider it UTC (note: in the format, there should not be a 'T' but a space)
        let pagination_str = "updated>'2021-01-12 10:12:36'";
        assert_eq!(
            CollectionFilter::from_str(pagination_str).unwrap(),
            CollectionFilter {
                field: CollectionField::Updated,
                comparator: Comparison::Greater,
                value: "'2021-01-12T10:12:36+00:00'".to_string()
            }
        );
    }

    #[test]
    fn invalid_pagination_date() {
        let pagination_str = "created>'plop'";
        assert_eq!(
            CollectionFilter::from_str(pagination_str)
                .unwrap_err()
                .to_string(),
            "Invalid parameter: Invalid parameter: Invalid date: plop did not match any formats."
                .to_string()
        );
    }

    #[test]
    fn invalid_pagination_field() {
        let pagination_str = "pouet>'2021-01-12 10:12:36'";
        assert_eq!(
            CollectionFilter::from_str(pagination_str)
                .unwrap_err()
                .to_string(),
            "Invalid parameter: invalid collection field: `pouet`, only `created`, `updated` and `provider_id` are supported for the moment".to_string()
        );
    }

    #[test]
    fn test_from_query_params() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct Params {
            page: CqlFilter,
        }
        let query = actix_web::web::Query::<Params>::from_query(
            "page=created+%3E+'2021-01-12T10%3A12%3A36Z'",
        )
        .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            Params {
                page: CqlFilter {
                    field: "created".to_string(),
                    comparator: Comparison::Greater,
                    value: "2021-01-12T10:12:36Z".to_string()
                }
            }
        );
    }
    #[test]
    fn test_items_filter_from_query_params() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct Params {
            page: CollectionItemsFilter,
        }
        let query = actix_web::web::Query::<Params>::from_query(
            "page=datetime+%3E+'2021-01-12T10%3A12%3A36Z'",
        )
        .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            Params {
                page: CollectionItemsFilter {
                    field: ItemField::CaptureTime,
                    comparator: Comparison::Greater,
                    datetime: Utc.with_ymd_and_hms(2021, 1, 12, 10, 12, 36).unwrap()
                }
            }
        );
    }

    #[test]
    fn provider_id() {
        let filter = "provider_id = '12345678-1234-5678-1234-567812345678'";
        assert_eq!(
            CollectionFilter::from_str(filter).unwrap(),
            CollectionFilter {
                field: CollectionField::ProviderId,
                comparator: Comparison::Equal,
                value: "'12345678-1234-5678-1234-567812345678'".to_string()
            }
        );
    }

    #[test]
    fn invalid_provider_id() {
        let pagination_str = "provider_id = 'toto'";
        assert_eq!(
            CollectionFilter::from_str(pagination_str)
                .unwrap_err()
                .to_string(),
            "Invalid parameter: Invalid parameter: Invalid UUID: invalid character: expected an optional prefix of `urn:uuid:` followed by [0-9a-fA-F-], found `t` at 1".to_string()
        );
    }

    #[test]
    fn test_collection_filter_provider_id() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct Params {
            filter: CollectionFilter,
        }
        let query = actix_web::web::Query::<Params>::from_query(
            "filter=provider_id = '12345678-1234-5678-1234-567812345678'",
        )
        .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            Params {
                filter: CollectionFilter {
                    field: CollectionField::ProviderId,
                    comparator: Comparison::Equal,
                    value: "'12345678-1234-5678-1234-567812345678'".to_string()
                }
            }
        );
    }
    #[test]
    fn test_collection_filter_provider_id_spaces_before() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct Params {
            filter: CollectionFilter,
        }
        let query = actix_web::web::Query::<Params>::from_query(
            "filter=  provider_id=  '12345678-1234-5678-1234-567812345678'",
        )
        .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            Params {
                filter: CollectionFilter {
                    field: CollectionField::ProviderId,
                    comparator: Comparison::Equal,
                    value: "'12345678-1234-5678-1234-567812345678'".to_string()
                }
            }
        );
    }
}
