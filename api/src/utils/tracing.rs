use actix_web_opentelemetry::PrometheusMetricsHandler;
use opentelemetry::{global, KeyValue};
use opentelemetry_sdk::{
    metrics::{Aggregation, Instrument, SdkMeterProvider, Stream},
    Resource,
};
use tracing::level_filters::LevelFilter;
use tracing_subscriber::prelude::*;

// Create a trace exporter if there are environment variables for it
// The trace exporter will send Opentelemetry traces asynchronously to the OTEL backend
fn create_otlp_tracer() -> Option<opentelemetry_sdk::trace::Tracer> {
    if !std::env::vars().any(|(name, _)| name.starts_with("OTEL_")) {
        println!("Telemetry traces disabled, to configure it, set OTEL_EXPORTER_OTLP_ENDPOINT to a valid OpenTelemetry endpoint");
        return None;
    }
    println!("Exporting telemetry traces to configured OpenTelemetry endpoint");
    let tracer = opentelemetry_otlp::new_pipeline().tracing();

    let mut exporter = opentelemetry_otlp::new_exporter().tonic();

    // Check if we need TLS
    if let Ok(endpoint) = std::env::var("OTEL_EXPORTER_OTLP_ENDPOINT") {
        if endpoint.starts_with("https") {
            exporter = exporter.with_tls_config(Default::default());
        }
    }
    let tracer = tracer.with_exporter(exporter);

    Some(
        tracer
            .install_batch(opentelemetry_sdk::runtime::TokioCurrentThread)
            .expect("impossible to install opentelemetry tracer"),
    )
}

/// Init the tracing (with optional opentelemetry) and metrics
/// The metrics are formated for prometheus and can be served in a /metric endpoint
///
/// Note: this method should be called just after startup, there won't be logs before this calls
pub fn init_tracing_and_metrics() -> (PrometheusMetricsHandler, SdkMeterProvider) {
    let fmt_layer = tracing_subscriber::fmt::layer();

    let telemetry_layer =
        create_otlp_tracer().map(|t| tracing_opentelemetry::layer().with_tracer(t));

    let (metrics_handler, meter_provider) = {
        let registry = prometheus::Registry::new();
        let exporter = opentelemetry_prometheus::exporter()
            .with_registry(registry.clone())
            .build()
            .expect("impossible to build prometheus exporter");
        let provider = SdkMeterProvider::builder()
            .with_reader(exporter)
            .with_resource(Resource::new([KeyValue::new(
                "service.name",
                "panoramax-api",
            )]))
            .with_view(
                opentelemetry_sdk::metrics::new_view(
                    Instrument::new().name("http.server.duration"),
                    Stream::new().aggregation(Aggregation::ExplicitBucketHistogram {
                        boundaries: vec![
                            0.0, 0.005, 0.01, 0.025, 0.05, 0.075, 0.1, 0.25, 0.5, 0.75, 1.0, 2.5,
                            5.0, 7.5, 10.0,
                        ],
                        record_min_max: true,
                    }),
                )
                .unwrap(),
            )
            .build();
        global::set_meter_provider(provider.clone());

        (PrometheusMetricsHandler::new(registry), provider)
    };

    tracing_subscriber::registry()
        .with(
            tracing_subscriber::EnvFilter::builder()
                .with_default_directive(LevelFilter::INFO.into())
                .from_env_lossy(),
        )
        .with(fmt_layer)
        .with(telemetry_layer)
        .init();

    tracing::info!("Tracer inited");
    (metrics_handler, meter_provider)
}
