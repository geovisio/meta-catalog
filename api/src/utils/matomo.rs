#[derive(Clone)]
pub struct MatomoConfig {
    pub url: Option<String>,
    pub site_id: Option<String>,
}

const MATOMO_HTML: &str = r#"
<script>
  var _paq = window._paq = window._paq || [];
  _paq.push(["setDocumentTitle", document.domain + "/" + document.title]);
  _paq.push(['trackPageView']);
  _paq.push(['enableLinkTracking']);
  (function() {
    var u="//{url}/";
    _paq.push(['setTrackerUrl', u+'matomo.php']);
    _paq.push(['setSiteId', '{site-id}']);
    var d=document, g=d.createElement('script'), s=d.getElementsByTagName('script')[0];
    g.async=true; g.src=u+'matomo.js'; s.parentNode.insertBefore(g,s);
  })();
</script>
"#;

/// If a matomo server/site-id has been configured, the matomo html will be injected in the home page to send usage statistics to the configured matomo instance
pub fn matomo_html(config: &MatomoConfig) -> String {
    match (&config.url, &config.site_id) {
        (Some(url), Some(site_id)) => MATOMO_HTML
            .replace("{url}", url)
            .replace("{site-id}", site_id),
        _ => String::new(),
    }
}
