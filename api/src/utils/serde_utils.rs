use crate::model;
use serde::de::{self, SeqAccess};
use std::{any::type_name, fmt, str::FromStr};

/// Deserialize a List from either a list or a string with comma separated values
/// This makes it possible to handle STAC list parameters as:
/// * Either a real list (like in json)
/// * Either a comma separated string (like in query string)
pub fn deserialize_as_stac_opt_list<'de, D, I>(deserializer: D) -> Result<Option<Vec<I>>, D::Error>
where
    D: de::Deserializer<'de>,
    I: de::DeserializeOwned + FromStr,
{
    struct StringVecVisitor<I>(std::marker::PhantomData<I>);

    impl<'de, I> de::Visitor<'de> for StringVecVisitor<I>
    where
        I: de::DeserializeOwned + FromStr,
    {
        type Value = Option<Vec<I>>;

        fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
            formatter.write_str("a string containing a coma separated list")
        }

        fn visit_none<E>(self) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            Ok(None)
        }

        fn visit_seq<V>(self, mut seq: V) -> Result<Self::Value, V::Error>
        where
            V: SeqAccess<'de>,
        {
            let mut values = Vec::new();
            while let Some(elt) = seq.next_element()? {
                values.push(elt);
            }
            Ok(Some(values))
        }

        fn visit_str<E>(self, v: &str) -> Result<Self::Value, E>
        where
            E: de::Error,
        {
            if v.is_empty() {
                return Ok(None);
            }
            let mut values = Vec::new();
            let type_name = type_name::<I>()
                .rsplit_once("::")
                .map(|(_, t)| t)
                .unwrap_or(type_name::<I>());
            for id in v.split(',') {
                let id = I::from_str(id).map_err(|_e| {
                    de::Error::custom(model::APIError::InvalidParameter(format!(
                        "{id} is not a valid {type_name}"
                    )))
                })?;
                values.push(id);
            }
            Ok(Some(values))
        }
    }

    deserializer.deserialize_any(StringVecVisitor(std::marker::PhantomData::<I>))
}

#[cfg(test)]
mod test {
    use super::deserialize_as_stac_opt_list;
    use serde_json;

    #[derive(Debug, serde::Deserialize, PartialEq)]
    pub struct Params {
        #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
        ids: Option<Vec<String>>,
    }

    #[test]
    fn test_classic_json() {
        let j = r#"{"ids": ["bob", "bobette"]}"#;
        let parsed_params: Params = serde_json::from_str(j).expect("should be valid json");
        assert_eq!(
            parsed_params,
            Params {
                ids: Some(vec!["bob".to_string(), "bobette".to_string()])
            }
        );
    }
    #[test]
    fn test_empty_json() {
        let parsed_params: Params = serde_json::from_str("{}").expect("should be valid json");
        assert_eq!(parsed_params, Params { ids: None });
    }

    #[test]
    fn test_comma_separated() {
        let query = actix_web::web::Query::<Params>::from_query("ids=bob,bobette")
            .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            Params {
                ids: Some(vec!["bob".to_string(), "bobette".to_string()])
            }
        );
    }
    #[test]
    fn test_comma_separated_float() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct FloatParams {
            #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
            ids: Option<Vec<f32>>,
        }
        let query = actix_web::web::Query::<FloatParams>::from_query("ids=1.45,-543.5")
            .expect("should be valid query");
        assert_eq!(
            query.into_inner(),
            FloatParams {
                ids: Some(vec![1.45, -543.5])
            }
        );
    }

    #[test]
    fn test_comma_separated_uuid() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct UuidParams {
            #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
            ids: Option<Vec<uuid::Uuid>>,
        }
        let query = actix_web::web::Query::<UuidParams>::from_query("ids=pouet");
        assert!(query.is_err());
        match query {
            Ok(_) => panic!("this should be an error"),
            Err(e) => assert_eq!(
                e.to_string(),
                "Query deserialize error: Invalid parameter: pouet is not a valid Uuid".to_owned()
            ),
        }
    }

    #[test]
    fn test_invalid_float() {
        #[derive(Debug, serde::Deserialize, PartialEq)]
        pub struct FloatParams {
            #[serde(deserialize_with = "deserialize_as_stac_opt_list", default)]
            ids: Option<Vec<f64>>,
        }
        let query = actix_web::web::Query::<FloatParams>::from_query("ids=pouet,plop");
        assert!(query.is_err());
        match query {
            Ok(_) => panic!("this should be an error"),
            Err(e) => assert_eq!(
                e.to_string(),
                "Query deserialize error: Invalid parameter: pouet is not a valid f64".to_owned()
            ),
        }
    }
}
