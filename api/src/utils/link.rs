use actix_web::HttpRequest;
use stac::{Href, Link};

pub fn from_service(req: &HttpRequest, service_name: &str, rel: &str) -> Link {
    Link::new(
        req.url_for_static(service_name)
            .unwrap_or_else(|_| panic!("impossible to find service {service_name}")),
        rel,
    )
}

pub fn from_path(req: &HttpRequest, path: &str, rel: &str) -> Link {
    Link::new(Href::String(href_from_path(req, path)), rel)
}

pub fn href_from_path(req: &HttpRequest, path: &str) -> String {
    let conn = req.connection_info();
    let base = format!("{}://{}", conn.scheme(), conn.host());
    format!("{base}{path}")
}
